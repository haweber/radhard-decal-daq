// ***************************************************************************
// These macros were writting by Marie Sturm (HU) for her bachelor project
// These are scripts to analyze the data from bias scan (doing fits, plotting)
// Some minor code cleanup/formatting by Hannsjoerg Weber (HU) - no warranty
// ***************************************************************************

#include "TPad.h"
#include "TMath.h"
#include <cmath>

string BVstring(float biasvoltage){
  int v1 = (int)biasvoltage;
  int v2 = (int) ((biasvoltage+1)*10)% ((v1+1)*10);
  string bv =  (to_string(v1)+"p"+to_string(v2)).c_str();
  return bv;
}

// Funktion um Gaus Fits fuer ausgewählte Pixel zu erstellen
void FitGauss_toPeak(vector<TF1*>& allfits, vector<TH1D*>& allhists, TFile* f, unsigned int row, unsigned int col, float biasvoltage,float xmin = 1.17, float xmax = 1.21 , string histoprefix = "counterThresholdScanRow"){

  //just converting the float into string
  string bv = BVstring(biasvoltage);
  
  TF1 *fgaus = new TF1(("fGaus_row"+to_string(row)+"col"+to_string(col)+"_Bias"+ bv).c_str(),"gaus",1.17, 1.21); 

  char histoname[200] = "";
  sprintf(histoname,"%s%02d_Bias_%s",histoprefix.c_str(), row, (bv).c_str());
  TH2D *histo = (TH2D*)f->Get(histoname);
  TH1D *projy = histo->ProjectionY(("Hist_row"+to_string(row)+"col"+to_string(col)+"_Bias"+bv).c_str(),col+1,col+1); // sum all strips together
  for(int b = 0 ; b < projy->GetNbinsX(); b++){ // set all errors to same value
    projy->SetBinError(b, max(sqrt(projy->GetBinContent(b)), 1.0));
  }
  projy->Fit(fgaus,"0Q+");
  projy->SetXTitle("Threshold voltage (V)");
  projy->SetYTitle("Counts");
  projy->GetXaxis()->SetRangeUser(xmin, xmax);
  projy->ResetStats();
  gStyle->SetOptStat(1111);
  gStyle->SetOptFit(111);

  fgaus->SetTitle(("Gausfit Row:"+to_string(row)+" Col:"+to_string(col)+" Bias:"+ bv+"V;Threshold voltage (V)"+";Counts").c_str());
  allfits.push_back(fgaus);
  allhists.push_back(projy);
  return;

}

// Funktion um Gaus Fits für All Rows Enabled (ARE) zu erstellen
void FitGauss_toPeak_AllColsEnabled(float biasvoltage, unsigned int col, string Chip = "2", string dir = "220617counterThresholdScanMultipleRows_C", float xmin = 1.17, float xmax = 1.21 , string histoprefix = "counterThresholdScanRow100_Bias_"){

  string bv = BVstring(biasvoltage);
  string ch = "";
  if (Chip == "2"){ch = "_C2";}

  string rootfile = (dir + Chip + "/allEnabled/"+ histoprefix + bv+".root").c_str();  //Wenn ich das alte Tuning benutzen will muss ich das abändern!
  
  TFile *f = TFile::Open(rootfile.c_str());
 
  TF1 *fgaus = new TF1(("fGaus_row100col"+to_string(col)+"_Bias"+ bv).c_str(),"gaus",1.17, 1.21); 

  char histoname[200] = "";
  sprintf(histoname,"%s%s",histoprefix.c_str(), (bv).c_str());
  TH2D *histo = (TH2D*)f->Get(histoname);
  TH1D *projy = histo->ProjectionY(("Hist_row100col"+to_string(col)+"_Bias"+bv).c_str(), col+1, col+1); // sum all strips together
  for(int b = 0 ; b < projy->GetNbinsX(); b++){ // set all errors to same value
    projy->SetBinError(b, max(sqrt(projy->GetBinContent(b)), 1.0));
  }
  projy->Fit(fgaus,"0Q+");
  projy->SetXTitle("Threshold voltage (V)");
  projy->SetYTitle("Counts");
  projy->GetXaxis()->SetRangeUser(xmin, xmax);
  projy->ResetStats();
  gStyle->SetOptStat(1111);
  gStyle->SetOptFit(111);
  
  fgaus->SetTitle(("Gausfit Col: "+to_string(col)+" Bias:"+ bv +"V ARE;Threshold voltage (V)"+";Counts").c_str());

  TFile *fout = new TFile(("Gausfit_ACE" + ch + ".root").c_str(),"update");
  fout->cd();
  projy->Write(projy->GetName(),TObject::kOverwrite);
  fgaus->Write(fgaus->GetName(),TObject::kOverwrite);

  fout->Close();
  f->Close();
  delete fgaus;
  // delete projy;
  delete fout;
  delete f;
}

void FitGauss_toPeak_AllRows(float biasvoltage, string Chip = "2", string file = "220617counterThresholdScanMultipleRows_C"){

  string bv = BVstring(biasvoltage);
  string ch = "";
  if (Chip == "2"){ch = "_C2";}
  
  string rootfile = (file + Chip + "/counterThresholdScanMultipleRows_Bias_"+ bv+".root").c_str();  //Wenn ich das alte Tuning benutzen will muss ich das abändern!
  
  TFile *f = TFile::Open(rootfile.c_str());
  vector <TF1*> allfits;
  vector <TH1D*> allhists;

  for (int i=0; i < 64; i++){
    for (int j=0; j<64; j++){
      FitGauss_toPeak( allfits, allhists, f, i, j, biasvoltage);
    }
  }

  TFile *fout = new TFile(("Gausfit_bv" + bv + ch + ".root").c_str(),"update");

  for (unsigned int i=0; i< allfits.size(); i++){
    allfits[i]-> Write(allfits[i]->GetName(), TObject::kOverwrite);
    allhists[i]-> Write(allhists[i]->GetName(), TObject::kOverwrite);
  }

  fout -> Close();
  //   allhists.clear();
  // allhists.shrink_to_fit();
  //allfits.clear();
  //allfits.shrink_to_fit();
  delete fout;
  f -> Close();
  delete f;
}


// Diese Funktion liest die neuen Daten ein und  erstellt Hists und Fits zu den verschiedenen Bias-Spannungen
void FitGauss_toPeak_AllRows_AllBias(float biasmin, float biasmax, string Chip = "2", float BiasStep = 0.5, string file = "220617counterThresholdScanMultipleRows_C"){
  for (float i=biasmin; i<=biasmax; i+=BiasStep ){
    FitGauss_toPeak_AllRows( i, Chip, file);
  }
}		       

// Diese Funktion erstellt Hists und Fits zu den verschiedenen Bias-Spannungen für alle Cols enabled
void FitGauss_toPeak_AllColsEnabled_AllBias(float biasmin = 0, float biasmax = 6, string Chip = "2", float BiasStep = 0.5, string file = "220617counterThresholdScanMultipleRows_C"){
  for (float i=biasmin; i<=biasmax; i+=BiasStep ){
    for (unsigned int col = 0; col<64; col++){
      FitGauss_toPeak_AllColsEnabled( i, col, Chip, file);
    }
  }			
}

//Diese Funktion gibt einen Parameter eines Fits aus  
double GetFitParameter (int param, unsigned int row, unsigned int col, float biasvoltage, TFile* fout){
  if(param<0 || param>2) return -1.;
  string bv = BVstring(biasvoltage);
  TF1 *fgaus = (TF1*)fout->Get(("fGaus_row"+to_string(row)+"col"+to_string(col)+"_Bias"+ bv).c_str());
  double parameter = fgaus->GetParameter(param);
  fgaus -> Delete();
  if(param==2)
    return 2.3548*parameter;
  return parameter;
}

//Diese Funktion gibt das Maximum eines Fits aus  
double GetMaximum(unsigned int row, unsigned int col, float biasvoltage, TFile* fout){
  return GetFitParameter(0,row,col,biasvoltage,fout);
}
//Diese Funktion gibt den Mean eines Fits aus  
double GetMean(unsigned int row, unsigned int col, float biasvoltage, TFile* fout){
  return GetFitParameter(1,row,col,biasvoltage,fout);
}
//Diese Funktion gibt die Halbwertsbreite eines Fits aus
double GetHalbwertsbreite(unsigned int row, unsigned int col, float biasvoltage, TFile* fout){
  return GetFitParameter(2,row,col,biasvoltage,fout);
}

//Diese Funktion gibt ein 2D Histogramm des ganzen Chips aus, indem die Means, Maxima oder FWHM dargestellt sind
void Hist2D(string y, float biasvoltage, string Chip = "2", bool allenabled = false){
  
  string bv = BVstring(biasvoltage);
  string bv2 = bv.replace("p",".") + " V";

  string rootfile, YAxis, ZAxis, ch = "";
  if (Chip == "2"){ch = "_C2";}
  TH2D *hist2;
  TH1D *hist1;
  if (y == "Mean") { YAxis = "Threshold voltage [V]"; ZAxis = "Threshold voltage [V]"; }
  if (y == "Max")  { YAxis = "Number of Hits";        ZAxis = "Number of Hits"; }
  if (y == "FWHM") { YAxis = "FWHM [V]";              ZAxis = "FWHM [V]"; }
 
  if (allenabled){
    hist1 = new TH1D(("1DHisto_" + y + "_" + bv + "_ACE").c_str(), (bv2+";Column;" + YAxis).c_str(), 64, 0, 64);
    rootfile = ("Gausfit_ACE" + ch + ".root").c_str();
  }
  else {
    hist2 = new TH2D(("2DHisto_" + y + "_" + bv).c_str(),(bv2+";Column;Row;" + ZAxis).c_str(), 64, 0, 64, 64, 0, 64);
    rootfile = ("Gausfit_bv" + bv + ch + ".root").c_str();
  }

  TFile  *f = TFile::Open(rootfile.c_str());  
  gStyle->SetOptStat(0);
 
  if (allenabled){
    for (int i=0; i<64; i++){
      TF1 *f1 = (TF1*)f->Get(("fGaus_row100col"+to_string(i)+"_Bias"+bv).c_str());
      if(y == "Mean"){     
	hist1 -> SetBinContent(i+1, f1->GetParameter(1));}
      if(y == "Max"){
	hist1 -> SetBinContent(i+1, f1->GetParameter(0));}
      if(y == "FWHM"){
	hist1 -> SetBinContent(i+1, f1->GetParameter(2));}
    }
    hist1 -> GetXaxis() -> SetTickLength(0.018);
    hist1 -> GetYaxis() -> SetTickLength(0.018);
    hist1 -> GetXaxis() -> SetTitleSize(0.05);
    hist1 -> GetYaxis() -> SetTitleSize(0.05);
    hist1 -> GetXaxis() -> SetLabelSize(0.05);
    hist1 -> GetYaxis() -> SetLabelSize(0.05);
    hist1 -> Draw("colz");
    TFile *fout = new TFile(("Hists2D" + ch + ".root").c_str(),"update");   
    fout->cd();
    hist1->Write(hist1->GetName(),TObject::kOverwrite);
    fout->Close();
    delete fout; 
  }
  else {
    for (int row = 0; row<64; row++){
      for (int col = 0; col<64; col++){
	if(y == "Mean"){
	  hist2 -> SetBinContent(row+1, col+1, GetMean(row, col, biasvoltage, f));}
	if(y == "Max"){
	  hist2 -> SetBinContent(row+1, col+1, GetMaximum(row, col, biasvoltage, f));}
	if(y == "FWHM"){
	  hist2 -> SetBinContent(row+1, col+1, GetHalbwertsbreite(row, col, biasvoltage, f));}
      }
    }

    hist2 -> GetXaxis() -> SetTickLength(0.018);
    hist2 -> GetYaxis() -> SetTickLength(0.018);
    hist2 -> GetZaxis() -> SetTitleOffset(2);
    hist2 -> GetXaxis() -> SetTitleSize(0.05);
    hist2 -> GetYaxis() -> SetTitleSize(0.05);
    hist2 -> GetZaxis() -> SetTitleSize(0.05);
    hist2 -> GetXaxis() -> SetLabelSize(0.05);
    hist2 -> GetYaxis() -> SetLabelSize(0.05);
    hist2 -> GetZaxis() -> SetLabelSize(0.05);
    hist2 -> Draw("colz");
    TFile *fout = new TFile(("Hists2D" + ch + ".root").c_str(),"update");   
    fout->cd();
    hist2->Write(hist2->GetName(),TObject::kOverwrite);
    fout->Close();
    delete fout; 
  }
  f -> Close();
}

//Diese Funktion erstellt ein Histogram in dem alle Means über der Thresholdspannung dargestellt sind
void Verteilung(string y, float biasvoltage, string Chip = "2", bool allenabled = false){

  string bv = BVstring(biasvoltage);
  float xmin = 0, xmax = 1;
  string xBeschriftung = "", rootfile, ACE = "", ACE2 = "", ch = "";
  if (Chip == "2"){ch = "_C2";}
  if (y=="Mean"){
    xmin = 1.180; xmax=1.196;
    xBeschriftung = "Threshold voltage (V)";
  }
  if (y=="Max"){
    xmin = 2000; xmax=12000;
    xBeschriftung = "Number of hits";
  }
  if (y=="FWHM"){
    xmin = 0.002; xmax=0.0075;
    xBeschriftung = "Full width at half maximum (V)";
  }
  if(allenabled){
    rootfile = ("Gausfit_ACE" + ch + ".root").c_str();
    ACE = "_ACE"; ACE2 = " (ARE)";
  }
  else {
    rootfile = ("Gausfit_bv" + bv + ch + ".root").c_str();
  }

  // auto c1 = new TCanvas("c1","c1",1200,600);
  TH1D *h = new TH1D((y+"Verteilung_wholeChip"+"_"+bv+ACE).c_str(), (y+" distributon at bias voltage "+bv+"V"+ACE2+";"+xBeschriftung+";Number of Pixels").c_str() , 64, xmin , xmax);
  TFile *f = TFile::Open(rootfile.c_str());

  if (allenabled){  
    for (int col = 0; col<64; col++){
      if(y == "Mean"){
	h -> Fill(GetMean(100, col, biasvoltage, f));}
      if(y == "Max"){
	h -> Fill(GetMaximum(100, col, biasvoltage, f));}
      if(y == "FWHM"){
	h -> Fill(GetHalbwertsbreite(100, col, biasvoltage, f));}
    }
  }
  else{
    for (int row = 0; row<64; row++){
      for (int col = 0; col<64; col++){
	if(y == "Mean"){
	  h -> Fill(GetMean(row, col, biasvoltage, f));}
	if(y == "Max"){
	  h -> Fill(GetMaximum(row, col, biasvoltage, f));}
	if(y == "FWHM"){
	  h -> Fill(GetHalbwertsbreite(row, col, biasvoltage, f));}
      }
    }
  }
  TFile *fout = new TFile(("Verteilungen" + ch + ".root").c_str(),"update");
  fout->cd();
  h->Write(h->GetName(),TObject::kOverwrite);
  fout->Close();
  delete fout;
}

//Diese Funktion erstellt ein Histogram in dem alle Means eines 8er Blocks Spalten über der Thresholdspannung dargestellt sind
void Verteilung_OneStrip(string y, float biasvoltage, string Chip = "2", bool allenabled = false){

  string bv = BVstring(biasvoltage);
  float xmin = 0, xmax = 1;
  string xBeschriftung = "", rootfile, ACE="", ACE2="", ch = "";
  if (Chip == "2"){ch = "_C2";}
  if (y=="Mean"){
    xmin = 1.180; xmax=1.196;
    xBeschriftung = "Threshold voltage (V)";
  }
  if (y=="Max"){
    xmin = 2000; xmax=12000;
    xBeschriftung = "Number of hits";
  }
  if (y=="FWHM"){
    xmin = 0.002; xmax=0.0075;
    xBeschriftung = "Full width at half maximum (V)";
  }

  if (allenabled){
    rootfile = ("Gausfit_ACE" + ch + ".root").c_str();
    ACE = "_ACE"; ACE2 = "(ARE)";
  }
  else {
    rootfile = ("Gausfit_bv" + bv +ch + ".root").c_str();
  }

  for (int block = 0; block<8; block++){
    TH1D *h = new TH1D((y+"Verteilung_wholeChip"+"_"+bv+"_Block"+to_string(block)+ACE).c_str(), (y+" distributon at bias voltage "+bv+"V Block:"+to_string(block)+ACE2+";"+xBeschriftung+";Number of Pixels").c_str() , 64, xmin , xmax);
    TFile *f = TFile::Open(rootfile.c_str());

    if (allenabled){  
      for (int col = 0; col<8; col++){
	if(y == "Mean"){
	  h -> Fill(GetMean(100, block * 8 + col, biasvoltage, f));}
	if(y == "Max"){
	  h -> Fill(GetMaximum(100, block * 8 + col, biasvoltage, f));}
	if(y == "FWHM"){
	  h -> Fill(GetHalbwertsbreite(100, block * 8 + col, biasvoltage, f));}
      }
    }
    else {
      for (int row = 0; row<64; row++){
	for (int col = 0; col<8; col++){
	  if(y == "Mean"){
	    h -> Fill(GetMean(row, block * 8 + col, biasvoltage, f));}
	  if(y == "Max"){
	    h -> Fill(GetMaximum(row, block * 8 + col, biasvoltage, f));}
	  if(y == "FWHM"){
	    h -> Fill(GetHalbwertsbreite(row, block * 8 + col, biasvoltage, f));}
	}
      }
    }
    TFile *fout = new TFile(("Verteilungen" + ch + ".root").c_str(),"update");
    fout->cd();
    h->Write(h->GetName(),TObject::kOverwrite);
    fout->Close();
    delete fout;
  }
}

// Diese Funktion erstellt 2D Mean, Max, FWHM Histogramme für alle BiasSpannungen
void Hist2D_AllBias(float biasmin = 0, float biasmax = 6, string Chip = "2", bool allenabled = false, float BiasStep = 0.5, string dirname = ""){

  string y[3] = {"Mean", "Max", "FWHM"}, ch = "";
  if (Chip == "2"){ch = "_C2";}
  for (int i = 0; i<3; i++){
    TCanvas *c1 = new TCanvas("c1", (y[i]).c_str(), 200, 10, 1200, 800);
    c1->Divide(4,4);
    TPad* p; string ACE = "", D = "2D";
    for (float j=biasmin; j<=biasmax; j+=BiasStep){
      TGaxis::SetMaxDigits(3);
      p = (TPad*) c1 -> cd((int)roundf(j*2)+1);
      p -> SetRightMargin(0.18);
      p -> SetLeftMargin(0.10);
      Hist2D(y[i], j, Chip, allenabled);
    }
    if( allenabled){
      ACE = "_ACE"; D = "1D";
    }
    c1->SaveAs((dirname+ y[i] +"_"+D+"Hists"+ACE+ch+".pdf").c_str());
    c1->SaveAs((dirname+ y[i] +"_"+D+"Hists"+ACE+ch+".png").c_str());
  }
}

// Diese Funktion erstellt 2D Mean, Max, FWHM Histogramme für alle BiasSpannungen
void Hist2D_AllBias_BiggerStep(float biasmin = 0, float biasmax = 5, string Chip = "2", bool allenabled = false, float BiasStep = 1, string dirname = ""){

  string y[3] = {"Mean", "Max", "FWHM"}, ch = "";
  if (Chip == "2"){ch = "_C2";}
  gStyle->SetTitleSize(0.1,"t");
  for (int i = 0; i<3; i++){
    TCanvas *c1 = new TCanvas("c1", (y[i]).c_str()/*, 200, 10*/, 1600, 300);
    c1->Divide(6,1);
    TPad* p; string ACE = "";
    for (float j=biasmin; j<=biasmax; j+=BiasStep){
      TGaxis::SetMaxDigits(3);
      p = (TPad*) c1 -> cd((int)roundf(j)+1);
      if (allenabled){
	p -> SetLeftMargin(0.23); p -> SetRightMargin(0.02); p -> SetBottomMargin(0.15); p -> SetTopMargin(0.13);
      }
      else {
	p -> SetRightMargin(0.24); p -> SetLeftMargin(0.13);  p -> SetBottomMargin(0.22);
	p -> SetTopMargin(0.13);
      }
      Hist2D(y[i], j, Chip, allenabled);
    }
    if( allenabled){
      ACE = "_ACE";
    }
      c1->SaveAs((dirname+ y[i] +"_2DHists"+ACE+ch+"_BiggerSteps.png").c_str());
  }
}

// Diese Funktion erstellt die Verteilungen der Mean, Max, FWHM in Abhängigkeit der Biasspannung für alle BiasSpannungen
void Verteilungen_AllBias(bool blocks = false, float biasmin = 0, float biasmax = 6, string Chip = "2", bool allenabled = false, float BiasStep = 0.5, string dirname=""){
  if(blocks){
    for (float i=biasmin; i<=biasmax; i+=BiasStep){
      Verteilung_OneStrip("Mean", i, Chip, allenabled);
      Verteilung_OneStrip("Max", i, Chip, allenabled);
      Verteilung_OneStrip("FWHM", i, Chip, allenabled);
    }
  }
  else {
    for (float i=biasmin; i<=biasmax; i+=BiasStep){
      Verteilung("Mean", i, Chip, allenabled);
      Verteilung("Max", i, Chip, allenabled);
      Verteilung("FWHM", i, Chip, allenabled);
    }
  }
}

//Diese Funktion gibt die Mean, Max, FWHM in Abhängigkeit der BiasSpannung aus
void Bias_Dependency(string Chip = "2", bool allenabled = false){

  string ch = "",  y[3] = {"Mean", "Max", "FWHM"};
  if (Chip == "2"){ch = "_C2";}
  TFile *f = TFile::Open(("Verteilungen" + ch + ".root").c_str());
  
  TH1D* hist;
  TGraphErrors* gerror;
  string ACE = "", ACE2 = "", yAxis = "", title = "", bv;
  int N;
  double Bias25[25] = {0, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6};
  double Bias13[13] = {0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6};
  if(allenabled){
    ACE = "_ACE"; ACE2 = " (ARE)";
  }
  for (int k = 0; k <= 3; k++){
    if (k == 0){
      yAxis = "Average of mean threshold voltages [V]";
      title = "mean values";
      N = 13;
    }
    else if (k == 1){
      yAxis = "Average of maximum hits of all pixels";
      title = "maxima";
      N = 25;
    }
    else if (k == 2){
      yAxis = "Average of FWHMs [V]";
      title = "FWHM values";
      N = 25;
    }

    double Werte[N], ey[N];
    if (k == 0){
      for (int i = 0; i < N; i ++){
	bv = BVstring(Bias13[i]);
	hist = (TH1D*)f->Get((y[k]+"Verteilung_wholeChip_"+ bv +ACE).c_str());
	double value  = hist->GetMean(), errory = hist->GetStdDev();
	Werte[i] = value;
	ey[i] = errory;
      }
    }
    else if (k == 1 || k == 2){
      for (int i = 0; i < N; i ++){
	bv = BVstring(Bias25[i]);
	hist = (TH1D*)f->Get((y[k]+"Verteilung_wholeChip_"+ bv +ACE).c_str());
	double value  = hist->GetMean(), errory = hist->GetStdDev();
	Werte[i] = value;
	ey[i] = errory;
      }
    }

    TCanvas *c = new TCanvas("c","c",1200,800);
    c -> SetLeftMargin(0.12);
  
    if (k == 0){
      gerror = new TGraphErrors(N, Bias13, Werte, nullptr, ey);
    }
    else if (k == 1 || k == 2){
      gerror = new TGraphErrors(N, Bias25, Werte, nullptr, ey);
    }

    gerror -> GetXaxis() -> SetLimits(-0.5,6.5);  
    gerror -> GetXaxis() -> SetTitle("Bias voltage [V]");
    gerror -> GetYaxis() -> SetTitle(yAxis.c_str());
    gerror -> SetLineColor(kCyan+3);
    gerror -> SetMarkerColor(kCyan+3);
    gerror -> Draw("AC*");
  
    gerror -> SetTitle(("Dependency of the "+title+" on the bias voltage" + ACE2).c_str());
    c->SaveAs((dirname+y[k]+"_Bias_Dependency" + ACE + ch + ".png").c_str());

    TFile *fout = new TFile(("Bias_Dependency" + ch + ".root").c_str(),"update");

    fout->cd();
    gerror->Write((y[k]+"Dependency" + ACE).c_str(),TObject::kOverwrite);
    fout->Close();
    delete fout;
  }

}

//Diese Funktion gibt die Anzahl Hits über die Bias Spannung aus
void NumberofHits_Bias_Dependency(float biasmin = 0, float biasmax = 6, string Chip = "2", bool allenabled = false, float BiasStep = 0.5, string dirname=""){

  double Werte[25], ey[25], biasvoltage[25] = {0, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6};
    
  string rootfile, ACE = "", ACE2 = "", ch = "";
  if (Chip == "2"){ch = "_C2";}
 
  for (int i = 0; i <= 24; i ++){
    string bv = BVstring(biasvoltage[i]);
    if (allenabled){
      rootfile =  ("Gausfit_ACE" + ch + ".root").c_str(); ACE = "_ACE"; ACE2 = " (ARE)";
    }
    else {
      rootfile = ("Gausfit_bv" + bv + ch + ".root").c_str();
    }
    TFile *f = TFile::Open((rootfile).c_str());        
    double Hits = 0;    

    if (allenabled){
      for (int col = 0; col < 64; col += 1){
	TH1D *hist = (TH1D*)f->Get(("Hist_row100col"+to_string(col)+"_Bias"+ bv).c_str());
	double I = hist->Integral();
	Hits += I;
      }
    }
    else {
      for (int row = 0; row < 64; row += 1){
	for (int col = 0; col < 64; col += 1){
	  TH1D *hist = (TH1D*)f->Get(("Hist_row"+to_string(row)+"col"+to_string(col)+"_Bias"+ bv).c_str());
	  double I = hist->Integral();
	  Hits += I;
	}
      }
    }
    Werte[i] = Hits;
    ey[i] = sqrt(Hits);
    f -> Close();
  }
  cout << ey[1] << ey[20] << endl;
  auto *c = new TCanvas("c", "c", 1200, 800);
  //TGaxis::SetMaxDigits(3);
  TGraphErrors *g = new TGraphErrors(25, biasvoltage, Werte, nullptr, ey);

  g -> SetTitle(("Number of hits depending on the bias voltage"+ACE2).c_str());
  g -> GetXaxis() -> SetLimits(-0.5,6.5);
  g -> GetXaxis() -> SetTitle("Bias voltage [V]");
  g -> GetYaxis() -> SetTitle("number of hits");
  g -> SetLineColor(kCyan+3);
  g -> SetMarkerColor(kCyan+3);
  g -> Draw("AC*");
  c->SaveAs((dirname + "/Hits_Bias_Dependency" + ACE + ch + ".png").c_str());
 
  TFile *fout = new TFile(("Hits_Bias_Dependency" + ch + ".root").c_str(),"update");
  fout->cd();
  g->Write(("Hits_Bias_Dependency"+ACE).c_str(),TObject::kOverwrite);
  fout->Close();
  delete fout;
}

//Diese Funktion stellt das Gefälle von y von links nach rechts vom Chip dar
void Slope(bool allenabled = false, float biasmin = 0, float biasmax = 6, string Chip = "2", float BiasStep = 0.5, string dirname=""){

  string ACE = "", y[3] = {"Mean", "Max", "FWHM"}, ch = "", yAxis, title;
  if (Chip == "2"){ch = "_C2";}
  double ex[8], ey[8], colors[13] = {1,2,3,4,6,7,8,9,30,46,13,kOrange-3,kViolet+1};
  if (allenabled){ ACE = "_ACE";}
  TCanvas *c = new TCanvas("c", "Slopes", 1500, 500);
  c -> Divide(3,1);
  TPad* p;
  for (int k = 0; k<3; k++){
    if (k == 0){
      yAxis = "Average of mean threshold voltages [V]";
      title = "mean";
    }
    else if (k == 1){
      yAxis = "Average of maximum hits";
      title = "maxima";
    }
    else if (k == 2){
      yAxis = "Average of FWHMs [V]";
      title = "FWHM";
    } 
    TMultiGraph *mg = new TMultiGraph();
  
    for (float i = biasmin; i <= biasmax; i+= BiasStep){
      Double_t Werte[20], Row[20];
      string bv = BVstring(i);
      string bv2 = bv.replace("p",".") + " V";

      TFile *f = TFile::Open(("Verteilungen" + ch + ".root").c_str());

      for (int Block = 0; Block < 8; Block += 1){	
	TH1D *hist = (TH1D*)f->Get((y[k] + "Verteilung_wholeChip" + "_" + bv + "_Block" + to_string(Block) + ACE).c_str());
	Werte[Block] = hist -> GetMean();
	if ((bv == "0p0") || (bv == "1p5")){
	  ey[Block] = 0.5* hist -> GetStdDev();
	  ex[Block] = 4;
	}
	else {
	  ey[Block] = 0;
	  ex[Block] = 0;
	}
	Row[Block] = Block*8 + 4;
      }
      f -> Close();
      auto *g = new TGraphErrors(8, Row, Werte, ex, ey);
      g -> SetTitle(bv2).c_str());
      g -> SetLineColor(colors[(int)roundf(i*2)]);
      if((bv == "0p0") || (bv == "1p5")){
	g -> SetLineStyle(1);
      }
      g -> SetMarkerColor(colors[(int)roundf(i*2)]);      
      mg -> Add(g);
    }
    p = (TPad*) c -> cd(k+1);
    p -> SetRightMargin(0.25);
    p -> SetLeftMargin(0.15);
    mg -> SetTitle((title + "; column;" + yAxis).c_str());
    mg -> GetXaxis() -> SetLimits(-0.5,64.5);
    mg -> GetXaxis() -> CenterTitle(true);
    mg -> GetYaxis() -> CenterTitle(true);
    mg -> GetYaxis() -> SetDecimals();
    mg -> Draw("AC*");
    c -> cd (k+1) -> BuildLegend(0.7501522,0.1010782,0.9523509,0.8994994,"bias voltages") -> SetTextSize(0.035); 
  }
  c->SaveAs((dirname +"/Slopes"+ACE+ ch +".png").c_str());
  TFile *fout = new TFile(("Hists2D" + ch +".root").c_str(),"update");
  fout->cd();
  c->Write(("Slopes" + ch).c_str(),TObject::kOverwrite);
  fout->Close();
  delete fout;
}

//Diese Funktion stellt das Gefälle von y von links nach rechts vom Chip aber nur für 2 bias spannungen dar
void Slope_2bias(bool allenabled = true, string Chip = "2", float BiasStep = 0.5, string dirname = ""){

  string ACE = "", y[3] = {"Mean", "Max", "FWHM"}, ch = "", yAxis, title;
  if (Chip == "2"){ch = "_C2";}
  double ex[8], ey[8], colors[2] = {kOrange-3,kViolet+1};
  if (allenabled){ ACE = "_ACE";}
  TCanvas *c = new TCanvas("c", "Slopes", 1500, 500);
  c -> Divide(3,1);
  TPad* p;
  for (int k = 0; k<3; k++){
    if (k == 0){
      yAxis = "Average of mean threshold voltages [V]";
      title = "mean";
    }
    else if (k == 1){
      yAxis = "Average of maximum hits";
      title = "maxima";
    }
    else if (k == 2){
      yAxis = "Average of FWHMs [V]";
      title = "FWHM";
    }   
    TMultiGraph *mg = new TMultiGraph();
    for (float i = 1; i < 1.6; i+= BiasStep){
      Double_t Werte[20], Row[20];
      string bv = BVstring(i);
      string bv2 = bv.replace("p",".") + " V";
      TFile *f = TFile::Open(("Verteilungen" + ch + ".root").c_str());
      for (int Block = 0; Block < 8; Block += 1){
	TH1D *hist = (TH1D*)f->Get((y[k] + "Verteilung_wholeChip" + "_" + bv + "_Block" + to_string(Block) + ACE).c_str());
	Werte[Block] = hist -> GetMean();
	ey[Block] = 0.5* hist -> GetStdDev();
	ex[Block] = 4;
	Row[Block] = Block*8 + 4;
      }
      f -> Close();
      cout << Werte[1] << Werte[7] << endl;

      auto *g = new TGraphErrors(8, Row, Werte, ex, ey);
      g -> SetTitle((bv2).c_str());
      g -> SetLineColor(colors[(int)roundf(i*2)]);
      g -> SetMarkerColor(colors[(int)roundf(i*2)]);      
      mg -> Add(g);
    }
    p = (TPad*) c -> cd(k+1);
    p -> SetRightMargin(0.25);
    p -> SetLeftMargin(0.15);
    mg -> SetTitle((title + "; column;" + yAxis).c_str());
    mg -> GetXaxis() -> SetLimits(-0.5,64.5);
    mg -> GetXaxis() -> CenterTitle(true);
    mg -> GetYaxis() -> CenterTitle(true);
    mg -> GetYaxis() -> SetDecimals();
    mg -> Draw();
    c -> cd (k+1) -> BuildLegend(0.7501522,0.1010782,0.9523509,0.8994994,"bias voltages") -> SetTextSize(0.035); 

  }
  c->SaveAs((dirname+"/Slopes"+ACE+ ch +"_2bias.png").c_str());
   
  TFile *fout = new TFile(("Hists2D" + ch +".root").c_str(),"update");
  fout->cd();
  c->Write(("Slopes" + ch + "_2bias").c_str(),TObject::kOverwrite);
  fout->Close();
  delete fout;
}

//____________________________________________________________________
double fitfunc(double * x, double * par) {
  // par[0] = a, par[1] = mu, par[2] = sigma, par[3] = offset
  return 0.5*par[0]*(1.0+ROOT::Math::erf(/*(x[0]-par[1])/par[2])*/par[1]))+par[2];
}
//____________________________________________________________________

void Bias_Dependency_source(string dirname=""){
  gStyle->SetOptStat();

  double biasvoltage[19] = {0,0.2,0.4,0.6,0.8,1.0,1.2,1.4,1.6,1.8,2,2.5,3,3.5,4,4.5,5,5.5,6};
  double Werte1[19], Werte2[19], Werte3[19], Werte4[19], ey1[19], ey2[19], ey3[19], ey4[19], Wert1, Wert2, Wert3, Wert4;

  for (int i = 0; i <= 18; i++){
    string bv = BVstring(biasvoltage[i]);

    TFile *f = TFile::Open(("220628counterThresholdScan_cource_C2_092_108/counterThresholdScanRow100_Bias_" + bv + ".root").c_str());
    TFile *ff = TFile::Open(("220630counterThresholdScan_source_C2_084_1/counterThresholdScanRow100_Bias_" + bv + ".root").c_str());
    TH2D *histo = (TH2D*)f->Get(("counterThresholdScanRow100_Bias_" + bv).c_str());
    TH2D *histo2 = (TH2D*)ff->Get(("counterThresholdScanRow100_Bias_" + bv).c_str());
 
    Wert1 = 0; Wert2 = 0; Wert3 = 0; Wert4 = 0;
    for(int a = 0 ; a < 64; a++){
      Wert1 += 0.5*(histo2 -> GetBinContent(a,1));
      Wert2 += histo -> GetBinContent(a,1);
      Wert3 += 0.5*(histo2 -> GetBinContent(a,2));
      Wert4 += histo -> GetBinContent(a,2);
    }
    Werte1[i] = Wert1;
    Werte2[i] = Wert2;
    Werte3[i] = Wert3;
    Werte4[i] = Wert4;
    if (Wert1 == 0){
      ey1[i] = 0;
    }
    else{
      ey1[i] = sqrt(Wert1);
    }
    if (Wert2 == 0){
      ey2[i] = 0;
    }
    else{
      ey2[i] = sqrt(Wert2);
    }
    if (Wert3 == 0){
      ey3[i] = 0;
    }
    else{
      ey3[i] = sqrt(Wert3);
    }
    if (Wert4 == 0){
      ey4[i] = 0;
    }
    else{
      ey4[i] = sqrt(Wert4);
    }
    f -> Close();
    ff -> Close();
  }

  auto c = new TCanvas("c","c",1200,700);
  TGraphErrors *g1 = new TGraphErrors(19, biasvoltage, Werte1, nullptr, ey1);
  g1 -> SetLineColor(kCyan+3);
  g1 -> SetMarkerColor(kCyan+3);
  g1 -> SetTitle("0.84V");
  auto f1 = new TF1("f1", "pol1", 2, 6);
  f1 -> SetLineColor(kCyan);
  g1 -> Fit("f1","R","",2,6);
  
  TGraphErrors *g2 = new TGraphErrors(19, biasvoltage, Werte2, nullptr, ey2);
  g2 -> SetLineColor(kMagenta+3);
  g2 -> SetMarkerColor(kMagenta+3);
  g2 -> SetTitle("0.92V");
  auto f2 = new TF1("f2", "pol1", 2, 6);
  f2 -> SetLineColor(kMagenta);
  g2 -> Fit("f2","R","",2,6);

  TGraphErrors *g3 = new TGraphErrors(19, biasvoltage, Werte3, nullptr, ey3);
  g3 -> SetLineColor(kOrange+3);
  g3 -> SetMarkerColor(kOrange+3);
  g3 -> SetTitle("1.00V");
  auto f3 = new TF1("f3", "pol1", 2, 6);
  f3 -> SetLineColor(kOrange);
  g3 -> Fit("f3","R","",2,6);

  TGraphErrors *g4 = new TGraphErrors(19, biasvoltage, Werte4, nullptr, ey4);
  g4 -> SetLineColor(kBlue+3);
  g4 -> SetMarkerColor(kBlue+3);
  g4 -> SetTitle("1.08V");
  auto f4 = new TF1("f4", "pol1", 2, 6);
  f4 -> SetLineColor(kBlue);
  g4 -> Fit("f4","R","",2,6);

  TMultiGraph *gr = new TMultiGraph();
  gr -> SetTitle("Number of hits depending on the bias voltage (Sr90 source);Bias voltage [V]; number of hits");
  gr -> Add(g1);
  gr -> Add(g2);
  gr -> Add(g3);
  gr -> Add(g4);
  gr -> GetXaxis() -> SetRangeUser(-0.5, 6.5);
  gr -> GetXaxis() -> CenterTitle(true);
  gr -> GetYaxis() -> CenterTitle(true);
  gr -> Draw("A*");
  c -> BuildLegend(0.1444073,0.7002967,0.3447412,0.8753709,"threshold voltages") -> SetTextSize(0.035);
  c -> SaveAs(dirname+"/Bias_Dependency_source_ARE_C2.png");
}

void Bias_Dependency_source_lowBias(string dirname=""){
  gStyle->SetOptStat();
  double biasvoltage[6] = {0,0.2,0.4,0.6,0.8,1.0}, Werte1[6], Werte2[6], Werte3[6], Werte4[6], ey1[6], ey2[6], ey3[6], ey4[6], Wert1, Wert2, Wert3, Wert4;
  for (int i = 0; i <= 5; i++){
    string bv = BVstring(biasvoltage[i]);
 
    TFile *f = TFile::Open(("220628counterThresholdScan_cource_C2_092_108/counterThresholdScanRow100_Bias_" + bv + ".root").c_str());
    TFile *ff = TFile::Open(("220630counterThresholdScan_source_C2_084_1/counterThresholdScanRow100_Bias_" + bv + ".root").c_str());
    TH2D *histo = (TH2D*)f->Get(("counterThresholdScanRow100_Bias_" + bv).c_str());
    TH2D *histo2 = (TH2D*)ff->Get(("counterThresholdScanRow100_Bias_" + bv).c_str());
 
    Wert1 = 0; Wert2 = 0; Wert3 = 0; Wert4 = 0;
    for(int a = 0 ; a < 64; a++){
      Wert1 += 0.5*(histo2 -> GetBinContent(a,1));
      Wert2 += histo -> GetBinContent(a,1);
      Wert3 += 0.5*(histo2 -> GetBinContent(a,2));
      Wert4 += histo -> GetBinContent(a,2);
    }
    Werte1[i] = Wert1;
    Werte2[i] = Wert2;
    Werte3[i] = Wert3;
    Werte4[i] = Wert4;

    if (Wert1 == 0){
      ey1[i] = 0;
    }
    else{
      ey1[i] = sqrt(Wert1);
    }
    if (Wert2 == 0){
      ey2[i] = 0;
    }
    else{
      ey2[i] = sqrt(Wert2);
    }
    if (Wert3 == 0){
      ey3[i] = 0;
    }
    else{
      ey3[i] = sqrt(Wert3);
    }
    if (Wert4 == 0){
      ey4[i] = 0;
    }
    else{
      ey4[i] = sqrt(Wert4);
    }
    f -> Close();
    ff -> Close();
  }

  auto c = new TCanvas("c","c",1200,700);
  TGraphErrors *g1 = new TGraphErrors(6, biasvoltage, Werte1, nullptr, ey1);
  g1 -> SetLineColor(kCyan+3);
  g1 -> SetMarkerColor(kCyan+3);
  g1 -> SetTitle("0.84V");
  
  TGraphErrors *g2 = new TGraphErrors(6, biasvoltage, Werte2, nullptr, ey2);
  g2 -> SetLineColor(kMagenta+3);
  g2 -> SetMarkerColor(kMagenta+3);
  g2 -> SetTitle("0.92V");

  TGraphErrors *g3 = new TGraphErrors(6, biasvoltage, Werte3, nullptr, ey3);
  g3 -> SetLineColor(kOrange+3);
  g3 -> SetMarkerColor(kOrange+3);
  g3 -> SetTitle("1.00V");
  
  TGraphErrors *g4 = new TGraphErrors(6, biasvoltage, Werte4, nullptr, ey4);
  g4 -> SetLineColor(kBlue+3);
  g4 -> SetMarkerColor(kBlue+3);
  g4 -> SetTitle("1.08V");
  
  TMultiGraph *gr = new TMultiGraph();
  gr -> SetTitle("Number of hits depending on the bias voltage (Sr90 source);Bias voltage [V]; number of hits");
  gr -> Add(g1);
  gr -> Add(g2);
  gr -> Add(g3);
  gr -> Add(g4);
  gr -> GetXaxis() -> SetRangeUser(-0.2, 1.2);
  gr -> GetXaxis() -> CenterTitle(true);
  gr -> GetYaxis() -> CenterTitle(true);
  gr -> Draw("AC*");
  c -> BuildLegend(0.1444073,0.7002967,0.3447412,0.8753709,"threshold voltages") -> SetTextSize(0.035);
  c -> SaveAs(dirname+"/Bias_Dependency_source_ARE_C2_lowBias.png");
}

void Spektrum(float biasvoltage = 0.5, bool only_spectrum = false, string dirname=""){

  string bv = BVstring(biasvoltage);
  string rootfile;
  double Werte;
  rootfile = ("Sr90_" + bv + "V_C2_Spektrum/Sr90_" + bv + "V_MainScan.root").c_str();
  auto c1 = new TCanvas("c1","c1",900,600);
  TFile *f = TFile::Open(rootfile.c_str());
  TH2D *histo = (TH2D*)f -> Get("counterThresholdScanRow100");
  TH1D *hist;
  hist = histo -> ProjectionY(("Spektrum_" + bv + "V").c_str(), 1, 64);
  hist -> SetStats(0);
  hist -> SetTitle("");
  hist -> SetXTitle("Threshold voltage (V)");
  hist -> SetYTitle("Counts");
  hist -> SetFillColor(kCyan+3);
  if (biasvoltage < 1.2){
    hist -> SetAxisRange(0.8,1.3);
  }
  else{
    hist -> SetAxisRange(0.6,1.3);
  }
  c1 -> SetLogy();
  hist -> Draw("hist");
  if (only_spectrum) {
    hist -> SetMaximum(10000);
    c1 -> SaveAs((dirname+"/SpektrumSr90_"+bv+"V_logarithmic_onlySpectrum.png").c_str());
  }
  else{
    c1 -> SaveAs((dirname+"/SpektrumSr90_"+bv+"V_logarithmic.png").c_str());
    c1 -> SetLogy(0);
    hist -> SetTitle("linear scale");
    hist -> SetMaximum(2000);
    c1 -> SaveAs((dirname+"/SpektrumSr90_"+bv+"V_linear.png").c_str());
  }
  
  TFile *fout = new TFile("Spektren.root","update");
  fout -> cd();
  hist -> Write(hist -> GetName(),TObject::kOverwrite);
  fout -> Close();
  delete fout;
}

//This function gives out the shape of the nois peak for different bias voltages
void shape_of_noise_peak(string Chip = "2", string dirname=""){
  double biasvoltages[4] = {0.5,1,1.5,6};
  float biasvoltage;
  string rootfile;
  for (int i = 0; i < 4; i++){
    biasvoltage = biasvoltages[i];
    string bv = BVstring(biasvoltage);
    rootfile = ("220617counterThresholdScanMultipleRows_C"+Chip+"/allEnabled/counterThresholdScanRow100_Bias_"+bv+".root").c_str();
  
    auto c1 = new TCanvas("c1","c1",900,600);
    TFile *f = TFile::Open(rootfile.c_str());
    TH2D *histo = (TH2D*)f -> Get(("counterThresholdScanRow100_Bias_"+bv).c_str());
    TH1D *hist;
    hist = histo -> ProjectionY(("Noise_peak_" + bv + "V").c_str(), 1, 64);
    TF1 *fgaus = new TF1(("fGaus_Bias"+ bv).c_str(),"gaus",1.183, 1.191); 
    //hist -> SetStats(0);
    hist -> SetTitle("");
    hist -> SetXTitle("Threshold voltage (V)");
    hist -> SetYTitle("Counts");
    hist -> SetFillColor(kCyan+3);
    hist -> SetAxisRange(1.05,1.3);
    hist -> Fit(("fGaus_Bias"+ bv).c_str(),"R"); //"0Q+");
    hist -> Draw();
    fgaus -> Draw("same");
    c1 -> SaveAs((dirname+"/Noise_peak_"+bv+".png").c_str());

    TFile *fout = new TFile("Noise_peaks.root","update");
    fout -> cd();
    hist -> Write(hist -> GetName(),TObject::kOverwrite);
    fout -> Close();
    delete fout;
  }
}

