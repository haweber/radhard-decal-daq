// Follwing include lines have to commented in to operate with the compiled version of DECAL related Objects
#include "../stdll/DecalConfig.h"
#include "DecalMotherboard.h"
#include "TH2F.h"
#include <fstream>
#include <unistd.h>
#include <vector>
#include <string>

using namespace std;

void pon(bool strip=true){
  startUP(strip);
  setManualPhase(true, 8);
  setupAnalog();
}

void poff(){
  setManualPhase(true, 8);
  powerDOWN(0);
  //setVDAC(14, 3, 0.); //< VGuardRing
  //setVDAC(14, 2, 0.); //< VShaperCascN
  //setVDAC(14, 1, 0.); //< Threshold
  //setVDAC(14, 0, 0., true); //< Bias Voltage
  //setAllCurrentDACs((float) 0.);
}

void setDACperPix(unsigned int col, unsigned int row, unsigned int DAC, bool polarity, bool mask, ChipConfig *config) {
  if (config == NULL) {config = new ChipConfig();}
  config->setDACPix(col, row, DAC, polarity, mask);
}

void configureColumnOFF() {
	printf("Masking all Pixels\n");
	ColumnConfig *cols = new ColumnConfig(1);
	for (int i=0;i<64;i++) {
		std::cout<< "Generated Pixel " << i << " with configuration " << int(cols->getPixConfig(i)->getConfig()) << std::endl;
	}
	writeColumn(cols);
//	debugConfig(data);
	printf("That was a full Column Config\n");
}

void configureChipOFF() {
	printf("Masking all Pixels\n");
	ColumnConfig *cols = new ColumnConfig(1);
//	debugConfig(data);
	for (int i=0; i<63; i++) {
		writeColumn(cols);
	}
	printf("That was a full Chip Config\n");
}

// Write a threshold scan:
// Configure one Pixel on
// set Threshold over full range
// at each threshold setting run the following
// captureData with parameters for a large set of data, e.g. 64 data words to be returned.
// sum up numbers per channel, then histogram

void thresholdScan_PadMode(unsigned int nSteps, unsigned int row, unsigned int col, float range, float offset, bool resetCalib, int nStrobes, unsigned int config) {
	ChipConfig *cfg = new ChipConfig(config);
	writeChip(cfg);
	double step=(range/((double) nSteps-1))/2.0;
	TH2D *myHisto = new TH2D("ThresholdScan", "ThresholdScan", 4, -0.5, 3.5, nSteps, offset-step, offset+range+step);
	TH2D *myOverflowHisto = new TH2D("OverflowThresholdScan", "OverflowThresholdScan", 4, -0.5, 3.5, nSteps, offset-step, offset+range+step);
	std::vector<StreamData> toBeHistogrammed;
	operatePixels(true);
	calibratePixels(true);
	calibratePixels(false);
	operatePixels(false);
	for(unsigned int i=0; i<nSteps; i++) {
		setThreshold((float) ((range/(nSteps-1)*i)+offset));
		std::cout << "Threshold step: " << std::dec << i << " Value is: " << (float) ((range/(nSteps-1)*i)+offset);
		unsigned int nHits=0;
		unsigned int nOverflow=0;
		if (resetCalib) {
			operatePixels(true);
			calibratePixels(true);
			calibratePixels(false);
			operatePixels(false);
		}
		captureData(toBeHistogrammed, nStrobes, 16, true);
    //pad mode, data comes in on channels 3,7,11,15, overflow on 2,6,10,14
    	for (unsigned int k=0; k < toBeHistogrammed.size(); k++) {//loop over channels
    		if(k%4!=3){continue;} //ignore these channels, don't contain data in pad mode
			for(unsigned int m=0; m<((unsigned int) nStrobes*7); m++) {
				int hits=toBeHistogrammed[k].getRawData(m/7).getHit(m%7+1);
				int overflow=toBeHistogrammed[k-1].getRawData(m/7).getHit(m%7+1);
				myHisto->Fill((float)(3-(k-3)/4), (float) ((range/(nSteps-1)*i)+offset),hits);
				myOverflowHisto->Fill((float)(3-(k-3)/4), (float) ((range/(nSteps-1)*i)+offset),hits);
				nHits+=hits;
				nOverflow+=overflow;
			}
		}
		std::cout << " Hit Count was: " << nHits <<" Oveflow was: "<<nOverflow<< std::endl;
		toBeHistogrammed.clear();
	}
	myOverflowHisto->Draw("COLZ");
	myOverflowHisto->SaveAs("OverflowThresholdScan.root");
	myHisto->Draw("COLZ");
	myHisto->SaveAs("ThresholdScan.root");
}

void thresholdScan(unsigned int nSteps, unsigned int row, unsigned int col, float range, float offset, bool resetCalib, int nStrobes, unsigned int config, unsigned int delay) {
	ChipConfig *cfg = new ChipConfig(config);
	writeChip(cfg);
	double step=(range/((double) nSteps-1))/2.0;
	TH2D *myHisto = new TH2D("ThresholdScan", "ThresholdScan", 64, -0.5, 63.5, nSteps, offset-step, offset+range+step);
	std::vector<StreamData> toBeHistogrammed;
	operatePixels(true);
	calibratePixels(true);
	calibratePixels(false);
	operatePixels(false);
	for(unsigned int i=0; i<nSteps; i++) {
		setThreshold((float) ((range/(nSteps-1)*i)+offset));
		std::cout << "Threshold step: " << std::dec << i << " Value is: " << (float) ((range/(nSteps-1)*i)+offset);
		unsigned int nHits=0;
		captureData(toBeHistogrammed, nStrobes, 16, true, resetCalib, delay);
// And here is where the histogramming should happen over toBeHistogrammed[index]->getRawData(N[0-99])...
		for (unsigned int k=0; k < toBeHistogrammed.size(); k++) {
			for(unsigned int m=0; m<((unsigned int) nStrobes*7); m++) {
				StripHit *hit = new StripHit(toBeHistogrammed[k].getRawData(m/7).getHit(m%7+1));
				for (unsigned int p=0;p<4;p++) {
					if (hit->getSingleHit(p) > 0) {
						myHisto->Fill((float) (p+(15-k)*4), (float) ((range/(nSteps-1)*i)+offset), hit->getSingleHit(p));
						nHits+=hit->getSingleHit(p) ;
					}
				}
			}
		}
		std::cout << " Hit Count was: " << nHits << std::endl;
		toBeHistogrammed.clear();
	}
	myHisto->Draw("COLZ");
	myHisto->SaveAs("ThresholdScan.root");
	delete cfg;
	cfg = 0;
}


void testRegisterScan(unsigned int phase) {
	// Check capLength
	unsigned int capLength = 1;
	printf("Scanning test register values versus output\n");
	char histoname[200] = "";
	sprintf(histoname,"ThresholdScanRow%03d",phase);
	char filename[200] = "";
	sprintf(filename,"TestRegisterScanPhase%03d.root",phase);
	char histoTitle[200] = "";
	sprintf(histoTitle,"Testregister Scan Phase %2d;Column Number;Injection Number",phase);
	TH2D *myHisto = new TH2D(histoname, histoTitle, 64, -0.5, 63.5, 66, -0., 65.5);
	std::vector<StreamData> v;
	v.clear();
	setupDigital();
	setManualPhase(true, phase);
	reset_testVector();
	for(int nInjection=0; nInjection<66;nInjection++) {
		if (!nInjection) {
			inject_number(1,1);
		} else {
			inject_number(0,1);
		}
		captureData(v, capLength, 16, true, true, 0);
		for (unsigned int k=0; k < v.size(); k++) {
			for(unsigned int m=0; m<((unsigned int) capLength*7); m++) {
				StripHit *hit = new StripHit(v[k].getRawData(m/7).getHit(m%7+1));
				for (unsigned int p=0;p<4;p++) {
					if (hit->getSingleHit(p)) {
						myHisto->Fill((float) (p+(15-k)*4), (float) nInjection);
					}
				}
			}
		}
		v.clear();
	}
	myHisto->Draw("COLZ");
	myHisto->SaveAs(filename);
}

void testRegisterFineScan(unsigned int phase) {
	// Check capLength
	unsigned int capLength = 1;
	unsigned int nDelay = 32;
	printf("Scanning test register values versus output\n");
	char histoname[200] = "";
	sprintf(histoname,"TestRegisterScanFinePhase%03d",phase);
	char filename[200] = "";
	sprintf(filename,"TestRegisterScanFinePhase%03d.root",phase);
	char histoTitle[200] = "";
	sprintf(histoTitle,"Testregister Scan Fine at Coarse Phase %2d;Fine Phase Setting;Column",phase);
	TH2D *myHisto = new TH2D(histoname, histoTitle, nDelay, -0., ((float) nDelay - 0.5), 64, -0.5, 63.5);
	std::vector<StreamData> v;
	v.clear();
	setupDigital();
	setManualPhase(true, phase);
	reset_testVector();
	for(int k=0; k<16; k++) {
		inject_number(1,1);
		inject_number(0,1);
		inject_number(1,1);
		inject_number(0,1);
	}
	for(int delay=0; delay<nDelay;delay++) {
		for (int k=0;k<16;k++) {
			e->ConfigureVariable(10009,(((k&0x3f)<<8) | (0xff & delay)));
		}
		captureData(v, capLength, 16, true, true, 0);
		for(unsigned int m=0; m<((unsigned int) capLength*7); m++) {
			for (int k=0;k<16;k++) {
				StripHit *hit = new StripHit(v[k].getRawData(m/7).getHit(m%7+1));
				for (unsigned int p=0;p<4;p++) {
					if (hit->getSingleHit(p)) {
						myHisto->Fill((float) delay, (p+(15-k)*4));
					}
				}
			}
		}
		v.clear();
	}
	myHisto->Draw("COLZ");
	myHisto->SaveAs(filename);
}


void thresholdScanHalfCorrect(unsigned int nSteps, float range, float offset, bool resetCalib, int nStrobes, unsigned int config, unsigned int row=64, unsigned int col=0, unsigned int delay=0) {
	ColumnConfig *cfg = 0;
	if (row < 64) {
		cfg = new ColumnConfig(0);
		cfg->getPixConfig(row)->setConfig(config);
	} else {
		cfg = new ColumnConfig(config);
	}
	ColumnConfig *dummyCfg = new ColumnConfig(0);
	for (unsigned int i=0;i<63;i++) {
		if (i%2 != (col%2)) { // by picking col one can choose to configure odd or even columns
			writeColumn(dummyCfg, 2, true);
		} else {
			writeColumn(cfg);
		}
	}
	double step=(range/((double) nSteps-1))/2.0;
	TH2D *myHisto = new TH2D("ThresholdScan", "ThresholdScan", 64, -0.5, 63.5, nSteps, offset-step, offset+range+step);
	std::vector<StreamData> toBeHistogrammed;
	operatePixels(true);
	calibratePixels(true);
	calibratePixels(false);
	operatePixels(false);
	for(unsigned int i=0; i<nSteps; i++) {
		setThreshold((float) ((range/(nSteps-1)*i)+offset));
		std::cout << "Threshold step: " << std::dec << i << " Value is: " << (float) ((range/(nSteps-1)*i)+offset);
		unsigned int nHits=0;
		if (delay > 0) {
			captureData(toBeHistogrammed, nStrobes, 16, true, resetCalib, delay);
		} else {
			if (resetCalib) {
				operatePixels(true);
				calibratePixels(true);
				calibratePixels(false);
				operatePixels(false);
			}
			captureData(toBeHistogrammed, nStrobes, 16, true, resetCalib, delay);
		}
// And here is where the histogramming should happen over toBeHistogrammed[index]->getRawData(N[0-99])...
		for (unsigned int k=0; k < toBeHistogrammed.size(); k++) {
			for(unsigned int m=0; m<((unsigned int) nStrobes*7); m++) {
				StripHit *hit = new StripHit(toBeHistogrammed[k].getRawData(m/7).getHit(m%7+1));
				for (unsigned int p=0;p<4;p++) {
					if (hit->getSingleHit(p)) {
						myHisto->Fill((float) (p+(15-k)*4), (float) ((range/(nSteps-1)*i)+offset));
						nHits++;
					}
				}
			}
		}
		std::cout << " Hit Count was: " << nHits << std::endl;
		toBeHistogrammed.clear();
	}
	myHisto->Draw("COLZ");
	myHisto->SaveAs("ThresholdScan.root");
	delete cfg;
	cfg = 0;
}

void FirstColCfgTest(unsigned int nSteps, unsigned int row, float range, float offset, bool resetCalib, int nStrobes) {

	double step=(range/((double) nSteps-1))/2.0;
	TH2D *myHisto = new TH2D("ThresholdScan", "ThresholdScan", 32, -0.5, 31.5, nSteps, offset-step, offset+range+step);
	ChipConfig *cfg = new ChipConfig(0x00);

	for(unsigned int config=0; config<32; config++)
	{
      //reset the chip cfg to 0x00
		writeChip(cfg);

      //write first col
		std::cout<<"Config is "<<config<<std::endl;
		ColumnConfig *colcfg = 0;
		if(config < 16) {
			colcfg = new ColumnConfig(reverseNibble(((15-config) & 0xf)));
		} else {
			colcfg = new ColumnConfig(reverseNibble((config & 0xf)) | 0x10);
		}
		writeColumn(colcfg);

		std::vector<StreamData> toBeHistogrammed;
		for(unsigned int i=0; i<nSteps; i++) {
			setThreshold((float) ((range/(nSteps-1)*i)+offset));
	//std::cout << "Threshold step: " << std::dec << i << " Value is: " << (float) ((range/(nSteps-1)*i)+offset);
			unsigned int nHits=0;
			if (resetCalib) {
				operatePixels(true);
				calibratePixels(true);
				calibratePixels(false);
				operatePixels(false);
			}
			captureData(toBeHistogrammed, nStrobes, 16, true);
	// And here is where the histogramming should happen over toBeHistogrammed[index]->getRawData(N[0-99])...
			for (unsigned int k=0; k < 1; k++) {
				for(unsigned int m=0; m<((unsigned int) nStrobes*7); m++) {
					StripHit *hit = new StripHit(toBeHistogrammed[k].getRawData(m/7).getHit(m%7+1));
					for (unsigned int p=3;p<4;p++) {
						if (hit->getSingleHit(p) && (float)(p+(15-k)*4)>62.5) {
							myHisto->Fill(config, (float) ((range/(nSteps-1)*i)+offset));
							nHits++;
						}
					}
					delete hit;
				}
			}
	//std::cout << " Hit Count was: " << nHits << std::endl;
			toBeHistogrammed.clear();
		}
		delete colcfg;
	}
	myHisto->Draw("COLZ");
	myHisto->SaveAs("ThresholdScan.root");
}

void thresholdScanConfig(unsigned int nSteps, float range, float offset, bool resetCalib, int nStrobes, ChipConfig *config) {
// Some examples of what could be done with a configuration
	writeChip(config);
	double step=(range/((double) nSteps-1))/2.0;
	TH2D *myHisto = new TH2D("ThresholdScan", "ThresholdScan", 64, -0.5, 63.5, nSteps, offset-step, offset+range+step);
	std::vector<StreamData> toBeHistogrammed;
	operatePixels(true);
	calibratePixels(true);
	calibratePixels(false);
	operatePixels(false);
	for(unsigned int i=0; i<nSteps; i++) {
		setThreshold((float) ((range/(nSteps-1)*i)+offset));
		std::cout << "Threshold step: " << std::dec << i << " Value is: " << (float) ((range/(nSteps-1)*i)+offset);
		unsigned int nHits=0;
		if (resetCalib) {
			operatePixels(true);
			calibratePixels(true);
			calibratePixels(false);
			operatePixels(false);
		}
		captureData(toBeHistogrammed, nStrobes, 16, true);
// And here is where the histogramming should happen over toBeHistogrammed[index]->getRawData(N[0-99])...
		for (unsigned int k=0; k < toBeHistogrammed.size(); k++) {
			for(unsigned int m=0; m<((unsigned int) nStrobes*7); m++) {
				StripHit *hit = new StripHit(toBeHistogrammed[k].getRawData(m/7).getHit(m%7+1));
				for (unsigned int p=0;p<4;p++) {
					if (hit->getSingleHit(p)) {
						myHisto->Fill((float) (p+(15-k)*4), (float) ((range/(nSteps-1)*i)+offset));
						nHits++;
					}
				}
			}
		}
		std::cout << " Hit Count was: " << nHits << std::endl;
		toBeHistogrammed.clear();
	}
	myHisto->Draw("COLZ");
	myHisto->SaveAs("ThresholdScan.root");
}

void thresholdScanRow(unsigned int nSteps, unsigned int row, float range, float offset, bool resetCalib, int nStrobes, ChipConfig *config/*, bool singlerow = true, bool update = true*/) {
// Some examples of what could be done with a configuration
	if (config == NULL) {config = new ChipConfig();}
	config->disable();
	if (row < 64) {
		config->enableRow(row);
	}
	writeChip(config);
	double step=(range/((double) nSteps-1))/2.0;
	char histoname[200] = "";
	sprintf(histoname,"ThresholdScanRow%02d",row);
	char filename[200] = "";
	//if(singlerow)
	sprintf(filename,"ThresholdScanRow%02d.root",row);
	//else
	//  sprintf(filename,"ThresholdScanRow.root");
	//char fileoption[200] = "";
	//if(update)
	//  sprintf(fileoption,"update");
	//else
	//  sprintf(fileoption,"recreate");
	TH2D *myHisto = new TH2D(histoname, histoname, 64, -0.5, 63.5, nSteps, offset-step, offset+range+step);
	std::vector<StreamData> toBeHistogrammed;
	operatePixels(true);
	calibratePixels(true);
	calibratePixels(false);
	operatePixels(false);
	for(unsigned int i=0; i<nSteps; i++) {
		setThreshold((float) ((range/(nSteps-1)*i)+offset));
		std::cout << "Threshold step: " << std::dec << i << " Value is: " << (float) ((range/(nSteps-1)*i)+offset);
		unsigned int nHits=0;
//		if (resetCalib) {
//			operatePixels(true);
//			calibratePixels(true);
//			calibratePixels(false);
//			operatePixels(false);
//		}
		captureData(toBeHistogrammed, nStrobes, 16, true, resetCalib, 0);
// And here is where the histogramming should happen over toBeHistogrammed[index]->getRawData(N[0-99])...
		for (unsigned int k=0; k < toBeHistogrammed.size(); k++) {
			for(unsigned int m=0; m<((unsigned int) nStrobes*7); m++) {
				StripHit *hit = new StripHit(toBeHistogrammed[k].getRawData(m/7).getHit(m%7+1));
				for (unsigned int p=0;p<4;p++) {
					if (hit->getSingleHit(p)) {
						myHisto->Fill((float) (p+(15-k)*4), (float) ((range/(nSteps-1)*i)+offset));
						nHits++;
					}
				}
				//delete hit;
			}
		}
		std::cout << " Hit Count was: " << nHits << std::endl;
		toBeHistogrammed.clear();
	}
	myHisto->Draw("COLZ");
	myHisto->SaveAs(filename);
	//TFile *myfile = new TFile(filename,fileoption);
	//myfile->cd();
	//myHisto->Write(myHisto->GetName(),TObject::kOverwrite);
	//myfile->Close();
	//delete myHisto;
	//delete myfile;
}


void thresholdScanChip(unsigned int nSteps, unsigned int row, float range, float offset, bool resetCalib, int nStrobes, ChipConfig *config) {
// Some examples of what could be done with a configuration
	if (config == NULL) {config = new ChipConfig();}
	config->disable();
	if (row < 64) {
		config->enableRow(row);
	}
	writeChip(config);
	double step=(range/((double) nSteps-1))/2.0;
	char histoname[200] = "";
	sprintf(histoname,"ThresholdScanRow%02d",row);
	char filename[200] = "";
	sprintf(filename,"ThresholdScanRow%02d.root",row);
	TH2D *myHisto = new TH2D(histoname, histoname, 64, -0.5, 63.5, nSteps, offset-step, offset+range+step);
	std::vector<StreamData> toBeHistogrammed;
	operatePixels(true);
	calibratePixels(true);
	calibratePixels(false);
	operatePixels(false);
	for(unsigned int i=0; i<nSteps; i++) {
		setThreshold((float) ((range/(nSteps-1)*i)+offset));
		std::cout << "Threshold step: " << std::dec << i << " Value is: " << (float) ((range/(nSteps-1)*i)+offset);
		unsigned int nHits=0;
//		if (resetCalib) {
//			operatePixels(true);
//			calibratePixels(true);
//			calibratePixels(false);
//			operatePixels(false);
//		}
		captureData(toBeHistogrammed, nStrobes, 16, true, resetCalib, 0);
// And here is where the histogramming should happen over toBeHistogrammed[index]->getRawData(N[0-99])...
		for (unsigned int k=0; k < toBeHistogrammed.size(); k++) {
			for(unsigned int m=0; m<((unsigned int) nStrobes*7); m++) {
				StripHit *hit = new StripHit(toBeHistogrammed[k].getRawData(m/7).getHit(m%7+1));
				for (unsigned int p=0;p<4;p++) {
					if (hit->getSingleHit(p)) {
						myHisto->Fill((float) (p+(15-k)*4), (float) ((range/(nSteps-1)*i)+offset));
						nHits++;
					}
				}
			}
		}
		std::cout << " Hit Count was: " << nHits << std::endl;
		toBeHistogrammed.clear();
	}
	myHisto->Draw("COLZ");
	myHisto->SaveAs(filename);
}

void runStripCheck() {
	std::vector<StreamData> v;
	unsigned int words=1;
	unsigned int nChannels=18;
	unsigned char i=0;
	while (true) {
		StripHit fill = StripHit(i);
		std::vector<unsigned int> values;
		for (unsigned int k=0; k<64; k++) {
			values.push_back(fill.getSingleHit((3-(k%4))));
		}
		inject_numbers(values);
		captureData(v, words, nChannels);
		for (unsigned int k=0; k<16; k++) {
			if (StripHit(v[k].getRawData(0).getHit(1)) != fill) {
				printf("%03d: Error in returned data, Channel %x, Data is %02x, Expected %02x\n", i, v[k].getChannel(), v[k].getRawData(0).getHit(1), fill.getData());
			}
		}
		v.clear();
		if(i==255) break;
		i++;
	}
}

void runPadCheck() {
	std::vector<StreamData> v;
	unsigned int words=1;
	unsigned int nChannels=18;
	unsigned int i=0;
	std::vector<unsigned int> sum2be;
	sum2be.clear();
//	printf("Starting PadCheck...\n");
	while (true) {
//		printf("In Loop, counter %d\n", i);
		unsigned int temp = i;
		sum2be.clear();
		for (unsigned int k=0; k<16; k++) {
			if (temp < 31) {
				sum2be.push_back(temp);
			} else {
				sum2be.push_back(31);
				temp -= 31;
			}
		}
		for (unsigned int blocks = 0; blocks < 4; blocks++) {
			inject_numbers(sum2be);
//			printf("Block Number %d is being configured\n", blocks);
		}
		unsigned int sum = 0;
		unsigned int overflow = 0;
		for (unsigned int index=0; index<sum2be.size(); index++) {
			sum += (sum2be.at(index) & 0xf);
			overflow += ((sum2be.at(index) & 0x10) >> 4);
		}
		PadHit fill = PadHit(sum, overflow);
		captureData(v, words, nChannels);
		for (unsigned int k=2; k<16; k+=4) {
			if (PadHit(v[k+1].getRawData(0).getHit(1), v[k].getRawData(0).getHit(1)) != fill) {
				printf("%03d: Error in returned data, Channel %x, Sum is %02x, Channel %x, Overflow is %02x, Expected %02x and %02x\n", i, v[k+1].getChannel(), v[k+1].getRawData(0).getHit(1), v[k].getChannel(), v[k].getRawData(0).getHit(1), fill.getSum(), fill.getOverflow());
			}

		}
		v.clear();
		i++;
		if(i>31*16) break;
	}
}

void resetChip(bool enable) {
	ChipConfig *cfg = new ChipConfig(0);
// Some examples of what could be done with a configuration
//	cfg->disable(); // disable all pixels
//	cfg->enableRow(row); // enable one row (as per parameters)
	if (enable) {
		cfg->enable();
	} else {
		cfg->disable();
	}
	writeChip(cfg);
}

void DECAL_UseHisto(uint32_t delay, int dg_rates, bool linkid_mode, int recordMode=3) {

  //DG Rates:
  //  0-4  : 16MHz   8MHz   4MHz   2MHz   1MHz
  //  5-9  : 1600kHz 800kHz 400kHz 200kHz 100kHz
  // 10-14 : 160kHz  80kHz  40kHz  20kHz  10kHz
  //   15  : 16kHz

  int rates_khz[16] = {
    16000, 8000, 4000, 2000, 1000,
    1600,  800,  400,  200,  100,
    160,   80,   40,   20,   10,
    16};

  unsigned int modeSwitch = ((recordMode & 0x3) << 8) | 0xf0ff;

  // Init
  e->ConfigureVariable(10019, 0x1000); // Disable Trigger-Data packets
  e->ConfigureVariable(10023, 0x0000);

  // Modes decode
  if (linkid_mode) {
    printf("LinkID mode: Setting counters = 0xdeca1 + linkid(8b) + sub-link(4b).\n");
    e->ConfigureVariable(10023, 0x0010);
  }
  else if (dg_rates > 0xffff) {
    printf("DG rates value too high, aborting!\n");
    return;
  }
  else if (dg_rates >= 0) {
    printf("DataGen mode, using DG0, sub-link rates (0-3) = ");
    printf("%1x(%dkHz), %1x(%dkHz), %1x(%dkHz), %1x(%dkHz)\n",
	   dg_rates&0xf,       rates_khz[dg_rates&0xf],
	   (dg_rates>>4)&0xf,  rates_khz[(dg_rates>>4)&0xf],
	   (dg_rates>>8)&0xf,  rates_khz[(dg_rates>>8)&0xf],
	   (dg_rates>>12)&0xf, rates_khz[(dg_rates>>12)&0xf]);
    e->ConfigureVariable(10036, dg_rates);
    e->ConfigureVariable(10037, dg_rates);      //for the future
    streamConfigWrite(0x000f, 0, 0x0008, 16*2); //Data source = DG0
  }
  else
    //streamConfigWrite(0xffff,0,(0x8311 & modeSwitch), 16*2);
    streamConfigWrite(0x88ff, 0, 0x8309, 16*2);

  uint16_t sb[300]; // status block buffer
  //Readback status block2 - using extended status block feature
  //Matt_GetStatusBlock2(sb, 2, delay, 0);

  static uint16_t seqnum = 0x7000;
  uint16_t ocdata[15];
  int i=0;
  ocdata[i++] = 0x2;
  ocdata[i++] = (delay >> 16) & 0xffff;
  ocdata[i++] = delay & 0xffff;

  uint16_t length = e->HsioSendReceiveOpcode(0x19, seqnum++, i, ocdata, 128+3, sb);
  if(length > 0xf000) printf("GetStatusBlock2: ERROR: No response\n");
  else {
    for (int n=0; n<(128+3); n++) sb[n]=((sb[n]&0xff)<< 8)|((sb[n]&0xff00)>>8);

    printf("Hdr: %04x %04x %04x\n", sb[0], sb[1], sb[2]);
    for (int n=3; n<(128+3); n++) {
      if ((n-3)%8 == 0) printf("%3d: ",(n-3));
      printf("%04x ", sb[n]);
      if ((n-3)%8 == 7) printf("\n");
    }
    printf("\n");
  }

  streamConfigWrite(0x000f, 0, 0, 16*2);
}


std::vector<int> captureCounters(unsigned long long int capLength, unsigned int nChannels, int delay, int maxcap = 2000, int nRepeat = 50) {
	std::vector<int> returnCounts;
	returnCounts.clear();
	// Check capLength
	if (capLength < 1) {
		printf("Not capturing nothin', screw you!\n");
		return returnCounts;
	}


	//printf("Capturing Data on %d streams\n", nChannels);

	e->HsioFlush();
	Matt_SendStreamCommand(2,0,32); // clear counters
	e->ConfigureVariable(10019, 0x1000); // Disable Trigger-Data packets

	e->ConfigureVariable(10036,0x0000); // enable DG
	streamConfigWrite(0xffff, 0, 0x8000, nChannels*2); //No more capture?
	e->ConfigureVariable(10023, 0x0000);
	e->ConfigureVariable(10032, 0x200a); // VarLen + Shortest TO
	e->ConfigureVariable(10023, 0x880); // set trig = capt start

	e->ConfigureVariable(10035, (delay>>16)&0xffff);
	e->ConfigureVariable(10034, delay&0xffff);

	//Set count-timer period (12.5ns steps)
	e->ConfigureVariable(10039, 0);
	e->ConfigureVariable(10038, (maxcap&0xffff)); //25us

	for(unsigned int i=0; i<64;i++) returnCounts.push_back(0);
	static uint16_t seqnum = 0x7000;
	uint16_t sb[300];
	uint16_t data[15];
	uint16_t opcode = 0x19;


	uint16_t rxsize   = 128+3;
	data[0] = 0;
	
	while (capLength > maxcap) {
	  resetPixels(0, nRepeat);
	  if (delay+maxcap > 9000) { 
	    usleep((delay+maxcap-9000)/80 + 1); // sleeps in Microseconds, rounds up and subtracts estimated latency of 112 us that corresponds to 9000 clock cycles
	    //e->Sleep((delay+maxcap)/80000 + 1); // Sleeps in unit milliseconds and rounds up the int division 
	  }
	  //if (delay > 0xffff) e->Sleep(delay/80000);
	  capLength-=maxcap;
	}
	e->ConfigureVariable(10038, (capLength & 0xffff));
	resetPixels(0, nRepeat);
	if (delay+capLength > 9000) usleep((delay+capLength-9000)/80 + 1);  // sleeps in Microseconds, rounds up and subtracts estimated latency of 112 us that corresponds to 9000 clock cycles
	//if (delay > 0xffff) e->Sleep(delay/80000);

	int i=0;
	data[i++] = 0x2;
	data[i++] = 0;
	data[i++] = 0;


	uint16_t length = e->HsioSendReceiveOpcode(opcode, seqnum++, i, data, rxsize, sb);
	if(length > 0xf000) { printf("GetStatusBlock: ERROR: No response\n"); return returnCounts; }

	for (int n=0; n<rxsize; n++) sb[n]=((sb[n]&0xff)<< 8)|((sb[n]&0xff00)>>8);

	for (int n=0; n<64; n++) {
		returnCounts[n]+= (sb[2*n+3] | (sb[2*n+4]<<16));
	}
	streamConfigWriteAll(0x000f, 0);
	return returnCounts;
}



// Give a row parameter bigger 63 to get all rows scanned as per the given chip config!
void counterThresholdScanRow(unsigned int nSteps, unsigned int row, float range, float offset, bool resetCalib, long long int nStrobes, unsigned int nsleep, ChipConfig *config, std::string nameoffile="", std::string nameofhisto="", bool singlerow = true, bool update = false, unsigned int MaxCap = 2000, int NRepeat = 50, bool ThreshLowtoHigh = true, bool triggered = false) {
// Some examples of what could be done with a configuration
	if (config == NULL) {config = new ChipConfig();}
	if (row < 64) {
		config->disable();
		config->enableRow(row);
	}
	writeChip(config);
	double step=(range/((double) nSteps-1))/2.0;
	char histoname[200] = "";
	if(!(nameofhisto==""))
	  sprintf(histoname,"%s",nameofhisto.c_str());
	else
	  sprintf(histoname,"counterThresholdScanRow%02d",row);
	//char filename[200] = "";
	//sprintf(filename,"counterThresholdScanRow%02d.root",row);
	char filename[200] = "";
	if(!(nameoffile==""))
	  sprintf(filename,"%s",nameoffile.c_str());
	else if(singlerow)
	  sprintf(filename,"counterThresholdScanRow%02d.root",row);
	else
	  sprintf(filename,"counterThresholdScanMultipleRows.root");
	char fileoption[200] = "";
	if(update)
	  sprintf(fileoption,"update");
	else
	  sprintf(fileoption,"recreate");
	TH2D *myHisto = new TH2D(histoname, histoname, 64, -0.5, 63.5, nSteps, offset-step, offset+range+step);
	std::vector<StreamData> toBeHistogrammed;
	unsigned int i;
	for(unsigned int j=0; j<nSteps; j++) {
	  if (ThreshLowtoHigh) i = j;
	  else i = nSteps - 1 - j;
	  resetPixels(0, NRepeat);
	  setThreshold((float) ((range/(nSteps-1)*i)+offset));
	  std::cout << "Threshold step: " << std::dec << i << " Value is: " << (float) ((range/(nSteps-1)*i)+offset) << " Sleeping " << nsleep << "BCs" << std::endl;
	  unsigned int nHits=0;

	  std::vector<int> temp = captureCounters(nStrobes, 16, nsleep, MaxCap, NRepeat);
	  // And here is where the histogramming should happen over toBeHistogrammed[index]->getRawData(N[0-99])...
	  for(int index=0; index<64; index++) {
	    myHisto->Fill((float) (index), (float) ((range/(nSteps-1)*i)+offset), temp[63-index]);
	    nHits += temp[63-index];
	  }
	  std::cout << " Hit Count was: " << nHits << std::endl;
	  toBeHistogrammed.clear();
	}
	//myHisto->Draw("COLZ");
	//myHisto->SaveAs(filename);
	TFile *myfile = new TFile(filename,fileoption);
	myfile->cd();
	myHisto->Write(myHisto->GetName(),TObject::kOverwrite);
	myfile->Close();
	std::cout << " Saved histo " << myHisto->GetName() << " to file " << myfile->GetName() << std::endl;
	delete myHisto;
	delete myfile;
}

// Scan all rows inside vector "rows"
void counterThresholdScanMultipleRows(unsigned int nSteps, std::vector<unsigned int> rows, float range, float offset, bool resetCalib, int nStrobes, unsigned int nsleep, ChipConfig *config, std::string nameoffile="", std::string nameofhisto="") {
  for(unsigned int r = 0; r<rows.size();++r){
    char filename[200] = "";
    sprintf(filename,"counterThresholdScanRow%02d_%02d.root",rows[r],r);
    counterThresholdScanRow(nSteps, rows[r], range, offset, resetCalib, nStrobes, nsleep, config, filename, nameofhisto, false, true);
  }
}

// repeat a measurement of rows in a given series multiple times, to investigate temperature effects
// example pass NrepExpo = 6, repeatme = {10, 20, 30, 40, 50} which will repeat measurement of the list 2^6 times.
void cTScanRepetition(unsigned int nSteps, unsigned int NrepExpo, std::vector<unsigned int> repeatme, float range, float offset, bool resetCalib, int nStrobes, unsigned int nsleep, ChipConfig *config, std::string nameoffile="", std::string nameofhisto=""){
  for(unsigned int i = 0; i<NrepExpo; ++i)
    repeatme.insert(repeatme.end(), repeatme.begin(), repeatme.end()); // appends it to itself and grows exponentially
  counterThresholdScanMultipleRows(nSteps, repeatme, range, offset, resetCalib, nStrobes, nsleep, config, nameoffile, nameofhisto);
}

void counterThresholdScanAllRows(unsigned int nSteps, float range, float offset, bool resetCalib, int nStrobes, unsigned int nsleep, ChipConfig *config, std::string nameoffile="", std::string nameofhisto="") {
  for(unsigned int r = 0; r<64;++r)
    counterThresholdScanRow(nSteps, r, range, offset, resetCalib, nStrobes, nsleep, config, nameoffile, nameofhisto, false, true);
}

void thresholdScanAllRows(unsigned int nSteps, float range, float offset, bool resetCalib, int nStrobes, ChipConfig *config) {
  for(unsigned int r = 0; r<64;++r)
    thresholdScanRow(nSteps, r, range, offset, resetCalib, nStrobes, config);
}

void delayThresholdScanRow(unsigned int nSteps, unsigned int row, unsigned int col, float range, float offset, bool resetCalib, int nStrobes, unsigned int sleeprange, unsigned int sleepsteps, ChipConfig *config) {
// Some examples of what could be done with a configuration
	if (config == NULL) {config = new ChipConfig();}
	config->disable();
	if (row < 64) {
		config->enableRow(row);
	}
	writeChip(config);
	double step=(range/((double) nSteps-1))/2.0;
	char histoname[200] = "";
	sprintf(histoname,"counterThresholdScanRow%02d",row);
	char filename[200] = "";
	sprintf(filename,"counterThresholdScanRow%02d.root",row);
	TH2D *myHisto = new TH2D(histoname, histoname, sleepsteps, -0.5, sleeprange-0.5, nSteps, offset-step, offset+range+step);
	for(unsigned int i=0; i<nSteps; i++) {
		for(unsigned int k=0; k<sleepsteps; k++) {
			setThreshold((float) ((range/(nSteps-1)*i)+offset));
			resetPixels(0);
			int sleeptime=int(float(sleeprange)/float(sleepsteps)*float(k));
			std::cout << "Threshold step: " << std::dec << i << " Value is: " << (float) ((range/(nSteps-1)*i)+offset) << " Sleeping " << sleeptime/2.0 << "BCs" << std::endl;
			std::vector<int> temp = captureCounters(nStrobes, 16, sleeptime);
	// And here is where the histogramming should happen over toBeHistogrammed[index]->getRawData(N[0-99])...
			myHisto->Fill((float) (sleeptime), (float) ((range/(nSteps-1)*i)+offset), temp[63-col]);
		}
	}
	myHisto->Draw("COLZ");
	myHisto->SaveAs(filename);
}

// Reads in DAC Config from file and stores in Chip Config. Later this can be written to he chip
void ReadConfig(string filename, ChipConfig *config, bool header = true, bool Colbitflipped = false) {

  if (config == NULL) {config = new ChipConfig();}

  std::ifstream infile(filename);
  std::string line;
  if(infile.fail()) {
    cout << " No such file exists! " << endl;
  }

  if(header)
    getline(infile, line); // skips header line
  unsigned int row , col, dac, pol;
  bool masked = false;
  while(getline(infile, line)) {
    std::istringstream iss(line);
    iss >> row >> col >> dac >> pol;
    bool polarity = !!pol; // for int to bool conversion
    col = 64 - col; // naming convention is reversed
    if(Colbitflipped)
      col = ((col&2)==0) ? col+=2 : col-=2; // flipping second bit of col index
    setDACperPix(col, row, dac, polarity, masked, config); // column values in txt file range from 1 to 64, while row from 0 to 63.
  }
}

void TuningScanRow(unsigned int nSteps, unsigned int row, float range, float offset, bool resetCalib, int nStrobes, unsigned int nsleep, ChipConfig *config, std::string nameoffile="",std::string nameofhisto="", bool singlerow = true, bool update = true, bool termoutput = false, bool enablesinglerow = true) {
// Some examples of what could be done with a configuration
	if (config == NULL) {config = new ChipConfig();}
	if (enablesinglerow){
	  config->disable();
	  if (row < 64) {
	    config->enableRow(row);
	  }
	}
	for( unsigned int pol = 0; pol<2; pol++){
	  bool polarity = !!pol; // for int to bool conversion
	  for(unsigned int DAC = 0; DAC<16; DAC++){
	    // loop over all cols to set all to the same DAC
	    for(unsigned int col = 0; col<64; col++){
	      setDACperPix(col, row, DAC, polarity, false, config);}
	    // FROM HERE SAME AS counterthresholdscanrow...
	    writeChip(config);
	    double step=(range/((double) nSteps-1))/2.0;
	    char histoname[200] = "";
	    if(!(nameofhisto==""))
	      sprintf(histoname,"%s",nameofhisto.c_str());
	    else
	      sprintf(histoname,"TuningScanRow%02dDAC%02dPol%d",row, DAC, pol);
	    //char filename[200] = "";
	    //sprintf(filename,"TuningScanRow%02d.root",row);
	    char filename[200] = "";
	    if(!(nameoffile==""))
	      sprintf(filename,"%s",nameoffile.c_str());
	    else if(singlerow)
	      sprintf(filename,"TuningScanRow%02d.root",row);
	    else
	      sprintf(filename,"TuningScanMultipleRows.root");
	    char fileoption[200] = "";
	    if(update)
	      sprintf(fileoption,"update");
	    else
	      sprintf(fileoption,"recreate");
	    TH2D *myHisto = new TH2D(histoname, histoname, 64, -0.5, 63.5, nSteps, offset-step, offset+range+step);
	    std::vector<StreamData> toBeHistogrammed;
	    for(unsigned int i=0; i<nSteps; i++) {
	      resetPixels(0);
	      setThreshold((float) ((range/(nSteps-1)*i)+offset));
	      //std::cout << "Threshold step: " << std::dec << i << " Value is: " << (float) ((range/(nSteps-1)*i)+offset) << " Sleeping " << nsleep << "BCs" << std::endl;
	      unsigned int nHits=0;

	      std::vector<int> temp = captureCounters(nStrobes, 16, nsleep);
	      // And here is where the histogramming should happen over toBeHistogrammed[index]->getRawData(N[0-99])...
	      for(int index=0; index<64; index++) {
		myHisto->Fill((float) (index), (float) ((range/(nSteps-1)*i)+offset), temp[63-index]);
		nHits += temp[63-index];
	      }
	      //std::cout << " Hit Count was: " << nHits << std::endl;
	      toBeHistogrammed.clear();
	    }
	    //myHisto->Draw("COLZ");
	    //myHisto->SaveAs(filename);
	    TFile *myfile = new TFile(filename,fileoption);
	    myfile->cd();
	    myHisto->Write(myHisto->GetName(),TObject::kOverwrite);
	    myfile->Close();
	    std::cout << " Saved histo " << myHisto->GetName() << " to file " << myfile->GetName() << std::endl;
	    delete myHisto;
	    delete myfile;
	  }
	}
}

void TuningScanAllRows(unsigned int nSteps, float range, float offset, bool resetCalib, int nStrobes, unsigned int nsleep, std::string configtxt="", std::string nameoffile="", std::string nameofhisto="", bool singlerow = true, bool update = true){
	ChipConfig* config = new ChipConfig();
	for(unsigned int r = 0; r<64;++r){
		if (configtxt == "") {config = new ChipConfig();}
		else {ReadConfig(configtxt, config);} // This resets config after each tuning of a row.
    TuningScanRow(nSteps, r, range, offset, resetCalib, nStrobes, nsleep, config, nameoffile, nameofhisto, singlerow, update);
	}
}

void TuningScanMultipleRows(unsigned int row_max, unsigned int nSteps, float range, float offset, bool resetCalib, int nStrobes, unsigned int nsleep, std::string configtxt="", std::string nameoffile="", std::string nameofhisto="", bool singlerow = true, bool update = true){
	ChipConfig* config = new ChipConfig();
	for(unsigned int r = 0; r<row_max;++r){
		if (configtxt == "") {config = new ChipConfig();}
		else {ReadConfig(configtxt, config);} // This resets config after each tuning of a row.
    TuningScanRow(nSteps, r, range, offset, resetCalib, nStrobes, nsleep, config, nameoffile, nameofhisto, singlerow, update);
	}
}

void TuneConfigNeighborsRow(unsigned int nSteps, int nNeighbors, unsigned int row, float range, float offset, bool resetCalib, int nStrobes, unsigned int nsleep, ChipConfig *config, std::string nameoffile="",std::string nameofhisto="", bool singlerow = true, bool update = true, bool termoutput = false) {
	if (config == NULL) {config = new ChipConfig();}
	config->disable();
	if (row < 64) {
		config->enableRow(row);
	}
	std::vector<int> dac(64);
	std::vector<int> dacpol(64);
	std::vector<bool> polarity(64);
	for(unsigned int col = 0; col<64; col++){
	  // get DAC value of config per pixel
	  dac[col] = config->getColConfig(col)->getPixConfig(row)->getDAC();
	  polarity[col] = config->getColConfig(col)->getPixConfig(row)->isPositive();
	  dacpol[col] = (int(polarity[col])*2 -1 )*(dac[col]+0.5) + 15.5; // combine dac and pol to one int between 0 and 31
	  std::cout << "Col " << col << " Dac, pol: " << dac[col] << ", " << polarity[col]<< " DacPol: "<< dacpol[col] << std::endl;
	}
	// scan neighboring configurations
	for(int neigh = - nNeighbors; neigh < nNeighbors+1; neigh++){
	  // loop over all cols to set all to the same DAC
	  for(unsigned int col = 0; col<64; col++){
	    int  pol, DAC;
	    int dacpolneigh;
	    dacpolneigh = dacpol[col] + neigh; // scan neighboring configurations if not at edge
	    if(dacpolneigh < 0){ dacpolneigh = 0;}
	    else if(dacpolneigh > 31) {dacpolneigh = 31;}
	    pol = dacpolneigh/16; // extract polarity
	    DAC = int((dacpolneigh - 15.5)* (pol*2 - 1) - 0.5); // extract 4 bit DAC
	    //std::cout << "Col " << col << " Dac, pol: " << DAC << ", " << !!pol<< " DacPol: "<< dacpolneigh << std::endl;
	    setDACperPix(col, row, (unsigned int) DAC, !!pol, false, config);
	  }
	  // FROM HERE SAME AS counterthresholdscanrow...
	  writeChip(config);
	  double step=(range/((double) nSteps-1))/2.0;
	  char histoname[200] = "";
	  if(!(nameofhisto==""))
	    sprintf(histoname,"%s",nameofhisto.c_str());
	  else
	    sprintf(histoname,"TuningScanRow%02dNeigh%02d",row, neigh);
	  //sprintf(histoname,"TuningScanRow%02dDAC%02dPol%d",row, DAC, pol);
	  //char filename[200] = "";
	  //sprintf(filename,"TuningScanRow%02d.root",row);
	  char filename[200] = "";
	  if(!(nameoffile==""))
	    sprintf(filename,"%s",nameoffile.c_str());
	  else if(singlerow)
	    sprintf(filename,"TuningScanRow%02d.root",row);
	  else
	    sprintf(filename,"TuningScanMultipleRows.root");
	  char fileoption[200] = "";
	  if(update)
	    sprintf(fileoption,"update");
	  else
	    sprintf(fileoption,"recreate");
	  TH2D *myHisto = new TH2D(histoname, histoname, 64, -0.5, 63.5, nSteps, offset-step, offset+range+step);
	  std::vector<StreamData> toBeHistogrammed;
	  for(unsigned int i=0; i<nSteps; i++) {
	    resetPixels(0);
	    setThreshold((float) ((range/(nSteps-1)*i)+offset));
	    //std::cout << "Threshold step: " << std::dec << i << " Value is: " << (float) ((range/(nSteps-1)*i)+offset) << " Sleeping " << nsleep << "BCs" << std::endl;
	    unsigned int nHits=0;

	    std::vector<int> temp = captureCounters(nStrobes, 16, nsleep);
	    // And here is where the histogramming should happen over toBeHistogrammed[index]->getRawData(N[0-99])...
	    for(int index=0; index<64; index++) {
	      myHisto->Fill((float) (index), (float) ((range/(nSteps-1)*i)+offset), temp[63-index]);
	      nHits += temp[63-index];
	    }
	    //std::cout << " Hit Count was: " << nHits << std::endl;
	    toBeHistogrammed.clear();
	  }
	  //myHisto->Draw("COLZ");
	  //myHisto->SaveAs(filename);
	  TFile *myfile = new TFile(filename,fileoption);
	  myfile->cd();
	  myHisto->Write(myHisto->GetName(),TObject::kOverwrite);
	  myfile->Close();
	  std::cout << " Saved histo " << myHisto->GetName() << " to file " << myfile->GetName() << std::endl;
	  delete myHisto;
	  delete myfile;
	}
}

void TuneConfigNeighborsAllRows(unsigned int nSteps, int nNeighbors, float range, float offset, bool resetCalib, int nStrobes, unsigned int nsleep, std::string configtxt="", std::string nameoffile="", std::string nameofhisto="", bool singlerow = true, bool update = true){
	ChipConfig* config = new ChipConfig();
	for(unsigned int r = 0; r<64;++r){
		if (configtxt == "") {config = new ChipConfig();}
		else {ReadConfig(configtxt, config);} // This resets config after each tuning of a row.
    TuneConfigNeighborsRow(nSteps, nNeighbors, r, range, offset, resetCalib, nStrobes, nsleep, config, nameoffile, nameofhisto, singlerow, update);
	}
}


// This fct reads in the measurements of TuneConfigNeighborsAllRows().
// It optimizes/ checks the existing tuned DAC configuration.
// For each pixel the neigh neighbors relative to the optimal DAC config to both sides are scanned.
// The config with mean closest to the nominal value is chosen.
// The optimized DAC configurations are written out as a file that can be later read in to tune the chip with ReadConfig().
// Later, measurements and analysis can be combined in one fct.
void Retune(std::string rootfolder, int neigh, unsigned int yerror, float nom_mean, std::string fopti, bool header=true, bool foptiflippedbit = false){
  TF1 *fgaus = new TF1("fGaus","gaus",1, 2);
  // read in initial dac configuration and store as value between 0 and 31 per pixel
  std::ifstream infile(fopti);
  std::string line;
  if(header)
    getline(infile, line); // skips header line
  int ro , colu, dac, pol;
  std::vector<vector<int>> dacpol( 64 , vector<int> (64));
  while(getline(infile, line)) {
    std::istringstream iss(line);
    iss >> ro >> colu >> dac >> pol;

    colu = 64 - colu; // naming convention is reversed
    if (foptiflippedbit)
      colu = ((colu&2)==0) ? colu+=2 : colu-=2;
    dacpol[ro][colu] = int((2*pol-1)*(dac +0.5) +15.5);
    //dacpol[ro][colu-1] = int((2*pol-1)*(dac +0.5) +15.5);
    //colu= 64 - colu; // naming convention is reversed
  }
  infile.close();
  std::ofstream outfile ("ReTunedconfig.txt");
  outfile <<  "Row Column DAC Polarity Constant Constant_err Mean Mean_err Sigma Sigma_err DACshift" << endl;

  int N = 2*neigh +1;
  int nrows = 64;
  for(unsigned int row = 0; row < 64; row++){
    cout << "Row: " << row << endl;
    char rfile[200] = "";
    sprintf(rfile,"%s/TuningScanRow%02d.root", rootfolder.c_str(), row);
    TFile *f = new TFile(rfile);
    vector<vector<float>> constants( nrows , vector<float> (N));
    vector<vector<float>> constants_err( nrows , vector<float> (N));
    vector<vector<float>> means( nrows , vector<float> (N));
    vector<vector<float>> means_err( nrows , vector<float> (N));
    vector<vector<float>> sigmas( nrows , vector<float> (N));
    vector<vector<float>> sigmas_err( nrows , vector<float> (N));
    vector<vector<float>> deviate( nrows , vector<float> (N));
    int index, newdacpol;

    for(int n = - neigh; n < neigh +1; n++){
      char histoname[200] = "";
      sprintf(histoname,"TuningScanRow%02dNeigh%02d",row,n);
      TH2D * histo = (TH2D*)f->Get(histoname);
      for(int col = 0; col < 64; col++){ // 0 to 64 to avoid overflow bins in y-projection
        TH1D *projy = histo->ProjectionY("histo_py",64 - col, 64- col); // naming convention is reversed in writing config to chip
        for(int b = 0 ; b < projy->GetNbinsX(); b++){ // set all errors to same value
                  projy->SetBinError(b, yerror); }
        projy->Fit("fGaus", "Q");
        constants[col][n + neigh] = fgaus->GetParameter(0);
        constants_err[col][n + neigh] = fgaus->GetParError(0);
        means[col][n + neigh] = fgaus->GetParameter(1);
        means_err[col][n + neigh] = fgaus->GetParError(1);
        sigmas[col][n + neigh] = fgaus->GetParameter(2);
        sigmas_err[col][n + neigh] = fgaus->GetParError(2);
        deviate[col][n + neigh] = abs(fgaus->GetParameter(1) - nom_mean);
      }
    }

    for(int i = 0; i < nrows; i++)
      {
        index = min_element(deviate[i].begin(), deviate[i].end()) - deviate[i].begin();
        //cout <<  row << " " << i << " " << index - neigh << " " << constants[i][index] << " " << means[i][index] << " "  << sigmas[i][index] << " " << endl;
        newdacpol = min(max(0, dacpol[row][i] + index - neigh ),31) ; // allow only config values inside of 0 to 31 range

        if (outfile.is_open()){
          outfile <<  row << " " << 64 - i << " " <<  int((newdacpol -15.5)*(newdacpol/16 *2-1) -0.5) << " " <<  newdacpol/16 << " "  << constants[i][index] << " " << constants_err[i][index] << " " << means[i][index] << " " << means_err[i][index] << " "  << sigmas[i][index] << " " << sigmas_err[i][index] << " " << index - neigh << endl;
	  //outfile <<  row << " " << i+1 << " " <<  int((newdacpol -15.5)*(newdacpol/16 *2-1) -0.5) << " " <<  newdacpol/16 << " "  << constants[i][index] << " " << constants_err[i][index] << " " << means[i][index] << " " << means_err[i][index] << " "  << sigmas[i][index] << " " << sigmas_err[i][index] << " " << index - neigh << endl;
          }
        else
          cout << "unable to open outfile" << endl;
      }
  }
  outfile.close();
}


// Variation of IcalibDac in IDACnSteps from (including) 0 to 100 uA. One Row is scanned through all 32 DAC Configurations.
void IcalibDAC(unsigned int IDACnSteps , unsigned int nSteps, unsigned int row, float range, float offset, bool resetCalib, int nStrobes, unsigned int nsleep, ChipConfig *config, std::string nameoffile="", std::string nameofhisto=""){
  setupAnalog();
  sleep(1); // wait
  for (unsigned int i=0; i< IDACnSteps+1; i++)
    { float IDACval = i*100./IDACnSteps; // includes 0 and 100 (in uA)
      setIDAC(12, 0, IDACval);
      sleep(1);
      std::cout << "ICalibDAC value is: " <<   IDACval << " uA" << endl;
      char filename[200] = "";
      if(!(nameoffile==""))
	sprintf(filename,"IDAC%.0f%s",IDACval, nameoffile.c_str());
      else
	sprintf(filename,"TuningScanRow%02dIDAC%.0f.root",row,IDACval);
      TuningScanRow(nSteps, row, range, offset, resetCalib, nStrobes, nsleep, config, filename, nameofhisto);
    }
}


// Variation of current defined by i2c_addr and dacnum in given range. Current can be for example IShaperBias (12,1), IPreAmpBias (12,2) or IPFBPreAmp (13,2)
void CurrentVariation(unsigned int i2c_addr ,unsigned int dacnum, unsigned int ISteps, float Irange, float Ioffset, unsigned int nSteps, unsigned int row, float range, float offset, bool resetCalib, int nStrobes, unsigned int nsleep, ChipConfig *config, std::string nameoffile="", std::string nameofhisto=""){
  setupAnalog();
  sleep(1); // wait
  for (unsigned int i=0; i< ISteps+1; i++){
    float Ival = Ioffset + i*Irange/ISteps;
    setIDAC(i2c_addr, dacnum, Ival); // wait after changing current
    sleep(1);
    std::cout << "Current set to " <<   Ival << " uA" << endl;
    char histoname[200] = "";
    if(!(nameofhisto==""))
      (histoname,"Ival%.0f%s",Ival, nameofhisto.c_str());
    else
      sprintf(histoname,"counterThresholdScanRow%02dIval%.0f", row, Ival);
    char filename[200] = "";
    if(!(nameoffile==""))
      (filename,"%s", nameoffile.c_str());
    else
      sprintf(filename,"counterThresholdScanRow%02dCurrentVariation.root", row);
    counterThresholdScanRow(nSteps, row, range, offset, resetCalib, nStrobes, nsleep, config, filename, histoname, false, true);
  }
}

void warmup(unsigned int minutes, string configtxt=""){
  ChipConfig* config = new ChipConfig();
  if (configtxt == "") {config = new ChipConfig();}
  else {ReadConfig(configtxt, config);} // reset config when scaning other pixel
  for(unsigned int m = 0; m<minutes; m++){ // Each scan takes around 1 minute
    counterThresholdScanRow(361, 100, 0.18, 1.08, true, 1000000, 1100, config, "warumUpScan.root");
    cout << m+1 << " of " << minutes <<  " Scans performed"<< endl;}
}

void longterm_measurement_Row(unsigned int minutes, unsigned int row, unsigned int nSteps, float range, float offset,  unsigned long long int nStrobes, string configtxt , string fileprefix=""){
  ChipConfig* config = new ChipConfig();
  if (configtxt == "") {config = new ChipConfig();}
  else {ReadConfig(configtxt, config);}
  warmup(minutes, configtxt);
  char PreScanfilename[200] = "";
  char MainScanfilename[200] = "";
  char EndScanfilename[200] = "";
  sprintf(PreScanfilename,"%s_PreScan.root", fileprefix.c_str());
  sprintf(MainScanfilename,"%s_MainScan.root", fileprefix.c_str());
  sprintf(EndScanfilename,"%s_EndScan.root", fileprefix.c_str());
  counterThresholdScanRow(151, row, 0.15, 1.12, true, 1000000, 1100, config, PreScanfilename);
  counterThresholdScanRow(nSteps, row, range, offset, true, nStrobes, 1100, config, MainScanfilename);
  counterThresholdScanRow(151, row, 0.15, 1.12, true, 1000000, 1100, config, EndScanfilename);

}

void longtermmeasurement_pixel(unsigned int minutes, unsigned int nSteps, unsigned int nStrobes, string configtxt ){
  ChipConfig* config = new ChipConfig();
  if (configtxt == "") {config = new ChipConfig();}
  else {ReadConfig(configtxt, config);}
  warmup(minutes, configtxt);
  counterThresholdScanAllRows(nSteps, 0.08, 1.13, true, nStrobes, 1100, config, "pixmode_1.root");
  counterThresholdScanAllRows(nSteps, 0.08, 1.13, true, nStrobes, 1100, config, "pixmode_2.root");
}

void Find_minStd(string rootpath, string txtfname = "optimal_Tuning.txt", unsigned int rowmin = 0, unsigned int rowmax = 64, bool header = true){
  ofstream outfile;
  if(header){
    outfile.open(txtfname);
    outfile <<  "Row Column DAC Polarity" << endl;
    }
  else outfile.open(txtfname, ios_base::app);
  for(int row = rowmin; row < rowmax; row++){
    cout << "Row: "<< row << endl;
    char rfile[200] = "";
    sprintf(rfile,"%s", rootpath.c_str());
    TFile *f = new TFile(rfile);
    vector<vector<float>> Std( 64 , vector<float> (32));
    vector<vector<unsigned int>> Pols( 64 , vector<unsigned int> (32));
    vector<vector<unsigned int>> Dacs( 64 , vector<unsigned int> (32));
    int index;
    unsigned int count = 0;
    for(int pol = 0; pol < 2; pol++){
      for(int dac = 0; dac < 16; dac++){
        char histoname[200] = "";
        sprintf(histoname,"TuningScanRow%02dDAC%02dPol%01d",row,dac, pol);
        TH2D *histo = (TH2D*)f->Get(histoname);
        for(int col = 0; col < 64; col++){
          TH1D *projy = histo->ProjectionY("histo_py", col +1, col +1);// 1 to 64 to avoid overflow bins
          Std[col][count]=projy->GetStdDev();
          Pols[col][count]=pol;
          Dacs[col][count]=dac;
	  cout << count << " " << col << " " <<   Std[col][count]  << " " <<   Pols[col][count] <<  " " <<   Dacs[col][count] << endl;
        }
        count++;
      }
    }
    for(int co = 0; co < 64; co++){//find lowest std for each column
      index = min_element(Std[co].begin(), Std[co].end()) - Std[co].begin();
      outfile <<  row << " " << co + 1  << " " <<  Dacs[co][index] << " "  << Pols[co][index] << " " << endl;
    }
  }
  outfile.close();
}

void TuneStripmode(unsigned int nSteps, int uptorow, float range, float offset, bool resetCalib, int nStrobes, unsigned int nsleep, float nom_mean, ChipConfig *config, string nameofrootfile= ""){
  if (config == NULL) {config = new ChipConfig();}
  config->disable();
  char rootfilename[200] = "";
  if(!(nameofrootfile==""))
    sprintf(rootfilename,"%s",nameofrootfile.c_str());
  else
    sprintf(rootfilename,"TuningStripMode.root");
  config->enableRow(63); // config that is passed for row 0 is being used here.
  bool txtheader = true;
  for(int row = 62; row > uptorow; row--){
    config->enableRow(row); // enable all rows after another
    TuningScanRow(nSteps, row, range, offset, resetCalib, nStrobes, nsleep, config, rootfilename, "", false, true, false, false); // Repeat Tuning while adding more enabled rows
    // Select minimum std of all 32 dac configurations for each pixel
    string configouttxt = "optimal_DACs_StripMode.txt";
    Find_minStd(rootfilename, configouttxt, row, row +1, txtheader);
    txtheader = false;
    ReadConfig(configouttxt, config); // should read in config from new txt-file
    writeChip(config);
  }
}

// set all pixel dacs to same config value except of one row ROW and a col COL -if it is passed a value < 64
// scan the row with and without all other rows enabled.
void TuneStripallEnabled(unsigned int ROW, unsigned int enableROWS, unsigned int nSteps, float range, float offset, unsigned int nStrobes, string configtxt, unsigned int DAC = 15, bool polarity = false, unsigned int COL=70){
  ChipConfig* conf = new ChipConfig();
  ReadConfig(configtxt, conf);
   for(unsigned int row = 0; row<64; row++){
     for(unsigned int col = 0; col<64; col++){
       if (row == ROW) break;
       if (col > COL) setDACperPix(col, row, DAC, polarity, false, conf);}
   }
   writeChip(conf);
   counterThresholdScanRow(nSteps, ROW, range, offset, true, nStrobes, 1100, conf);
   for(unsigned int row = 0; row<enableROWS; row++) conf->enableRow(row);
   writeChip(conf);
   counterThresholdScanRow(nSteps, ROW +100, range, offset, true, nStrobes, 1100, conf);
  }


// Tune Column col pixelwise while all other columns are enabled (if maskChip set to false).
// The resulting root file can be later analyzed via TunePixwisePerCol() that plots std and other variables describing the single pixel counts.
// PlotAllConfigsPixwisePerCol() produces png plots that can be combined to form a gif showing the Tuning of the pixel and the other strips.
void TuneEnabledChipCol(unsigned int nSteps, unsigned int col, float range, float offset, bool resetCalib, int nStrobes, unsigned int nsleep, std::string configtxt="", unsigned int rowmin =0, unsigned int rowmax = 64, std::string nameoffile="", bool update = true, bool maskChip = false) {
  ChipConfig* config = new ChipConfig();
  for(unsigned int row = rowmin; row<rowmax; row++){
    if (configtxt == "") {config = new ChipConfig();}
    else {ReadConfig(configtxt, config);} // reset config when scaning other pixel
    for( unsigned int pol = 0; pol<2; pol++){
      bool polarity = !!pol; // for int to bool conversion
      for(unsigned int DAC = 0; DAC<16; DAC++){
	setDACperPix(col, row, DAC, polarity, maskChip, config); // config whole Chip
	if (maskChip) config->disable(); // somehow previous line doesnt mask the other strips.... Do here again
	config->disableCol(col); // disable other pixel in column
	config->enablePix(col, row); // enable just one pix in column
	writeChip(config);
	double step=(range/((double) nSteps-1))/2.0;
	char histoname[200] = "";
	sprintf(histoname,"TuningScanCol%02dRow%02dDAC%02dPol%d",col, row, DAC, pol);
	char filename[200] = "";
	if(!(nameoffile==""))
	  sprintf(filename,"%s",nameoffile.c_str());
	else
	  sprintf(filename,"TuningScanCol%02d.root",col);
	char fileoption[200] = "";
	if(update)
	  sprintf(fileoption,"update");
	else
	  sprintf(fileoption,"recreate");
	TH2D *myHisto = new TH2D(histoname, histoname, 64, -0.5, 63.5, nSteps, offset-step, offset+range+step);
	std::vector<StreamData> toBeHistogrammed;
	for(unsigned int i=0; i<nSteps; i++) {
	  resetPixels(0);
	  setThreshold((float) ((range/(nSteps-1)*i)+offset));
	  unsigned int nHits=0;
	  std::vector<int> temp = captureCounters(nStrobes, 16, nsleep);
	  for(int index=0; index<64; index++) {
	    myHisto->Fill((float) (index), (float) ((range/(nSteps-1)*i)+offset), temp[63-index]);
	    nHits += temp[63-index];
	  }
	  //std::cout << " Hit Count was: " << nHits << std::endl;
	  toBeHistogrammed.clear();
	}
	TFile *myfile = new TFile(filename,fileoption);
	myfile->cd();
	myHisto->Write(myHisto->GetName(),TObject::kOverwrite);
	myfile->Close();
	std::cout << " Saved histo " << myHisto->GetName() << " to file " << myfile->GetName() << std::endl;
	delete myHisto;
	delete myfile;
      }
    }
  }
}

// specify the column and a config txt file. The config of one column is read out and used to configure each column.
void ReadOneColConfigtoAllCols(unsigned int column,string filename, ChipConfig *config, bool header = true){
  if (config == NULL) {config = new ChipConfig();}

  std::ifstream infile(filename);
  std::string line;

  if(header)
    getline(infile, line); // skips header line
  unsigned int row , col, dac, pol;
  bool masked = false;
  while(getline(infile, line)) {
    std::istringstream iss(line);
    iss >> row >> col >> dac >> pol;
    bool polarity = !!pol; // for int to bool conversion
    col = 64 - col; // naming convention is reversed
    if(col == column){
			for(unsigned int c=0; c<64; c++) {
			setDACperPix(c, row, dac, polarity, masked, config); // set all 64 pixels in each row to same dac.
			}
		}
	}
}

void UnmaskStripafterStrip(string configtxt, unsigned int nStrobes, string nameoffile = "", bool startat0=true){
  ChipConfig* config = new ChipConfig();
  if (configtxt == "") {config = new ChipConfig();}
  else {ReadConfig(configtxt, config);} // reset config when scaning other pixel
  config->disable();
  char filename[200] = "";
  if(!(nameoffile==""))
    sprintf(filename,"%s",nameoffile.c_str());
  else
    sprintf(filename,"counterThresholdScanUnmaskStrips.root");
  unsigned int col;
  for(unsigned int i = 0; i<64; i++){
    if(startat0) col = i;
    else col = 63 - i;
    config->enableCol(col);
    char histoname[200] = "";
    sprintf(histoname,"counterThresholdScanUnmaskedStrip%02d",col);
    counterThresholdScanRow(141, 100, 0.14, 1.12, true, nStrobes, 1100, config, filename, histoname, false, true);
  }
}

// Do multiple thresholdScans when changing a parameters value through the list of par_vals.
// The paramter to be varied is specified via partovary and can be nsleep or maxcap
void VarycTScanparameters(unsigned int nSteps, unsigned int row, float range, float offset, unsigned int nStrobes, unsigned int valstart, unsigned int valend, unsigned int valstep, string partovary="nsleep", string configtxt="", string nameoffile = "", int nRepeat = 50){
  ChipConfig* config = new ChipConfig();
  if (configtxt == "") {config = new ChipConfig();}
  else {ReadConfig(configtxt, config);} // reset config when scaning other pixel
  char filename[200] = "";
  if(!(nameoffile==""))
    sprintf(filename,"%s",nameoffile.c_str());
  else
    sprintf(filename,"counterThresholdScan_%sVariation.root", partovary.c_str());
  std::vector<unsigned int> par_vals;
  unsigned int valnum = (valend-valstart)/valstep + 1;
  for(unsigned int i = 0; i<valnum;++i){
    par_vals.push_back(valstart + i*valstep);
  }
  for(unsigned int i = 0; i<par_vals.size();++i){
    char histoname[200] = "";
    sprintf(histoname,"counterThresholdScan%s%04d", partovary.c_str(), par_vals[i]);
    if (partovary=="nsleep")
      counterThresholdScanRow(nSteps, row, range, offset, true, nStrobes, par_vals[i], config, filename, histoname, false, true, 2000, nRepeat);
    else if (partovary=="maxcap")
      counterThresholdScanRow(nSteps, row, range, offset, true, nStrobes, 1100, config, filename, histoname, false, true, par_vals[i], nRepeat);
  }
}

// this function is searching for the course of the horizontal lines that sometimes appear in a threshold Scan.
// when a count is detected, the function breaks and the threshold voltage can be measured manually at the motherboard

void breakwhenCount(unsigned int nSteps, unsigned int row, float range, float offset, long long int nStrobes, unsigned int nsleep, ChipConfig *config, unsigned int MaxCap = 2000, int NRepeat = 50) {
  if (config == NULL) {config = new ChipConfig();}
  if (row < 64) {
    config->disable();
    config->enableRow(row);
  }
  writeChip(config);
  double step=(range/((double) nSteps-1))/2.0;
  for(unsigned int i=0; i<nSteps; i++) {
    std::cout << "Threshold step: " << std::dec << i << " Value is: " << (float) ((range/(nSteps-1)*i)+offset) << " Sleeping " << nsleep << "BCs" << std::endl;
    resetPixels(0, NRepeat);
    setThreshold((float) ((range/(nSteps-1)*i)+offset));
    unsigned int nHits=0;
     writeChip(config); // try whether this creates extra counts
    std::vector<int> temp = captureCounters(nStrobes, 16, nsleep, MaxCap, NRepeat);
    for(int index=0; index<64; index++) {
      nHits += temp[63-index];
    }
    std::cout << " Hit Count was: " << nHits << std::endl;
    if(nHits > 0) break;
  }
}

// Multiple resets being done while changing the register for the delay.
// An oscilloscope measures the time difference between the peaks from the reset for the test pixel's analog output. 
void Resetpixels_delayvariation(unsigned int delay1, unsigned int delay2){
  resetPixels(0);
  resetPixels(0);
  e->ConfigureVariable(10035, (delay1>>16)&0xffff);
  e->ConfigureVariable(10034, (delay1&0xffff));
  resetPixels(0);
  resetPixels(0);
  e->ConfigureVariable(10035, (delay2>>16)&0xffff);
  e->ConfigureVariable(10034, (delay2&0xffff));
  resetPixels(0);
  resetPixels(0);
}


// Variation of current defined by i2c_addr and dacnum in given range. Current can be for example IShaperBias (12,1), IPreAmpBias (12,2) or IPFBPreAmp (13,2).
// For each of the currents a scan through a parameter like "maxcap" or "nsleep" is performed.
void Current_cTScanparameter_Variation(unsigned int i2c_addr, unsigned int dacnum, unsigned int ISteps, float Irange, float Ioffset, unsigned int nSteps, float range, float offset, int nStrobes, unsigned int valstart, unsigned int valstop, unsigned int valstep, string parameter, string configtxt){
  setupAnalog();
  usleep(10000); // sleeps in microseconds
  for (unsigned int i=0; i< ISteps+1; i++){
    float Ival = Ioffset + i*Irange/ISteps;
    setIDAC(i2c_addr, dacnum, Ival); // wait after changing current
    usleep(10000);
    std::cout << "Current set to " <<   Ival << " uA" << endl;
    char filenameChip[200] = "";
    sprintf(filenameChip,"Current_nsleepVariation_Chip_Ival%.0f.root", Ival);
    char filenameRow[200] = "";
    sprintf(filenameRow,"Current_nsleepVariation_Row31_Ival%.0f.root", Ival);

    VarycTScanparameters(nSteps, 100, range, offset, nStrobes, valstart, valstop, valstep, parameter, configtxt, filenameChip );
    VarycTScanparameters(nSteps, 31, range, offset, nStrobes, valstart, valstop, valstep, parameter, configtxt, filenameRow );
  }
}


// NEW FROM HERE ON

// Determination of voltage drift under variations of calibration DAC current and some values of tuning DAC
// For each of the currents a scan through a parameter like "maxcap" or "nsleep" is performed. 
// "maxcap" allows for V_drift determination
// IcalibDAC has (12,0) as i2c_addr

void CalibDACcurrent_cTScanparameter_Variation(unsigned int row, unsigned int i2c_addr, unsigned int dacnum, unsigned int Inumvalues, float Istepsize, float Ioffset, unsigned int nSteps, float range, float offset, int nStrobes, unsigned int valstart, unsigned int valstop, unsigned int valstep, unsigned int polstart=0, unsigned int DACstart = 0, unsigned int DACstop = 15, unsigned int DACstep =1, string parameter="nsleep"){
	setupAnalog();
  	usleep(10000); // sleeps in microseconds
    for (unsigned int i=0; i< Inumvalues; i++){ // Variation of current
    	float Ival = Ioffset + i*Istepsize;
		setIDAC(i2c_addr, dacnum, Ival);
		usleep(10000);
    	std::cout << "Current set to " <<   Ival << " uA" << endl;
    	    	
    	ChipConfig *config = new ChipConfig(); 
		for( unsigned int pol = polstart; pol<2; pol++){ 
			bool polarity = !!pol; // for int to bool conversion
			for(unsigned int DAC = DACstart; DAC<DACstop+1; DAC+=DACstep){ // Variation of Dac and pol. All pixels to same value
				for(unsigned int col = 0; col<64; col++){
					setDACperPix(col, row, DAC, polarity, false, config); // config whole Chip 
				}
	
				writeChip(config);
				char filenameRow[200] = "";
				sprintf(filenameRow,"Current_nsleepVariation_Row%02d_Ival%.0f_DAC%02dPol%d.root",row, Ival, DAC, pol);

				// VarycTScanparameters(nSteps, row, range, offset, nStrobes, valstart, valstop, valstep, parameter, configtxt, filenameRow );
				// nsleep variation for V_drift determination
				std::vector<unsigned int> par_vals;
				unsigned int valnum = (valstop-valstart)/valstep + 1;
				for(unsigned int k = 0; k<valnum;++k){
					par_vals.push_back(valstart + k*valstep);
					}
					for(unsigned int j = 0; j<par_vals.size();++j){
					char histoname[200] = "";
					sprintf(histoname,"counterThresholdScan%s%04d", parameter.c_str(), par_vals[j]);
					if (parameter=="nsleep")
					  counterThresholdScanRow(nSteps, row, range, offset, true, nStrobes, par_vals[j], config, filenameRow, histoname, false, true, 2000, 50);
					else if (parameter=="maxcap")
					  counterThresholdScanRow(nSteps, row, range, offset, true, nStrobes, 1100, config, filenameRow, histoname, false, true, par_vals[j], 50);
				}		
			}
		}
	}
}



string Biasstring(float biasvoltage){
  int v1 = (int)biasvoltage;
  int v2 = (int) ((biasvoltage+1)*10)% ((v1+1)*10);
  string bv =  (to_string(v1)+"p"+to_string(v2)).c_str();
  return bv;
}

void counterThresholdScanRow_bias(unsigned int nSteps, unsigned int row, float range, float offset, bool resetCalib, long long int nStrobes, unsigned int nsleep, ChipConfig *config, float biasvoltage,std::string nameoffile="", std::string nameofhisto="", bool singlerow = true, bool update = false, unsigned int MaxCap = 2000, int NRepeat = 50, bool ThreshLowtoHigh = true, bool triggered = false) {
  // Some examples of what could be done with a configuration
  string bv =  Biasstring(biasvoltage);
  if (config == NULL) {config = new ChipConfig();}
  if (row < 64) {
    config->disable();
    config->enableRow(row);
  }
  writeChip(config);
  double step=(range/((double) nSteps-1))/2.0;
  char histoname[200] = "";
  if(!(nameofhisto==""))
    sprintf(histoname,"%s",nameofhisto.c_str());
  else
    sprintf(histoname,"counterThresholdScanRow%02d_Bias_%s",row, (bv).c_str());
  char filename[200] = "";
  if(!(nameoffile==""))
    sprintf(filename,"%s",nameoffile.c_str());
  else if(singlerow)
    sprintf(filename,"counterThresholdScanRow%02d_Bias_%s.root", row, (bv).c_str());
  else
    sprintf(filename,"counterThresholdScanMultipleRows_Bias_%s.root", (bv).c_str());
  char fileoption[200] = "";
  if(update)
    sprintf(fileoption,"update");
  else
    sprintf(fileoption,"recreate");
  TH2D *myHisto = new TH2D(histoname, histoname, 64, -0.5, 63.5, nSteps, offset-step, offset+range+step);
  std::vector<StreamData> toBeHistogrammed;
  unsigned int i;
  for(unsigned int j=0; j<nSteps; j++) {
    if (ThreshLowtoHigh) i = j;
    else i = nSteps - 1 - j;
    resetPixels(0, NRepeat);
    setThreshold((float) ((range/(nSteps-1)*i)+offset));
    std::cout << "Threshold step: " << std::dec << i << " Value is: " << (float) ((range/(nSteps-1)*i)+offset) << " Sleeping " << nsleep << "BCs" << std::endl;
    unsigned int nHits=0;

    std::vector<int> temp = captureCounters(nStrobes, 16, nsleep, MaxCap, NRepeat);
    // And here is where the histogramming should happen over toBeHistogrammed[index]->getRawData(N[0-99])...
    for(int index=0; index<64; index++) {
      myHisto->Fill((float) (index), (float) ((range/(nSteps-1)*i)+offset), temp[63-index]);
      nHits += temp[63-index];
    }
    std::cout << " Hit Count was: " << nHits << std::endl;
    toBeHistogrammed.clear();
  }
  TFile *myfile = new TFile(filename,fileoption);
  myfile->cd();
  myHisto->Write(myHisto->GetName(),TObject::kOverwrite);
  myfile->Close();
  std::cout << " Saved histo " << myHisto->GetName() << " to file " << myfile->GetName() << std::endl;
  delete myHisto;
  delete myfile;
}

void counterThresholdScanAllRows_bias(unsigned int nSteps, float range, float offset, bool resetCalib, int nStrobes, unsigned int nsleep, ChipConfig *config,float biasvoltage, std::string nameoffile="", std::string nameofhisto="") {
  for(unsigned int r = 0; r<64;++r){
    counterThresholdScanRow_bias(nSteps, r, range, offset, resetCalib, nStrobes, nsleep, config, biasvoltage, nameoffile, nameofhisto, false, true);
  }
}

//This function scans the chip changing bias voltages from Biasmin to Biasmax in steps of BiasStep, one row at a time.
//This function is written for testing at the noise threshold
void ScanBias(float Biasmin, float Biasmax, ChipConfig *config, float BiasStep=0.5){
  for (float i=Biasmin; i <= Biasmax; i+=BiasStep){
    setBias((float)i);
    counterThresholdScanAllRows_bias(131, 0.04, 1.17, true, 100000, 1100, config, i);
  }
}

//This function scans the chip changing bias voltages from Biasmin to Biasmax in steps of BiasStep, all rows enabled.
//This function is written for testing at the noise threshold
void ScanBias_allEnabled(float Biasmin, float Biasmax, ChipConfig *config, float BiasStep=0.5){
  for (float i=Biasmin; i <= Biasmax; i+=BiasStep){
    setBias((float)i);
    counterThresholdScanRow_bias(131, 100, 0.04, 1.17, true, 100000, 1100, config, i);
  }
}

//This function scans the chip changing bias voltages from Biasmin to Biasmax in steps of BiasStep, all rows enabled.
//This function is written for source testing, a long recording time is needed.
void ScanBias_allEnabled_source(ChipConfig *config, unsigned int warmupmin = 10){
   warmup(warmupmin);
   for (float i=0; i <= 2.1; i += 0.5){
     setBias((float)i);
     counterThresholdScanRow_bias(2, 100, 0.16, 0.92, true, 4000000000, 1100, config, i);
   }
   for (float i=4; i <= 6.1; i += 2){
     setBias((float)i);
     counterThresholdScanRow_bias(2, 100, 0.16, 0.92, true, 4000000000, 1100, config, i);
   }
}

void longterm_measurement_Row(unsigned int minutes, float biasvoltage, unsigned int row, unsigned int nSteps, float range, float offset,  unsigned long long int nStrobes, string configtxt , string fileprefix=""){
  ChipConfig* config = new ChipConfig();
  if (configtxt == "") {config = new ChipConfig();}
  else {ReadConfig(configtxt, config);}
  setBias((float)biasvoltage);
  warmup(minutes, configtxt);
  char PreScanfilename[200] = "";
  char MainScanfilename[200] = "";
  char EndScanfilename[200] = "";
  sprintf(PreScanfilename,"%s_PreScan.root", fileprefix.c_str());
  sprintf(MainScanfilename,"%s_MainScan.root", fileprefix.c_str());
  sprintf(EndScanfilename,"%s_EndScan.root", fileprefix.c_str());
  counterThresholdScanRow(151, row, 0.15, 1.12, true, 1000000, 1100, config, PreScanfilename);
  counterThresholdScanRow(nSteps, row, range, offset, true, nStrobes, 1100, config, MainScanfilename);
  counterThresholdScanRow(151, row, 0.15, 1.12, true, 1000000, 1100, config, EndScanfilename);
}
