#ifndef DECAL_MOTHERBOARD_H
#define DECAL_MOTHERBOARD_H 1

#include <vector>
#include "../stdll/DecalConfig.h"

#include "../stlib/sct_types.h"

#include "../stdll/TST.h"

#include "hsioUtils.h"
#include "MattTests.h"
#include "MattTests_Common.h"

#define CALIB_DATA_IN_V				0x0001
#define CALIB_EN							0x0002
#define DAC_CLOCK_IN 					0x0004
#define DAC_RESET_BAR					0x0008
#define DAC_CLOCK_IN_V				0x0010
#define COMP_RST_BAR_EXT			0x0020
#define OPERATE								0x0040
#define TEST_CLOCK_IN					0x0080
#define TEST_DATA_IN					0x0100
// previous define
//#define FRAME_CLOCK_PERIPHERY	0x0200

#define GOOD_FIRMWARE					0xB1CC // Good Firmware on 18.12.2017

// Old funtionality
//void strobe_Periphery(std::vector<uint16_t> &data, unsigned int nStrobes=1);

// NOW I need a global variable...
uint16_t configRegister_val;
uint16_t ioDirection;

// standard value is 10k Ohm, DO NOT GET THIS WRONG!
//#define CURRENT_DAC_LOAD_RESISTOR 10000.
// New value (to be fixed on all motherboards) is 1k
#define CURRENT_DAC_LOAD_RESISTOR 1000.

//returns maximum current (set by CURRENT_DAC_LOAD_RESISTOR) in uA
float getMaxCurrent() { return 1.0e6*2./(CURRENT_DAC_LOAD_RESISTOR); }

void printBinary(unsigned int number, unsigned int nBits=16, unsigned int startBit=15);
void setConfigRegister(uint16_t  newVal, bool forceWrite=false);

void setManualPhase(bool onOff, unsigned int value);
uint16_t getManualPhase();
bool getManualPhaseOnOff();
/// Function to generate a stream strobing a single bit into the test register
void writeBit_TestReg(std::vector<uint16_t> &data, unsigned int bit=0);
/// Function to generate a stream strobing a full value into the test register
void writeVal_TestReg(std::vector<uint16_t> &data, unsigned int val=0, unsigned int nTimes=1);
/// Function to generate a stream loading up to the full test register
void setTestRegister(std::vector<uint16_t> &data, std::vector<uint16_t> testValues);
/// Resetting all test register content to 1s (as in each column gets 1 as a value)
void fillZeros(std::vector<uint16_t> &data);
uint16_t readRegister(unsigned int registerAddress, bool verbose=false);
uint16_t readStatus(unsigned int registerAddress, bool verbose=false);

void slowPush(std::vector<uint16_t> &data, uint16_t value, int nRepeat=2);
void writeBit_V(std::vector<uint16_t> &data, int bit, int nRepeat=2);
void writeBit_H(ColumnConfig *cfg, int bit, int nRepeat=2);
void resetPeriphery();
void writeColumn(ColumnConfig *cfg, int nRepeat=2, bool drop=false);

void writeChip(ChipConfig *chip, int nRepeat=4);
void dumpWord(unsigned int word);
void debugConfig(std::vector<uint16_t> &data);

StreamData receiveData(unsigned int requested);
void captureData(std::vector<StreamData> &returnData, unsigned int capLength, unsigned int nChannels, bool sorted=true, bool resetPix=false, unsigned int delay=0);
void printData(std::vector<StreamData> &data, bool sorted = false); //sorting is false, needs debug
void debugMe(unsigned int capLength, unsigned int nChannels, bool sorted=false); //sorting is false, needs debug
// Remove the following two, DebugMe needs renaming, but does what is required.

// firmware check to verify that the correct firmware was loaded to Nexys!
bool firmwareIsGood();

uint16_t powerDOWN(bool withPLL=false);
uint16_t nexys_writeI2C(unsigned int i2c_addr, unsigned int channel, std::vector<unsigned int> dataS);
uint16_t setDAC(unsigned int i2c_addr, unsigned int dacnum, unsigned int value=32768);
uint16_t setVDAC(unsigned int i2c_addr, unsigned int dacnum, float value, bool iAmSmart=false);
// Set a current load in uA
uint16_t setIDAC(unsigned int i2c_addr, unsigned int dacnum, float value);
uint16_t setAllCurrentDACs(unsigned int value=32768);
uint16_t setAllCurrentDACs(float value=100.);
void setThreshold(unsigned int value);
void setThreshold(float value);
void setBias(unsigned int value);
void setBias(float value);

void sendData(std::vector<uint16_t> &data, bool verbose=false);

void inject_number(unsigned int value, unsigned int nTimes=1, bool verbose=false);
void inject_numbers(std::vector<unsigned int> values, bool verbose=false);
void reset_testVector(bool verbose=false);
void inject_one() { inject_number(1); }
void inject_zero() { inject_number(0); }

void setupDigital();
void setupAnalog();
void setIoDir(unsigned int value);
void startUP(bool modeSel, bool startPLL=true, bool resetTestRegister=true, bool resetPhase=true);

void resetPixels(unsigned int delay=0, int nRepeat=50);
void operatePixels(bool connectPixel=true);
void calibratePixels(bool connectDAC=false);

void loadDecalmacros(void);

#endif /// ends ifndef DECAL_MOTHERBOARD_H
