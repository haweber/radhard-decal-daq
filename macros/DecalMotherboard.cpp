#include "DecalMotherboard.h"


// Functions around motherboard/firmware operation, sending data to chips for config
// Should add trigger functions
// Generally needs cleanup.

/*
15	FRMCLKPEREN	frameClockPeriph enable	LA31p
14	FRMCLKEN	frameClockArray enable	LA18
13	SAMPLEODD	Sampe the od bits of the 640Mb ISERDES (use if seeing bit errors)
12	AUTOPH_HOLD	hold auto bit-byte phase value
11	MANPH_EN	Manual bit-byte phase set enable
10:8	MANPH	Manual bit-byte phase value
7	 	Unused
6:4	CLK8OFFS	DataClk8 offset
3	 	Unused
2	PLLOVRDSEL	PLLOverrideSel	LA27p
1	MODESEL	modeSel	LA30p
0	POWERUP	POWER_UP/DOWN	LA30n
*/

/// DAC definitions
/// Currents:
// ICalibDAC: 12, 0
// IShaperBias: 12, 1
// IPreAmpBias: 12, 2
// IBiasPLL: 12, 3
// IBiasCL: 13, 0
// IBiasL: 13, 1
// IPFBPreAmp: 13, 2
// ICompTail: 13, 3
/// Voltages
// HVBias: 14, 0
// VThreshold: 14, 1
// VShaperCascN: 14, 2
// VGuardRing: 14, 3

void printBinary(unsigned int number, unsigned int nBits, unsigned int startBit) {
	unsigned int debugCount = 0;
	number = number << (31-startBit);
	while (debugCount < nBits) {
		if (number & (0x1 << 31)) {
			printf("1");
		} else {
			printf("0");
		}
		number = number << 1;
		debugCount++;
		if (debugCount > 256 || debugCount > nBits) {
			break;
		}
	}
	return;
}

void setConfigRegister(uint16_t newVal, bool forceWrite) {
	if ((configRegister_val != newVal) || forceWrite) {
		e->ConfigureVariable(10018, newVal);
		configRegister_val = newVal;
	}
	return;
}

void setManualPhase(bool onOff, unsigned int value) {
	// Be blunt and just set the value, rather than checking if it is ok:
	uint16_t tempVal = configRegister_val & 0xD0FF; //< This clears all the bits in the configuration register value that belong to manual phase adjustment
	if (onOff) {
		tempVal |= 0x800;
	}
	tempVal |= ((value & 0xe) << 7);
	if (value & 0x1) {
		tempVal |= 0x2000;
	}
	setConfigRegister(tempVal);
	return;
}

uint16_t getManualPhase() {
	return ((configRegister_val & 0x2000) >> 13) + ((configRegister_val >> 7) & 0x7);
}

bool getManualPhaseOnOff() {
	return ((configRegister_val & 0x800) > 0);
}

void writeBit_TestReg(std::vector<uint16_t> &data, unsigned int bit) {
	uint16_t SIN = 0x0;
	if (bit) {
		SIN = TEST_DATA_IN;
	}
	data.push_back(SIN | TEST_CLOCK_IN); data.push_back(SIN | TEST_CLOCK_IN);
	data.push_back(SIN); data.push_back(SIN);
	return;
}

void writeVal_TestReg(std::vector<uint16_t> &data, unsigned int val, unsigned int nTimes) {
	for (unsigned int iTimes=0; iTimes<nTimes; iTimes++) {
		for (int i=0; i<5; i++) {
			writeBit_TestReg(data, (val & (1 << i)));
		}
	}
	return;
}

void setTestRegister(std::vector<uint16_t> &data, std::vector<uint16_t> testValues) {
	if (testValues.size() != 64) {
		printf("Warning, writing %zu test values into test register rather than 64.\n", testValues.size());
	}
	for (unsigned int i=0; i<testValues.size(); i++) {
		writeVal_TestReg(data, testValues.at(i));
	}
	return;
}

void fillZeros(std::vector<uint16_t> &data) {
	data.push_back(0); data.push_back(0);
	return;
}

void sendData(std::vector<uint16_t> &data, bool verbose) {
//	printf("Sending data of size %lu\n", data.size());
	if (verbose) {
		for (unsigned int i=0; i<data.size(); i++) {
			printf("%04x\n", data.at(i));
		}
	}
	e->HsioSendReceiveOpcode(0x0078, 0x1234, data.size(), &data[0],0,0);
	return;
}

/// This takes the internal register number, i.e. 18 for EXT_HW_DRV
uint16_t readRegister(unsigned int registerAddress, bool verbose) {
	uint16_t data[] = {1};
	uint16_t opcode = 0x15;
	e->HsioSendReceiveOpcode(opcode, 0x1234, 0, data,0,0);

	uint16_t seqnum;
	uint16_t length;
	uint16_t *recv_data;

	e->HsioReceiveOpcode(opcode, seqnum, length, recv_data);
	if(recv_data == 0) {
		printf("No data from HSIO\n");
		return 0;
	}

	uint16_t reg_block[length];

	for (int n=0; n<length; n++) {
		reg_block[n] = ((recv_data[n] & 0xff) << 8) | ((recv_data[n] & 0xff00) >> 8);
	}
	
	if (verbose) {
		printf("Register %02d has content: %04x", registerAddress, reg_block[registerAddress]);
	}
	return reg_block[registerAddress];
}

uint16_t readStatus(unsigned int registerAddress, bool verbose) {
	uint16_t data[] = {1};
	uint16_t opcode = 0x19;
	e->HsioSendReceiveOpcode(opcode, 0x1234, 0, data,0,0);

	uint16_t seqnum;
	uint16_t length;
	uint16_t *recv_data;

	e->HsioReceiveOpcode(opcode, seqnum, length, recv_data);
	if(recv_data == 0) {
		printf("No data from HSIO\n");
		return 0;
	}

	uint16_t stat_block[length];

	for (int n=0; n<length; n++) {
		stat_block[n] = ((recv_data[n] & 0xff) << 8) | ((recv_data[n] & 0xff00) >> 8);
	}
	
	if (verbose) {
		printf("Status Register %02d is: %04x", registerAddress, stat_block[registerAddress]);
	}
	return stat_block[registerAddress];
}


void slowPush(std::vector<uint16_t> &data, uint16_t value, int nRepeat) {
	for (int i=0;i<nRepeat; i++) {
		data.push_back(value);
	}
}


void writeBit_V(std::vector<uint16_t> &data, int bit, int nRepeat) {
	uint16_t SIN = (bit?CALIB_DATA_IN_V:0x000);
// Could preset data here to allow for settling times in case rising edge does something    
//    data.push_back(SIN); data.push_back(SIN);
	slowPush(data, (SIN | DAC_CLOCK_IN_V), nRepeat);
// Data is read into vertical shift register on falling edge transition, i.e. now
	slowPush(data, SIN, nRepeat);
	return;
}

// Chip is configured from column 0, row 63, down the column and then across columns, i.e. column 63 data has to go in first with row 0 as first bits up to row 63, followed by column 62 data from row 0 to 63.
// Bits starting at MSB first
// this function will load a whole set of config bits into the vertical shift register and strobe it into the horizontal shift registers
void writeBit_H(ColumnConfig *cfg, int bit, int nRepeat) {
	// read one bit after the other out of a column of the chip config
	std::vector<uint16_t> data;
	data.clear();
	for (int i=0; i<64; i++) {
		writeBit_V(data, (cfg->getPixConfig(i)->getConfig() & (1<<bit)), nRepeat); //< This is a total of 64*4=256 Words added to the buffer, can only do one vertical bit per packet really...
		if (data.size()>512) {
			sendData(data);
			data.clear();
		}
	}
// Following the vertical strobing, send a small gap and then a Puls on DAC_CLOCK_IN (Horizonntal clock), to feed data from the vertical shift register into the Pixel Array
	slowPush(data, 0, nRepeat);
	slowPush(data, DAC_CLOCK_IN, nRepeat);
	slowPush(data, 0, nRepeat);
	sendData(data);
	data.clear();
	return;
}

void resetPeriphery() {
	std::vector<uint16_t> data;
	writeBit_V(data, 0);
	sendData(data);
	data.clear();
}

void writeColumn(ColumnConfig *cfg, int nRepeat, bool drop) {
	int nBits=6;
	if (drop) nBits--;
	for(int i=0;i<nBits;i++) {
		writeBit_H(cfg, (5-i), nRepeat);
	}
	return;
}

void writeChip(ChipConfig *chip, int nRepeat) {
	for(int i=0;i<64;i++) {
		writeColumn(chip->getColConfig(63-i), nRepeat);
	}
	return;
}

void dumpWord(unsigned int word) {
	for (unsigned int i=0; i<16; i++) {
		if ((0x8000 >> i) & word) {
			std::cout << "1";
		} else {
			std::cout << "0";
		}
	}
	std::cout << std::endl;
}

void debugConfig(std::vector<uint16_t> &data) {
	std::cout << "Dumping " << data.size() << " words to screen" << std::endl;
	for (unsigned int i=0; i<data.size(); i++) {
		dumpWord(data.at(i));
	}
}

StreamData receiveData(unsigned int requested) {
	uint16_t opcode;
	uint16_t seqnum;
	uint16_t length;
	uint16_t *recv_data=0;
	opcode = 0xd004; // 4 for capture, 8 for ABC130 packets
	e->HsioReceiveOpcode(opcode, seqnum, length, recv_data); // Allready receiving Data - but we have not yet sent any triggers...
	if(recv_data == 0) {
		printf("No data ...\n");
		return StreamData();
	} else {
		StreamData temp(length, recv_data, requested);
		return temp;
	}
}

void captureData(std::vector<StreamData> &returnData, unsigned int capLength, unsigned int nChannels, bool sorted, bool resetPix, unsigned int delay)
{
	returnData.clear();
	// Check capLength
	if (capLength < 1) {
		printf("Not capturing nothin', screw you!\n");
		return;
	}
//	printf("Capturing %d words\n", capLength);
	e->HsioFlush();
	e->ConfigureVariable(10019, 0x1000); // Disable Trigger-Data packets
	e->ConfigureVariable(10007, capLength); // Seems to request a certain length of capture data?

	streamConfigWriteAll(0xffff, 0);
	streamConfigWrite(0xffff, 0, 0x8811, nChannels*2);
	e->ConfigureVariable(10023, 0x880); // set trig = capt start
    e->ConfigureVariable(10001, 0x0003); // trigger output enable

	e->HsioFlush();
    if (resetPix) {
    	resetPixels(delay);
    }
    Matt_RawSigsPulse(1,0x8000); // Trying something new
	//hsioCommand(1);


	for(unsigned int channel=0; channel<nChannels; channel++) {
		returnData.push_back(receiveData(capLength)); // Allready receiving Data - but we have not yet sent any triggers...
//        printf("Received Data for Channel %02d\n", returnData.back().getChannel());
	}

	streamConfigWriteAll(0xffff, 0);

	if(sorted) {
		std::vector<StreamData> temp;
		temp = returnData;
		returnData.clear();
		unsigned int channelIndex = 0;
		while (channelIndex < 255) {
			for (unsigned int i=0; i<temp.size(); i++) {
				if (temp[i].getChannel() == channelIndex) {
					returnData.push_back(temp[i]);
					break;
				}
			}
			channelIndex++;
		}
	}
//// ROOT 6 Version can do the following
//	if (sorted) {
//		std::sort(returnData.begin(), returnData.end());
//	}
	return;
}

void printData(std::vector<StreamData> &data, bool sorted) {
	unsigned int channelIndex = 0;
	while (channelIndex < 255) {
		for (unsigned int i=0; i<data.size(); i++) {
			if (data[i].getChannel() == channelIndex) {
				printf("Dumping data for index %02d\n", i);
				data[i].dump(false);
				break;
			}
		}
		channelIndex++;
	}
}

void debugMe(unsigned int capLength, unsigned int nChannels, bool sorted) {
	// Check capLength
	if (capLength < 1) {
		printf("Not capturing nothin', screw you!\n");
		return;
	}
	printf("Capturing Data on %d streams\n", nChannels);
	std::vector<StreamData> v;
	v.clear();
	captureData(v, capLength, nChannels, true, true, 0);
	printf("Received Data, number of Streams in returned Data is %zu\n", v.size());
	printData(v, sorted);
	v.clear();
}

uint16_t powerDOWN(bool withPLL) {
	if (withPLL) {
		setConfigRegister(0x4 | 0xc000, true);
	} else {
		setConfigRegister(0x0 | 0xc000, true);
	}
	return 0;
}

uint16_t nexys_writeI2C(unsigned int i2c_addr, unsigned int channel, std::vector<unsigned int> dataS) {
	// Not sure where this limit comes from, but something stops working
	const int size = 63;
	uint16_t ocd[size];
	int o = 0;
	// Fit in buffer
	//if(repeats > size - 12) repeats = size - 12;  
	unsigned int I2C_speed = 0; // 0 = 100kHz, 1 = 10kHz, 2 = 1kHz
	ocd[o++]=(I2C_speed << 4) | (channel & 0xf); // 10kHz, channel 15
	if (dataS.size() == 0) {
		printf("No data to be sent, sending just the address then...");
		ocd[o++]=0x1c00 | ((i2c_addr<<1) & 0xff); // start + address chip for write  note low 2 address bits -> byte address A8 A9
	} else {
		ocd[o++]=0x0c00 | ((i2c_addr<<1) & 0xff); // start + address chip for write  note low 2 address bits -> byte address A8 A9
		for (unsigned int i = 0; i < dataS.size()-1; i++) {
			ocd[o++]=0x0400 | (dataS.at(i) & 0xff);        // stop + byte address
		}
		ocd[o++]=0x1400 | ((dataS.at(dataS.size()-1)) & 0xff);
	}

	static uint16_t seqnum = 0x112;

	uint16_t opcode = 0x0080;
	uint16_t length;
	if(false) {
		const uint16_t bufLength = 100;
		uint16_t recv_data[bufLength];

		length = e->HsioSendReceiveOpcode(opcode, seqnum++, o, ocd, bufLength, recv_data);
		if(length < 32766) {
			if (length > bufLength) {
				printf("Length of return buffer was overlong(%d), cropping\n", length);
				length = bufLength;
			}
//  for(int w=0; w<length; w++) {
//    uint16_t recv = recv_data[w];
//    printf("H%d: %04x %c %c\n", w, recv, recv, (recv>>8));
//  }
		}
	} else {
    // Check with Matt whether no response is OK
		e->HsioSendOpcode(opcode, seqnum++, o, ocd);
		int count = 0;
		while(1) {
		//	uint16_t seqnum;
    	// length = bufLength;
			uint16_t *recv_data2;
			uint16_t inSeqnum;
			uint16_t opcode2 = opcode;
			e->HsioReceiveOpcode(opcode2, inSeqnum, length, recv_data2);
	  // length = bufLength;
			if(length != 0) {
			//	printf("op: %x seq: %d len: %d\n", opcode2, inSeqnum, length);
			//	for(unsigned int i=0; i<length; i++) {
			//		uint16_t data_out = recv_data2[i];
			//		printf(" * %2u: %04x\n", i, recv_data2[i]);
			//	}

				break;
			}
			e->Sleep(10);
			std::cout << "Slept a bit, now try again " << count << "\n";
			if(count++ > 10) {
				std::cout << "Slept too much, got bored\n";
				break;
			}
		}
	}
	return length;
}

uint16_t setDAC(unsigned int i2c_addr, unsigned int dacnum, unsigned int value) {
	std::vector<unsigned int> data;
	data.push_back(((1 << dacnum) & 0xf) | 0x30);
	data.push_back(((value >> 8) & 0xff));
	data.push_back((value & 0xff));
	nexys_writeI2C(i2c_addr, 15, data);
	return 0;
}

uint16_t setVDAC(unsigned int i2c_addr, unsigned int dacnum, float value, bool iAmSmart){
	if (i2c_addr == 14) {
		if (dacnum == 0) {
			if (iAmSmart) {
				if (value < 0.) value = 0.0;
				if (value > 10.0) value = 10.0;
				setDAC(i2c_addr, dacnum, ((unsigned int) (value/10.0*65535)));
			} else{
				printf("You should not mess with the Bias Voltage yet! (Not doing it)\n");
			}
		} else {
			if (value < 0.) value = 0.0;
			if (value > 2.048) value = 2.048;
			setDAC(i2c_addr, dacnum, ((unsigned int) (value/2.048/2.0*65535)));
		}
	} else {
		printf("Trying to set a voltage on a current DAC - not doing it!\n");
	}
	return 0;
}

uint16_t setIDAC(unsigned int i2c_addr, unsigned int dacnum, float value) {
	if (i2c_addr == 14) {
		printf("Trying to set a current on a voltage DAC - not doing it!\n");
		return 0;
	}
	if (value < 0.) value = 0.0;
	if (value > getMaxCurrent()) value = getMaxCurrent();
	setDAC(i2c_addr, dacnum, ((unsigned int) value/getMaxCurrent()*65535));
	return 0;
}

uint16_t setAllCurrentDACs(unsigned int value) {
	for (unsigned int i2c_addr=12; i2c_addr<14; i2c_addr++) {
		for (unsigned int dacnum=0;dacnum<4;dacnum++) {
			setIDAC(i2c_addr, dacnum, value);
		}
	}
	return 0;
}

uint16_t setAllCurrentDACs(float value) {
	for (unsigned int i2c_addr=12; i2c_addr<14; i2c_addr++) {
		for (unsigned int dacnum=0;dacnum<4;dacnum++) {
			setIDAC(i2c_addr, dacnum, value);
		}
	}
	return 0;
}

void setThreshold(unsigned int value) {
	setDAC(14, 1, value & 0xffff);
}

void setThreshold(float value) {
	setVDAC(14, 1, value);
}

void setBias(unsigned int value) {
	printf("Setting the Bias requires you to use a float value\n");
}

void setBias(float value) {
	setVDAC(14, 0, value, true);
}

void inject_number(unsigned int value, unsigned int nTimes, bool verbose) {
	std::vector<uint16_t> data;
	if (value > 31) {
		printf("Numbers bigger than 31 cannot be injected into the column, injecting the remainder of divide by 32: %d\n", (value & 0x1f));
	}
	value &= 0x1f;
	if (verbose) {
		if (nTimes > 1) {
			printf("Writing %d Values %d into test register\n", nTimes, value);
		} else {
			printf("Writing %d Value %d into test register\n", nTimes, value);
		}
	}
	while (nTimes > 32) {
		writeVal_TestReg(data, value, 32);
		sendData(data);
		data.clear();
		nTimes-=32;
	}
	writeVal_TestReg(data, value, nTimes);
	sendData(data);
	data.clear();
	return;
}

void inject_numbers(std::vector<unsigned int> values, bool verbose) {
	std::vector<uint16_t> data;
	for (unsigned int i=0; i<values.size();i++) {
		if (values[i] > 31) {
			printf("Numbers bigger than 31 cannot be injected into the column, injecting the remainder of divide by 32: %d\n", (values[i] & 0x1f));
			values[i] = values[i] & 0x1f;
		}
	}
	if (verbose) {
		if (values.size() > 1) {
			printf("Writing %lu Values into test register\n", values.size());
		} else {
			printf("Writing %lu Value into test register\n", values.size());
		}
	}
	unsigned int nVals=0;
	for (unsigned int i=0; i<values.size(); i++) {
		writeVal_TestReg(data, values[i], 1);
		nVals++;
		if (nVals == 32) {
			sendData(data);
			data.clear();
			nVals = 0;
		}
	}
	if (nVals > 0) {
		sendData(data);
		data.clear();
	}
	return;
}

void reset_testVector(bool verbose) {
	std::vector<uint16_t> data;
	inject_number(0, 64);
	if (verbose) {
		printf("Test register should now be empty, check data output\n");
	}
}

/// Digital Test:
// Set all currents and voltages to zero, except: iBiasL, iBiasCL, iBiasPLL
// Disbale FrameClockArray
// Set operate high

/// Setting up DECAL for digital only operation
// Assuming Startup has happened, i.e. PLL is operating

/// DAC definitions
/// Currents:
// ICalibDAC: 12, 0
// IShaperBias: 12, 1
// IPreAmpBias: 12, 2
// IBiasPLL: 12, 3
// IBiasCL: 13, 0
// IBiasL: 13, 1
// IPFBPreAmp: 13, 2
// ICompTail: 13, 3
void setupDigital() {
	setIDAC(12, 0, 0.); //< ICalibDac
	setIDAC(12, 1, 0.); //< IShaperBias
	setIDAC(12, 2, 0.); //< IPreAmpBias
	setIDAC(12, 3, 100.); //< IBiasPLL
	setIDAC(13, 0, 100.); //< IBiasCL
	setIDAC(13, 1, 100.); //< IBiasL
	setIDAC(13, 2, 0.); //< IPFBPreAmp
	setIDAC(13, 3, 0.); //< ICompTail
	for (unsigned int i=0; i<4; i++) { //< All voltages zero, but do we need to bring GRN up?
		setVDAC(14, i, 0., true);
	}
//	setVDAC(14, 3, 1.8); //< Could bring up GRN
/// All Currents/Voltages are now set are now set
	reset_testVector();
}

/// Analogue Tests:
// Bias the Chip:
// All currents 100uA
// vShaperCascN: 1.4V
// vThreshold 1.1V
// HV Bias: something (5V?)
/// Analogue Pixel:
// Setup as above, then observe Pre-Amp and Shaper out on SMA connectors
/// Analogue on array:
// As above
// Reset test register
// Strobe DACResetBar and compResetBarExt low and then keep them high
// Strobe CalibEn high for a few 10-100 clock ticks
// Strobe Operate Low afterwards to [Enabled/Disable] pixel operation???
void setupAnalog() {
	setAllCurrentDACs((float) 100.);
	//setIDAC(12, 0, 75.);
	//printf("Setting ICALIBDAC to 75uA\n");
/// Set Various Voltages:
	setVDAC(14, 0, 3.3, true); //< Bias Voltage
	setVDAC(14, 1, 1.1); //< Threshold
	setVDAC(14, 2, 1.4); //< VShaperCascN
	setVDAC(14, 3, 1.8); //< VGuardRing
/// All Currents/Voltages are now nominal
	reset_testVector();
}

void setIoDir(unsigned int value) {
		ioDirection = value;
}

void setTriggerEnable(bool TriggerOutEnable) {
	if (TriggerOutEnable) {
		
	} else {
		
	}
}

void startUP(bool modeSel, bool startPLL, bool resetTestRegister, bool resetPhase) {
	ioDirection = 0;
	std::cout << "Powering up motherboard, Mode is " << (modeSel? "high":"low") << std::endl;
	if (modeSel) {
		setConfigRegister(0x3 | 0xc000, true);
	} else {
		setConfigRegister(0x1 | 0xc000, true);
	}
	std::cout << "Setting all current DACs to 100uA" << std::endl;
	setAllCurrentDACs((float) 100.);
	std::cout << "Setting all low voltage DACs to roughly 1V" << std::endl;
	setVDAC(14, 1, 1.1);
	setDAC(14, 2, 32768);
	setDAC(14, 3, 32768);
	if (startPLL) {
		std::cout << "Bringing up the PLL" << std::endl;
		if (modeSel) {
			setConfigRegister(0x7 | 0xc000);
		} else {
			setConfigRegister(0x5 | 0xc000);
		}
	}
	if (resetTestRegister) {
		std::cout << "Resetting test register to all zeros" << std::endl;
		reset_testVector();
	}
	if (resetPhase) setManualPhase(true, 0);
	resetPeriphery();
	return;
}

void resetPixels(unsigned int delay, int nRepeat) {
	std::vector<uint16_t> data;
	data.clear();
	slowPush(data, ioDirection, nRepeat);
	slowPush(data, ioDirection | OPERATE, nRepeat);
	slowPush(data, ioDirection | OPERATE | CALIB_EN, nRepeat);
	slowPush(data, ioDirection | OPERATE, nRepeat);
	slowPush(data, ioDirection, delay);
	sendData(data);
	data.clear();
}

void operatePixels(bool connectPixel) {
	
	if (connectPixel) {
		if (!(ioDirection & OPERATE)) {
			ioDirection |= OPERATE;
			e->ConfigureVariable(10004, ioDirection);
		}
	} else {
		if (ioDirection & OPERATE) {
			ioDirection &= ~OPERATE;
			e->ConfigureVariable(10004, ioDirection);
		}
	}

}

void calibratePixels(bool connectDAC) {
	
	if (connectDAC) {
		if (!(ioDirection & CALIB_EN)) {
			ioDirection |= CALIB_EN;
			e->ConfigureVariable(10004, ioDirection);
		}
	} else {
		if (ioDirection & CALIB_EN) {
			ioDirection &= ~CALIB_EN;
			e->ConfigureVariable(10004, ioDirection);
		}
	}

}

void DECAL_Counters_Test(uint32_t delay, int dg_rates, bool linkid_mode) {

  //DG Rates:
  //  0-4  : 16MHz   8MHz   4MHz   2MHz   1MHz
  //  5-9  : 1600kHz 800kHz 400kHz 200kHz 100kHz 
  // 10-14 : 160kHz  80kHz  40kHz  20kHz  10kHz   
  //   15  : 16kHz
        
  int rates_khz[16] = {
    16000, 8000, 4000, 2000, 1000,
    1600,  800,  400,  200,  100,
    160,   80,   40,   20,   10,
    16};
  
  // Init
  e->ConfigureVariable(10019, 0x1000); // Disable Trigger-Data packets
  e->ConfigureVariable(10023, 0x0000);

  // Modes decode
  if (linkid_mode) {
    printf("LinkID mode: Setting counters = 0xdeca1 + linkid(8b) + sub-link(4b).\n");
    e->ConfigureVariable(10023, 0x0010);
  }
  else if (dg_rates > 0xffff) {
    printf("DG rates value too high, aborting!\n");
    return;  
  }
  else if (dg_rates >= 0) {
    printf("DataGen mode, using DG0, sub-link rates (0-3) = ");
    printf("%1x(%dkHz), %1x(%dkHz), %1x(%dkHz), %1x(%dkHz)\n",
	   dg_rates&0xf,       rates_khz[dg_rates&0xf],
	   (dg_rates>>4)&0xf,  rates_khz[(dg_rates>>4)&0xf],
	   (dg_rates>>8)&0xf,  rates_khz[(dg_rates>>8)&0xf],
	   (dg_rates>>12)&0xf, rates_khz[(dg_rates>>12)&0xf]);
    e->ConfigureVariable(10036, dg_rates);
    e->ConfigureVariable(10037, dg_rates);      //for the future
    streamConfigWrite(0x000f, 0, 0x0008, 16*2); //Data source = DG0
  }
  else
    streamConfigWrite(0x000f, 0, 0, 16*2);

  uint16_t sb[300]; // status block buffer
  //Readback status block2 - using extended status block feature
  //Matt_GetStatusBlock2(sb, 2, delay, 0); 

  static uint16_t seqnum = 0x7000;
  uint16_t ocdata[15];
  int i=0;
  ocdata[i++] = 2;
  ocdata[i++] = (delay >> 16) & 0xffff;
  ocdata[i++] = delay & 0xffff;
    
  uint16_t length = e->HsioSendReceiveOpcode(0x19, seqnum++, i, ocdata, 128+3, sb);
  if(length > 0xf000) printf("GetStatusBlock2: ERROR: No response\n");
  else {
    for (int n=0; n<(128+3); n++) sb[n]=((sb[n]&0xff)<< 8)|((sb[n]&0xff00)>>8);

    printf("Hdr: %04x %04x %04x\n", sb[0], sb[1], sb[2]);
    for (int n=3; n<(128+3); n++) {
      if ((n-3)%8 == 0) printf("%3d: ",(n-3));
      printf("%04x ", sb[n]);
      if ((n-3)%8 == 7) printf("\n");
    }
    printf("\n");
  }

  streamConfigWrite(0x000f, 0, 0, 16*2);
}

void loadDecalmacros(){
  gROOT->LoadMacro("test_DECAL.cxx");
  gROOT->LoadMacro("stdll/DecalConfig.cpp");
}
