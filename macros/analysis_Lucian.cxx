#include <vector>
#include <sys/stat.h>

// ROOT COLORS:
/*
kWhite  = 0,   kBlack  = 1,   kGray    = 920,  kRed    = 632,  kGreen  = 416,
kBlue   = 600, kYellow = 400, kMagenta = 616,  kCyan   = 432,  kOrange = 800,
kSpring = 820, kTeal   = 840, kAzure   =  860, kViolet = 880,  kPink   = 900

Am-241: kOrange +2: 802
Sr-90: kTeal + 3: 843
Cs-137 ?? kCyan-2: 430
kGray +1

x-ray targets:
Cu: kOrange +3: 803
Mo: kGray + 1: 921
Fe: kRed +2: 634
Pb: kBlue +4: 604
W: kGreen +4: 420 // kOrange: 800
Cd: kMagenta -1: 617
*/

// Fits gaussian to variation of DAC configs and writes fitresults to txt-file.
// The produced txt-file can then be used to tune each pixel by selecting the optimal dac configuration for a nominal threshold value.
// yerror is the error on the counts that is assumed via fitting and doesnt influence the position of the mean much.
// rootpath is folder and name where ...Row%02d.root files for each row from Retune() are saved.
void Fit_Rows_Cols_Tuning(string rootpath, unsigned int rowmin = 0, unsigned int rowmax = 64, unsigned int yerror = 200){
  TF1 *fgaus = new TF1("fGaus","gaus",1, 2);
  ofstream outfile ("fitresults_tuning.txt");
  outfile <<  "Row Column DAC Polarity Constant Constant_err Mean Mean_err Sigma Sigma_err" << endl;
  for(int row = rowmin; row < rowmax; row++){ // 64
    cout << "Row: "<< row << endl;
    char rfile[200] = "";
    sprintf(rfile,"%sRow%02d.root", rootpath.c_str(), row);
    TFile *f = new TFile(rfile);
    for(int pol = 0; pol < 2; pol++){
      for(int dac = 0; dac < 16; dac++){
        char histoname[200] = "";
        sprintf(histoname,"TuningScanRow%02dDAC%02dPol%01d",row,dac, pol);
        TH2D * histo = (TH2D*)f->Get(histoname);
        for(int col = 0; col < 64; col++){
          TH1D *projy = histo->ProjectionY("histo_py", col +1, col +1);// 1 to 64 to avoid overflow bins
          for(int b = 0 ; b < projy->GetNbinsX(); b++){ // set all errors to same value
                    projy->SetBinError(b, yerror); }
          projy->Fit("fGaus", "Q");
          outfile <<  row << " " << col + 1  << " " <<  dac << " "  << pol << " " <<  fgaus->GetParameter(0) << " " << fgaus->GetParError(0) << " "  << fgaus->GetParameter(1)<< " " << fgaus->GetParError(1)<< " " << fgaus->GetParameter(2)<< " " << fgaus->GetParError(2)<< endl;
        }
      }
    }
  }
  outfile.close();
}

// reads in txt file and selects the Dac configuration for each pixel with mean closest to nom_mean.
// txt input is produced via Fit_Rows_Cols_Tuning().
void Find_DAC_at_Thresh(string txtfile, float nom_mean, string outfname="optimDacs_tuning.txt", bool header = true , unsigned int rowmin = 0, unsigned int rowmax = 64) {
  ifstream infile(txtfile);
  string line;
  if(header)
    getline(infile, line); // skips header line

  ofstream outfile (outfname);
  outfile <<  "Row Column DAC Polarity Constant Constant_err Mean Mean_err Sigma Sigma_err" << endl;
  int index, ro , colu, dac, pol, dacpol;
  float constant, constant_err, mean, mean_err, sigma, sigma_err;
  for(int row = rowmin; row < rowmax; row++){
    // for each row store 32 difference between mean and nominal value per pixel and find pixelwise their minimum
    vector<vector<float>> Deltameans( 64 , vector<float> (32));
    vector<vector<float>> Constant( 64 , vector<float> (32));
    vector<vector<float>> Constant_err( 64 , vector<float> (32));
    vector<vector<float>> Mean( 64 , vector<float> (32));
    vector<vector<float>> Mean_err( 64 , vector<float> (32));
    vector<vector<float>> Sigma( 64 , vector<float> (32));
    vector<vector<float>> Sigma_err( 64 , vector<float> (32));
    vector<vector<unsigned int>> Pol( 64 , vector<unsigned int> (32));
    vector<vector<unsigned int>> Dac( 64 , vector<unsigned int> (32));


    // read in 64 *32 lines for each row
    for(int count = 0; count < 64*32; count++){
      getline(infile, line);
      std::istringstream iss(line);
      iss >> ro >> colu >> dac >> pol >> constant >> constant_err >> mean >> mean_err >> sigma >> sigma_err;
      dacpol  = int((2*pol-1)*(dac +0.5) +15.5);
      Constant[colu-1][dacpol] = constant;
      Constant_err[colu-1][dacpol] = constant_err;
      Mean[colu-1][dacpol] = mean;
      Mean_err[colu-1][dacpol] = mean_err;
      Sigma[colu-1][dacpol] = sigma;
      Sigma_err[colu-1][dacpol] = sigma_err;
      Pol[colu-1][dacpol] = pol;
      Dac[colu-1][dacpol] = dac;
      Deltameans[colu-1][dacpol] = abs(mean- nom_mean);
    }
    // choose best mean for each pixel
    for(int col = 0; col < 64; col++){
    index = min_element(Deltameans[col].begin(), Deltameans[col].end()) - Deltameans[col].begin();
    outfile << row << " " << col +1  << " " << Dac[col][index]   << " " <<  Pol[col][index] << " " <<  Constant[col][index] << " " <<  Constant_err[col][index] << " " << Mean[col][index] << " " << Mean_err[col][index] << " " << Sigma[col][index]<< " " << Sigma_err[col][index]<< endl;
  }

  }
    infile.close();
}


// Plot the sum of histograms in lin and log scale. In Stripmode or measurement of just one row R pass rowmin = R, rowmax = R+1.
// For multiple row measurements stored in the same root file, give the desired range of rows that should be added in the plot.
// Also cTScans where histogram name ends with number can be plotted via passing the number as rowmin and defining the histprefix before that.
void CombineallPixels(string rootfile, unsigned int rowmin = 0, unsigned int rowmax = 64, string nameofpdf = "", string bkgr_rootfile = "", string bkgr_histoname ="", string part_source="Americium-241", int signal_hist_color = 802, double bkgr_scale=1.0, float maxyrange=2000, float xmin=0.7, float xmax = 1.3,  string histprefix="counterThresholdScanRow", double minyerror = 1, float fontsize=0.04, float titlesize=0.045, float titleoffsety=1.0, float titleoffsetx=0.9){
  char rfile[200] = "";
  sprintf(rfile,"%s", rootfile.c_str());
  TFile *f = new TFile(rfile);
  TF1 *fgaus = new TF1("fGaus","gaus",1, 2);
  TH1D *histosum, *bkgr_histosum;
  TCanvas *c = new TCanvas("c1", "yprojections", 0, 0, 900, 600);
  unsigned int count = 0;

  for(int row = rowmin; row < rowmax; row++){ // 64
    cout << "Row: "<< row << endl;
    char histoname[200] = "";
    sprintf(histoname,"%s%02d",histprefix.c_str(),row);
    TH2D *histo = (TH2D*)f->Get(histoname);
    char py_name[20] = "";
    sprintf(py_name,"Row%02d",row);
    if(count == 0)
      {histosum = histo->ProjectionY(py_name, 1, 64);
      count++;}
    else
      {
      histosum->Add(histo->ProjectionY(py_name, 1, 64));
      }
  }
  if(!(bkgr_rootfile=="")){
    char bkgr_rfile[200] = "";
    sprintf(bkgr_rfile,"%s", bkgr_rootfile.c_str());
    TFile *bkgr_f = new TFile(bkgr_rfile);
    char bkgr_charhistoname[200] = "";
    sprintf(bkgr_charhistoname,"%s", bkgr_histoname.c_str());
    TH2D *bkgr_histo = (TH2D*)bkgr_f->Get(bkgr_charhistoname);
    bkgr_histosum = bkgr_histo->ProjectionY("background", 1, 64);
  }

  // Set bin error to square root of bin content
  for(int b = 0 ; b < histosum->GetNbinsX(); b++){ // set all errors to same value
    histosum->SetBinError(b, max(sqrt(histosum->GetBinContent(b)), minyerror));
      }
  //histosum->Fit("fGaus", "Q");
  histosum->SetXTitle("Threshold voltage (V)");
  histosum->SetYTitle("Counts");
  histosum->SetFillColor(kBlue);
  //if(rowmin > 63) histosum->SetTitle("stripmode y projection");
  //else
  histosum->SetTitle("");
  if((nameofpdf==""))
    nameofpdf = "p_y_allrows";
  histosum->SetName(nameofpdf.c_str());
  histosum->ResetStats();
  histosum->SetLineColorAlpha(signal_hist_color, 0.0001);
  histosum->SetFillColorAlpha(signal_hist_color, 0.5);
  histosum->Draw("hist");
  TLegend *legend = new TLegend(0.15,0.6,0.5,0.9);
  legend->AddEntry(histosum, (part_source + " data").c_str());
  if(!(bkgr_rootfile=="")) {
    bkgr_histosum->Scale(bkgr_scale);
    // Set bin error to square root of bin content
    for(int b = 0 ; b < bkgr_histosum->GetNbinsX(); b++){ // set all errors to same value
      bkgr_histosum->SetBinError(b, max(sqrt(bkgr_histosum->GetBinContent(b)), minyerror));
      }
    bkgr_histosum->SetLineColor(kBlue);
    //bkgr_histosum->SetFillColorAlpha(kBlue, 0.5);
    bkgr_histosum->Draw("hist same");
    legend->AddEntry(bkgr_histosum,"Background");
  }
  histosum->SetStats(0);
  histosum->GetXaxis()->SetRangeUser(xmin, xmax);
  legend->SetFillStyle(0);
  legend->SetBorderSize(0);
  legend->Draw();
  TLatex *text = new TLatex();
  text->DrawLatexNDC(0.46, 0.91,"DECAL");
  text->Draw();
  c->SetTopMargin(0.05);
  c->SetRightMargin(0.05);
  histosum->GetXaxis()->SetLabelSize(fontsize);
  histosum->GetYaxis()->SetLabelSize(fontsize);
  histosum->GetXaxis()->SetTitleOffset(titleoffsetx);
  histosum->GetYaxis()->SetTitleOffset(titleoffsety);
  histosum->GetXaxis()->SetTitleSize(titlesize);
  histosum->GetYaxis()->SetTitleSize(titlesize);
  c->SaveAs(("plots/"+nameofpdf + ".pdf").c_str());
  c->SaveAs(("plots/"+nameofpdf + ".root").c_str());
  c->SetLogy();
  c->SaveAs(("plots/"+nameofpdf + "_logy.pdf").c_str());
  c->SaveAs(("plots/"+nameofpdf + "_logy.root").c_str());
  c->SetLogy(0); // reset to lin scale
  histosum->SetMaximum(maxyrange); // sets ymax for another linear plot that shows signal counts next to noise peak
  c->SaveAs(("plots/"+nameofpdf + "_smallyrange.pdf").c_str());
  c->SaveAs(("plots/"+nameofpdf + "_smallyrange.root").c_str());
  c->Close();
}

// Plot the sum of histograms in lin and log scale. In Stripmode or measurement of just one row R pass rowmin = R, rowmax = R+1.
// For multiple row measurements stored in the same root file, give the desired range of rows that should be added in the plot.
void CombineallPixels_Offset_correction(string rootfile, unsigned int rowmin = 0, unsigned int rowmax = 64, string nameofpdf = "", float xmin = -0.05, float xmax=0.05, string histfilename = "", double minyerror = 1){
  char rfile[200] = "";
  sprintf(rfile,"%s", rootfile.c_str());
  TFile *f = new TFile(rfile);
  TF1 *fgaus = new TF1("fGaus","gaus",1, 2);
  TH1D *histosum;
  TCanvas *c = new TCanvas("c1", "yprojections", 0, 0, 900, 600);
  unsigned int count = 0;
  int nSteps;
  float V_range;
  float V_step;

  for(int row = rowmin; row < rowmax; row++){ // 64
    cout << "Row: "<< row << endl;
    char histoname[200] = "";
    sprintf(histoname,"counterThresholdScanRow%02d",row);
    TH2D *histo = (TH2D*)f->Get(histoname);
    for(int col = 0; col < 64; col++){
      char py_name[20] = "";
      sprintf(py_name,"Row%02dCol%02d",row,col);
      TH1D *projy = histo->ProjectionY(py_name, col+1, col+1);
      cout << col << ": " << projy->GetNbinsX() << " " << projy->GetStdDev()<< " " << projy->GetMean() << endl;

      if(count == 0){//create histogram at beginning with correct x-range
        nSteps = projy->GetNbinsX(); //i.e. 141
        V_range = projy->GetXaxis()->GetXmax() - projy->GetXaxis()->GetXmin(); // 0.141
        V_step = V_range/nSteps; // 0.001
        cout << nSteps << " " << V_range << " " << V_step << endl;
        //histosum = new TH1D("sumofall", "sumtitle", nSteps, V_offset-V_step/2.0, V_offset + V_range + V_step/2.0);
        histosum = new TH1D("sumofall", "sumtitle", nSteps*2-1, - V_range + V_step/2.0, V_range - V_step/2.0);
        count++;}
      int meanbin = projy->FindBin(projy->GetMean());
      // This shifts the counts relative to the mean per column
      for (Int_t i=1;i<=nSteps;i++)
        histosum->AddBinContent(nSteps+i-meanbin,projy->GetBinContent(i));
    }
  }
  // Set bin error to square root of bin content
  for(int b = 0 ; b < histosum->GetNbinsX(); b++){ // set all errors to same value
    histosum->SetBinError(b, max(sqrt(histosum->GetBinContent(b)), minyerror));
      }

  histosum->SetXTitle("Threshold voltage - Mean (V)");
  histosum->SetYTitle("Counts");
  histosum->GetXaxis()->SetRangeUser(xmin, xmax);
  histosum->SetFillColor(kBlue);
  if(rowmin > 63) histosum->SetTitle("stripmode y projection");
  else histosum->SetTitle("pixelwise y projection");
  if((nameofpdf==""))
    nameofpdf = "p_y_allrows";
  histosum->SetName(nameofpdf.c_str());
  histosum->ResetStats();
  histosum->Draw("hist");
  c->SaveAs((nameofpdf + ".pdf").c_str());
  c->SetLogy();
  c->SaveAs((nameofpdf + "_logy.pdf").c_str());
  if(!(histfilename =="")){
    char histfname[200] = "";
    sprintf(histfname,"%s",histfilename.c_str());
    TFile* histfile = new TFile(histfname, "RECREATE");
    histosum->Write();
    histfile->Close();
    delete histfile;
  }
}

// Write function that reads in two histograms for signal and background and produces difference plots
// parameter opt is the parameter entering TRatioPlot, take "diff" for difference or "diffsig" for difference divided by error per bin of first histogram.
void Signal_Backg_Plots(string rootfileS, string rootfileB, string strhistnameS, string strhistnameB, string opt, string nameofpdf=""){
  char rfileS[200] = "";
  sprintf(rfileS,"%s", rootfileS.c_str());
  TFile *fS = new TFile(rfileS);
  char rfileB[200] = "";
  sprintf(rfileB,"%s", rootfileB.c_str());
  TFile *fB = new TFile(rfileB);
  char histonameS[200] = "";
  sprintf(histonameS,"%s",strhistnameS.c_str());
  TH1D *histoS = (TH1D*)fS->Get(histonameS);
  char histonameB[200] = "";
  sprintf(histonameB,"%s",strhistnameB.c_str());
  TH1D *histoB = (TH1D*)fB->Get(histonameB);
  TCanvas *c = new TCanvas("c1", "Signal_Backgr", 0, 0, 1100, 600);
  histoS->SetLineColor(kOrange+2);
  histoS->SetFillColorAlpha(kOrange+2, 0.5);
  histoB->SetLineColor(kBlue);
  //histoB->SetFillColorAlpha(kBlue, 0.5);
  auto rp = new TRatioPlot(histoS, histoB, opt.c_str());
  rp->Draw();
  rp->GetLowerRefYaxis()->SetTitle("S-B / #sigma (S) ");
  TLegend *legend = new TLegend(0.15,0.75,0.35,0.9);
  legend->AddEntry(histoS, "330-kBq Americium Signal");
  legend->AddEntry(histoB,"Background");
  legend->Draw();
  if((nameofpdf==""))
    nameofpdf = "S_B_differenceplot";
  c->SaveAs((nameofpdf + ".pdf").c_str());
  rp->GetUpperPad()->SetLogy();
  rp->GetLowerPad()->SetLogy();
  //rp->GetLowerRefYaxis()->SetRangeUser(0.1, 5*rp->GetLowerPad()->GetUymax());
  rp->GetLowerRefGraph()->SetMinimum(0.1);
  c->SaveAs((nameofpdf + "_logy.pdf").c_str());
}

// customized function for ratio / difference plots that is more flexible than root TRatioPlot
void ratioplot(string rootfileS, string rootfileB, string strhistnameS, string strhistnameB) {
   // Define two gaussian histograms. Note the X and Y title are defined
   // at booking time using the convention "Hist_title ; X_title ; Y_title"
   /*
   TH1F *h1 = new TH1F("h1", "Two gaussian plots and their ratio;x title; h1 and h2 gaussian histograms", 100, -5, 5);
   TH1F *h2 = new TH1F("h2", "h2", 100, -5, 5);
   h1->FillRandom("gaus");
   h2->FillRandom("gaus");
   */

   char rfileS[200] = "";
   sprintf(rfileS,"%s", rootfileS.c_str());
   TFile *fS = new TFile(rfileS);
   char rfileB[200] = "";
   sprintf(rfileB,"%s", rootfileB.c_str());
   TFile *fB = new TFile(rfileB);
   char histonameS[200] = "";
   sprintf(histonameS,"%s",strhistnameS.c_str());
   TH1D *h1 = (TH1D*)fS->Get(histonameS);
   char histonameB[200] = "";
   sprintf(histonameB,"%s",strhistnameB.c_str());
   TH1D *h2 = (TH1D*)fB->Get(histonameB);

   // Define the Canvas
   TCanvas *c = new TCanvas("c", "canvas", 800, 800);

   // Upper plot will be in pad1
   TPad *pad1 = new TPad("pad1", "pad1", 0, 0.3, 1, 1.0);
   pad1->SetBottomMargin(0); // Upper and lower plot are joined
   pad1->SetGridx();         // Vertical grid
   pad1->Draw();             // Draw the upper pad: pad1
   pad1->cd();               // pad1 becomes the current pad
   h1->SetStats(0);          // No statistics on upper plot
   h1->Draw();               // Draw h1
   h2->Draw("same");         // Draw h2 on top of h1

   // Do not draw the Y axis label on the upper plot and redraw a small
   // axis instead, in order to avoid the first label (0) to be clipped.
   h1->GetYaxis()->SetLabelSize(0.);
   TGaxis *axis = new TGaxis( -5, 20, -5, 220, 20,220,510,"");
   axis->SetLabelFont(43); // Absolute font size in pixel (precision 3)
   axis->SetLabelSize(15);
   axis->Draw();

   // lower plot will be in pad
   c->cd();          // Go back to the main canvas before defining pad2
   TPad *pad2 = new TPad("pad2", "pad2", 0, 0.05, 1, 0.3);
   pad2->SetTopMargin(0);
   pad2->SetBottomMargin(0.2);
   pad2->SetGridx(); // vertical grid
   pad2->Draw();
   pad2->cd();       // pad2 becomes the current pad

   // Define the ratio plot
   TH1F *h3 = (TH1F*)h1->Clone("h3");
   h3->SetLineColor(kBlack);
   //h3->SetMinimum(0.8);  // Define Y ..
   //h3->SetMaximum(1.35); // .. range
   h3->Sumw2();
   h3->SetStats(0);      // No statistics on lower plot
   h3->Divide(h2);
   h3->SetMarkerStyle(21);
   h3->Draw("ep");       // Draw the ratio plot

   // h1 settings
   h1->SetLineColor(kBlue+1);
   h1->SetLineWidth(2);

   // Y axis h1 plot settings
   h1->GetYaxis()->SetTitleSize(20);
   h1->GetYaxis()->SetTitleFont(43);
   h1->GetYaxis()->SetTitleOffset(1.55);

   // h2 settings
   h2->SetLineColor(kRed);
   h2->SetLineWidth(2);

   // Ratio plot (h3) settings
   h3->SetTitle(""); // Remove the ratio title

   // Y axis ratio plot settings
   h3->GetYaxis()->SetTitle("ratio h1/h2 ");
   h3->GetYaxis()->SetNdivisions(505);
   h3->GetYaxis()->SetTitleSize(20);
   h3->GetYaxis()->SetTitleFont(43);
   h3->GetYaxis()->SetTitleOffset(1.55);
   h3->GetYaxis()->SetLabelFont(43); // Absolute font size in pixel (precision 3)
   h3->GetYaxis()->SetLabelSize(15);

   // X axis ratio plot settings
   h3->GetXaxis()->SetTitleSize(20);
   h3->GetXaxis()->SetTitleFont(43);
   h3->GetXaxis()->SetTitleOffset(4.);
   h3->GetXaxis()->SetLabelFont(43); // Absolute font size in pixel (precision 3)
   h3->GetXaxis()->SetLabelSize(15);
}

// three heatmap plots are plotted for rowwise threshold scans that are stored in different histograms for each row but in the same rootfile.
// The mean, std and the maximum bin entry is plotted oer the pixel geometrie
// histoprefix gives the name of the histograms
// writeout = true produces txt file with mean, std and maximum counts for each pixel.
void heatmap2d(string rootfile, string histoprefix = "counterThresholdScan", unsigned int rowmin=0, unsigned int rowmax = 64, bool writeout = false ){
  char rfile[200] = "";
  sprintf(rfile,"%s", rootfile.c_str());
  TFile *f = new TFile(rfile);
  TH2D** heatmaps = new TH2D*[3];
  ofstream outfile;
  if(writeout){
  outfile.open("heatmap_results.txt");
  outfile <<  "Row Column Mean Std Maximum" << endl;
  }
  char hist_names[3][100] = { "means", "std", "maximum" };
  const char *hist_titles[3] = { "Mean per pixel (V)", "Std per pixel (V)",  "Counts in maximum bin per pixel" };

  for (int i=0; i<3;i++) heatmaps[i] = new TH2D(hist_names[i], hist_titles[i], 64, -0.5, 63.5, rowmax -rowmin, rowmin -0.5, rowmax -0.5);

  for(int row = rowmin; row < rowmax; row++){ // 64
    char histoname[200] = "";
    sprintf(histoname,"%sRow%02d",histoprefix.c_str(), row);
    TH2D *histo = (TH2D*)f->Get(histoname);
    char py_name[20] = "";
    sprintf(py_name,"Row%02d",row);
    for(int col = 0; col < 64; col++){
      heatmaps[0]->Fill(col, row, histo->ProjectionY(py_name, col+1, col+1)->GetMean());
      heatmaps[1]->Fill(col, row, histo->ProjectionY(py_name, col+1, col+1)->GetStdDev());
      heatmaps[2]->Fill(col, row, histo->ProjectionY(py_name, col+1, col+1)->GetMaximum());
      if(writeout){
	outfile <<  row << " " << col << " " << histo->ProjectionY(py_name, col+1, col+1)->GetMean()  << " " << histo->ProjectionY(py_name, col+1, col+1)->GetStdDev() << " " << histo->ProjectionY(py_name, col+1, col+1)->GetMaximum() << endl;
      }
    }
  }
  outfile.close();
  for (int i=0; i<3;i++){
    TCanvas *c = new TCanvas("c","Graph2D",900, 600);
    heatmaps[i]->GetXaxis()->SetTitle("Column");
    heatmaps[i]->GetYaxis()->SetTitle("Row");
    c->SetRightMargin(0.4);
    heatmaps[i]->Draw("COLZ");
    sprintf(hist_names[i], "%s.pdf",hist_names[i]);
    c->SaveAs(hist_names[i]);
    c->Close();
  }
}

// So far a basic 2D Histogram plot of a threshold scan
void Plot_Hist(string rootfile, string histoname="counterThresholdScan", string plotname = "HistCOLZ.pdf", float ymin = -1., float ymax = -1., float fontsize=0.05, float titlesize=0.055, float titleoffsety=0.8, float titleoffsetx=0.8, float titleoffsetz=0.75){
  char rfile[200] = "";
  sprintf(rfile,"%s", rootfile.c_str());
  TFile *f = new TFile(rfile);
  char histname[200] = "";
  sprintf(histname,"%s",histoname.c_str());
  TCanvas *c = new TCanvas("c","2DHisto",1500, 800);
  TH2D *histo = (TH2D*)f->Get(histname);
  histo->Draw("COLZ");
  histo->SetStats(0); //used to be FALSE instead of 0. but loeading in root gives problem
  //c->SetTopMargin(0.35);
  cout << "STDX: " << histo->GetStdDev()<< " STDY: " << histo->ProjectionY("projy",1,64)->GetStdDev() << endl;

  c->SetRightMargin(0.15);
  c->SetTopMargin(0.35);
  char title[200] = "";
  //sprintf(title,"Counts from %s ; Column ;Threshold voltage (V)",histname);
  sprintf(title,"; Column ;Threshold voltage (V)");
  histo->SetTitle(title);
  histo->SetZTitle("Counts");
  char pdfname[200] = "";
  sprintf(pdfname,"%s",plotname.c_str());
  if(ymin >0 & ymax >0) histo->GetYaxis()->SetRangeUser(ymin, ymax);

  histo->GetXaxis()->SetLabelSize(fontsize);
  histo->GetYaxis()->SetLabelSize(fontsize);
  histo->GetZaxis()->SetLabelSize(fontsize);

  histo->GetXaxis()->SetTitleOffset(titleoffsetx);
  histo->GetYaxis()->SetTitleOffset(titleoffsety);
  histo->GetZaxis()->SetTitleOffset(titleoffsetz);

  histo->GetXaxis()->SetTitleSize(titlesize);
  histo->GetYaxis()->SetTitleSize(titlesize);
  histo->GetZaxis()->SetTitleSize(titlesize);

  c->SaveAs(pdfname);
  c->Close();
}

// So far a basic 2D Histogram plot of a threshold scan
void Plot_Hist_withTuningstats(string rootfile, string histoname="counterThresholdScan", string plotname = "HistCOLZ.pdf", float ymin = -1., float ymax = -1., bool rebin = false, float fontsize=0.04, float titlesize=0.045, float titleoffsety=0.9, float titleoffsetx=0.87, float titleoffsetz=0.85){
  char rfile[200] = "";
  sprintf(rfile,"%s", rootfile.c_str());
  TFile *f = new TFile(rfile);
  char histname[200] = "";
  sprintf(histname,"%s",histoname.c_str());
  TCanvas *c = new TCanvas("c","2DHisto",1500, 800);
  TH2D *histo;

  if (rebin){
    TH2D *histo_orig= (TH2D*)f->Get(histname);
    double ystep = histo_orig->GetYaxis()->GetBinCenter(1) - histo_orig->GetYaxis()->GetBinCenter(0);
    histo = new TH2D("rebinned", "", 64, -0.5, 63.5, (ymax -ymin)/ystep +1, ymin-ystep/2, ymax+ystep/2);
    for(unsigned int xind=0; xind < histo_orig->GetNbinsX()+1; xind++){
      for(unsigned int yind=0; yind < histo_orig->GetNbinsY()+1; yind++){
        histo->Fill(histo_orig->GetXaxis()->GetBinCenter(xind) , histo_orig->GetYaxis()->GetBinCenter(yind), histo_orig->GetBinContent(xind, yind));
      }
    }
  }
  else histo = (TH2D*)f->Get(histname);
  histo->Draw("COLZ");
  histo->SetStats(0);
  //c->SetTopMargin(0.35);
  cout << "STDX: " << histo->GetStdDev()<< " STDY: " << histo->ProjectionY("projy",1,64)->GetStdDev() << endl;

  c->SetRightMargin(0.15);
  c->SetTopMargin(0.05);
  char title[200] = "";
  //sprintf(title,"Counts from %s ; Column ;Threshold voltage (V)",histname);
  sprintf(title,"; Pixel ;Threshold voltage (V)");
  histo->SetTitle(title);
  histo->SetZTitle("Counts");
  char pdfname[200] = "";
  sprintf(pdfname,"%s",plotname.c_str());
  if(ymin >0 & ymax >0) histo->GetYaxis()->SetRangeUser(ymin, ymax);

  histo->GetXaxis()->SetLabelSize(fontsize);
  histo->GetYaxis()->SetLabelSize(fontsize);
  histo->GetZaxis()->SetLabelSize(fontsize);

  histo->GetXaxis()->SetTitleOffset(titleoffsetx);
  histo->GetYaxis()->SetTitleOffset(titleoffsety);
  histo->GetZaxis()->SetTitleOffset(titleoffsetz);

  histo->GetXaxis()->SetTitleSize(titlesize);
  histo->GetYaxis()->SetTitleSize(titlesize);
  histo->GetZaxis()->SetTitleSize(titlesize);


  // find lowest and highest maximum of pixel in row:
  double minmean=2;
  double maxmean=0;
  for(unsigned int k = 0; k<64; k++){
    double checkmean = histo->ProjectionY("projy",1+k,1+k)->GetMean();
    if(checkmean < minmean) minmean = checkmean;
    if(checkmean > maxmean) maxmean = checkmean;
  }

  TLegend *legend = new TLegend(0.08,0.74,0.849,0.94);
  char meanlegend[200] = "";
  sprintf(meanlegend, "Mean: %.4f V", histo->ProjectionY("projy",1,64)->GetMean());
  char minlegend[200] = "";
  sprintf(minlegend, "Minimum: %.3f V", minmean);
  char maxlegend[200] = "";
  sprintf(maxlegend, "Maximum: %.3f V", maxmean);
  char widthlegend[200] = "";
  sprintf(widthlegend, "Range: %.1f mV", 1000*(maxmean-minmean));
  char stdlegend[200] = "";
  sprintf(stdlegend, "Std Dev:  %.1f mV", 1000*histo->ProjectionY("projy",1,64)->GetStdDev());
  legend->AddEntry((TObject*)0, maxlegend,"");
  legend->AddEntry((TObject*)0, minlegend,"");
  legend->AddEntry((TObject*)0, widthlegend,"");
  legend->AddEntry((TObject*)0, meanlegend, "");
  legend->AddEntry((TObject*)0, stdlegend, "");
  legend->SetNColumns(3);
  legend->SetFillStyle(0);
  legend->SetBorderSize(0);
  legend->SetTextSize(0.048);
  legend->Draw();

  c->SaveAs(pdfname);
  c->Close();
}

// Plot the Threshold scan of a single pixel under the variation of the DAC and Polarity value.

void Plot_TH2D_Pixel_DACPolScan(string rootfile, int col = 0, double ymin=-1, double ymax = -1, string histoprefix="TuningScanRow00", string plotname = "plots/PixTuning_32configs.pdf", float fontsize=0.04, float titlesize=0.045, float titleoffsety=0.9, float titleoffsetx=0.87, float titleoffsetz=0.85){
  char rfile[200] = "";
  sprintf(rfile,"%s", rootfile.c_str());
  TFile *f = new TFile(rfile);
  TCanvas *c = new TCanvas("c","Dacpol_canvas",1500, 800);
  TH2D *Pixel_DACPol;
  for(int pol = 0; pol < 2; pol++){
    for(int dac = 0; dac < 16; dac++){
      char histname[200] = "";
      sprintf(histname,"%sDAC%02dPol%1d",histoprefix.c_str(), dac, pol);

      TH2D *histo = (TH2D*)f->Get(histname);

      if (pol==0 and dac==0) {
        double Vmin =histo->GetYaxis()->GetXmin();
        double Vmax =histo->GetYaxis()->GetXmax();
        double Vsteps = histo->GetYaxis()->GetNbins();

        cout << Vmin << " " << Vmax<< " "<< Vsteps <<  endl;
        Pixel_DACPol = new TH2D("DacPol_variation", "", 32, -0.5, 31.5, Vsteps, Vmin, Vmax);
        }

      //cout << dac << " " << " " << pol << " " << int((2*pol-1)*(dac +0.5) +15.5) << endl;
      for(int i = 1; i < histo->GetYaxis()->GetNbins(); i++){
      double content = histo->GetBinContent(col+1, i);
      Pixel_DACPol->Fill( double(int((2*pol-1)*(dac +0.5) +15.5)), histo->GetYaxis()->GetBinCenter(i), content);
      }
    }
  }
  Pixel_DACPol->Draw("colz");
  Pixel_DACPol->SetStats(0);
  c->SetRightMargin(0.15);
  c->SetTopMargin(0.05);
  char title[200] = "";
  sprintf(title,"; DAC configuration (decimal representation) ;Threshold voltage (V)");
  Pixel_DACPol->SetTitle(title);
  Pixel_DACPol->SetZTitle("Counts");

  Pixel_DACPol->GetXaxis()->SetLabelSize(fontsize);
  Pixel_DACPol->GetYaxis()->SetLabelSize(fontsize);
  Pixel_DACPol->GetZaxis()->SetLabelSize(fontsize);

  Pixel_DACPol->GetXaxis()->SetTitleOffset(titleoffsetx);
  Pixel_DACPol->GetYaxis()->SetTitleOffset(titleoffsety);
  Pixel_DACPol->GetZaxis()->SetTitleOffset(titleoffsetz);

  Pixel_DACPol->GetXaxis()->SetTitleSize(titlesize);
  Pixel_DACPol->GetYaxis()->SetTitleSize(titlesize);
  Pixel_DACPol->GetZaxis()->SetTitleSize(titlesize);
  if(ymin >0 & ymax >0) Pixel_DACPol->GetYaxis()->SetRangeUser(ymin, ymax);
  char pdfname[200] = "";
  sprintf(pdfname,"%s",plotname.c_str());
  c->SaveAs(pdfname);
  c->Close();
}



// Analyse the rootfiles from the strip mode Tune algorithm where row per row is added as enabled and rms per strip is minimzed. Fct TuneStripmode() gives rootfile that is read in here.
void PlotStd_enabledrows(string rootfile, unsigned int rowmin = 1, unsigned int rowmax = 64, string nameofpdf = ""){
  char rfile[200] = "";
  sprintf(rfile,"%s", rootfile.c_str());
  TFile *f = new TFile(rfile);
  TGraph *gr = new TGraph(rowmax - rowmin);
  TCanvas *c = new TCanvas("c2", "STD", 0, 0, 1000, 600);
  unsigned int count = 0;
  for(int row = rowmin; row < rowmax; row++){
    char histoname[200] = "";
    sprintf(histoname,"TuningScanRow%02dDAC00Pol0",row);
    TH2D *histo = (TH2D*)f->Get(histoname);
    float std = histo->ProjectionY("0", 1, 64)->GetStdDev();
    cout <<row << " " << std << endl;
    gr->SetPoint(count, row, std);
    count++;
  }

  if((nameofpdf==""))
    nameofpdf = "STD_over_Rows";
  gr->SetTitle("Strip Tuning;Rows;Std");
  gr->Draw();
  c->SaveAs((nameofpdf + ".pdf").c_str());
}

void CompareStd(string rootfilepix, string rootfilestrip, unsigned int row, float Vmin = 1.16, float Vmax = 1.19, unsigned int colmin = 0, unsigned int colmax = 63, string nameofpdf=""){
  char rfilepix[200] = "";
  sprintf(rfilepix,"%s", rootfilepix.c_str());
  char rfilestrip[200] = "";
  sprintf(rfilestrip,"%s", rootfilestrip.c_str());
  TFile *fpix = new TFile(rfilepix);
  TGraph *gr = new TGraph(colmax +1 - colmin);
  TCanvas *c = new TCanvas("c2", "STD", 0, 0, 1000, 600);
  unsigned int count = 0;
  char histonamepix[200] = "";
  sprintf(histonamepix,"counterThresholdScanRow%02d",row);
  char histonamestrip[200] = "";
  sprintf(histonamestrip,"counterThresholdScanRow%02d",row+100);
  TH2D *histpix = (TH2D*)fpix->Get(histonamepix);
  for(int col = colmin; col < colmax; col++){
    float std = histpix->ProjectionY("0", col+1, col+1)->GetStdDev();
    cout <<col << " " << std << endl;
    gr->SetPoint(count, col, std);
    count++;
  }
  if((nameofpdf==""))
    nameofpdf = "STD_over_Cols";
  gr->SetTitle("Std for pix and strip scan;Cols;Std");
  gr->Draw();
  c->SaveAs((nameofpdf + ".pdf").c_str());
}

// Analyse the influence of the DAC configs of the other strips.
// Thresholdscans of a row were performed for one strip being tuned to its optimal pixelwise config.
// All other strips active and configured to same DAC and polarity, which is then varied through all 32 combinations.
// Idea here is to see, how mean and std of pixtune strip changes depending on config of other strips.

// works for STD but implement for other two vars too.
void DACInfluenceonStrip(string rootfile, unsigned int strip, unsigned int row, string nameofpdf=""){
  char rfile[200] = "";
  sprintf(rfile,"%s", rootfile.c_str()); // here could loop over all strips that were pixtuned
  TFile *f = new TFile(rfile);
  TGraph *gr_Std = new TGraph(32);
  TGraph *gr_Mean = new TGraph(32);
  TGraph *gr_Maximum = new TGraph(32);
  char histoname[200] = "";
  unsigned int count = 0;
  for(int pol = 0; pol < 2; pol++){
    for(int dac = 0; dac < 16; dac++){
      sprintf(histoname,"TuningScanRow%02dDAC%02dPol%01d", row, dac, pol);
      TH2D *histo = (TH2D*)f->Get(histoname);
      TH1D *projy = histo->ProjectionY("histo_py", 63-strip +1, 63-strip +1);// only look at strip that was pixtuned
      float std = projy->GetStdDev();
      float mean = projy->GetMean();
      float maximum = projy->GetMaximum();
      int dacpol  = int((2*pol-1)*(dac +0.5) +15.5);
      gr_Std->SetPoint(count, dacpol, std);
      gr_Mean->SetPoint(count, dacpol, mean);
      gr_Maximum->SetPoint(count, dacpol, maximum);
      count++;
    }
  }
  if((nameofpdf==""))
    nameofpdf = "DACinfluenceon";
  nameofpdf = nameofpdf+ "Strip"+to_string(strip);

  TList *myGraphs = new TList();
  myGraphs->Add(gr_Std);
  myGraphs->Add(gr_Mean);
  myGraphs->Add(gr_Maximum);
  const char *vars[3] = { "std", "mean", "maximum" };
  const char *varsunit[3] = { "(V)", "(V)", "(counts)" };
  double miny[3] = {0., 1.174, -1111};
  double maxy[3] = {-1111, 1.18, -1111};
  TIter next(myGraphs);
  TGraph *gr;
  unsigned int i=0;
  while((gr = (TGraph*)next())){
    cout << vars[i]<< endl;
    char title[200] = "";
    sprintf(title,"Influence of other DAC configs on strip %02d;DAC Config;%s %s", strip, vars[i], varsunit[i]);
    TCanvas *c = new TCanvas("c", "DACinfluence", 0, 0, 1000, 600);
    gr->SetTitle(title);
    gr->SetMarkerStyle(5);
    gr->SetMinimum(miny[i]);
    gr->SetMaximum(maxy[i]);
    gr->Draw("AP");
    c->SaveAs((nameofpdf +"_"+vars[i]+ ".pdf").c_str());
    c->Close();
    i++;
    }
}

void create_four_variablePlots(TGraph *gr_Std, TGraph *gr_Mean, TGraph *gr_Maximum, TGraph *gr_Integral, string plottitle="plottitle", string xtitle="xtitle", string nameofpdf="varplots"){
  TList *myGraphs = new TList();
  myGraphs->Add(gr_Std);
  myGraphs->Add(gr_Mean);
  myGraphs->Add(gr_Maximum);
  myGraphs->Add(gr_Integral);
  const char *vars[4] = { "std", "mean", "maximum", "integral" };
  const char *varsunit[4] = { "(mV)", "(V)", "(counts)", "(counts)" };
  TIter next(myGraphs);
  TGraph *gr;
  unsigned int j=0;
  while((gr = (TGraph*)next())){
    cout << vars[j]<< endl;
    char title[200] = "";
    sprintf(title,"%s ;%s;%s %s", plottitle.c_str(), xtitle.c_str(), vars[j], varsunit[j]);
    TCanvas *c = new TCanvas("c", plottitle.c_str(), 0, 0, 1100, 600);
    gr->SetTitle(title);
    gr->SetMarkerStyle(5);
    //gr->SetMinimum(miny[i]);
    //gr->SetMaximum(maxy[i]);
    gr->Draw("AP");
    c->SaveAs((nameofpdf +"_"+vars[j]+ ".pdf").c_str());
    c->Close();
    j++;
    }
}

void create_combinedPlot(TGraph *gr_Std, TGraph *gr_Mean,TGraph *gr_Std_sum, TGraph *gr_Mean_sum, double Stdmin = 3, double Stdmax = 6.5, string plottitle="plottitle", string xtitle="xtitle", string nameofpdf="UnmaskingStrips_Mean_Std", float fontsize=0.04, float titlesize=0.045, float titleoffsety=1.1){
  TMultiGraph *mg = new TMultiGraph();
  TMultiGraph *mg2 = new TMultiGraph();
  TCanvas *c = new TCanvas("c", "", 0, 0, 1200, 600);
  //gr_Mean->SetTitle("First unmasked strip");
  int linewidth = 3;
  gr_Mean->SetLineColor(kBlue);
  gr_Mean->SetLineStyle(7);
  gr_Mean->SetLineWidth(linewidth);
  //gr_Mean_sum->SetTitle("Sum of unmasked strips");
  gr_Mean_sum->SetLineColor(kBlue);
  gr_Mean_sum->SetLineWidth(linewidth);

  mg->Add(gr_Mean);
  mg->Add(gr_Mean_sum);

  //const char *vars[2] = { "std", "mean"};
  //const char *varsunit[2] = { "(mV)", "(V)"};

  mg->SetTitle(";Number of unmasked strips;Mean of noise peak (V)");
  mg->Draw("AL");
  mg->GetXaxis()->SetLabelSize(fontsize);
  mg->GetXaxis()->SetTitleSize(titlesize);
  mg->GetYaxis()->SetLabelSize(fontsize);
  mg->GetYaxis()->SetTitleSize(titlesize);
  //mg->GetYaxis()->SetLineColor(kBlue);
  mg->GetYaxis()->SetLabelColor(kBlue);
  mg->GetYaxis()->SetTitleColor(kBlue);
  mg->GetYaxis()->SetTitleOffset(titleoffsety);

  // rescale std graph for second y axis
  cout << gPad->GetUxmin() << " " << gPad->GetUxmax() << " "<< gPad->GetUymin() << " "<< gPad->GetUymax() << endl;
  TGraph *gr_Std_rescaled = new TGraph(0);
  TGraph *gr_Std_sum_rescaled = new TGraph(0);
  for (unsigned int i= 0; i<gr_Std->GetN() ; i++){
    gr_Std_rescaled->AddPoint( gr_Std->GetPointX(i), (gr_Std->GetPointY(i)-Stdmin)*(gPad->GetUymax()-gPad->GetUymin())/(Stdmax-Stdmin)+gPad->GetUymin());
    gr_Std_sum_rescaled->AddPoint( gr_Std_sum->GetPointX(i), (gr_Std_sum->GetPointY(i)-Stdmin)*(gPad->GetUymax()-gPad->GetUymin())/(Stdmax-Stdmin)+gPad->GetUymin());
  }
  gr_Std_rescaled->SetLineColor(kOrange+2);
  gr_Std_rescaled->SetLineStyle(7);
  gr_Std_rescaled->SetLineWidth(linewidth);
  gr_Std_sum_rescaled->SetLineColor(kOrange+2);
  gr_Std_sum_rescaled->SetLineWidth(linewidth);


  TLegend *legend = new TLegend(0.3,0.12,0.9,0.5);
  legend->AddEntry(gr_Mean, "First unmasked strip");
  legend->AddEntry(gr_Mean_sum, "Sum of unmasked strips");
  legend->SetFillStyle(0);
  legend->SetBorderSize(0);
  legend->Draw();
  //c->Close();
  c->SetTopMargin(0.05);
  //c->SetRightMargin(0.05);
  mg->GetXaxis()->SetRangeUser(0, 65);
  gr_Std_rescaled->Draw("L same");
  gr_Std_sum_rescaled->Draw("L same");
  //gr_Std_sum->Draw("p same");
  TGaxis *axis = new TGaxis(65,gPad->GetUymin(),65, gPad->GetUymax(),Stdmin, Stdmax,510,"+L");
  axis->SetLineColor(kOrange+2);
  axis->SetLabelColor(kOrange+2);
  axis->SetTitleColor(kOrange+2);
  axis->SetLabelSize(fontsize);
  axis->SetTitleSize(titlesize);
  axis->SetTitle("Std of noise peak (mV)");
  axis->Draw();



  c->SaveAs((nameofpdf +".pdf").c_str());

  /*
  TIter next(myGraphs);
  TGraph *gr;
  unsigned int j=0;
  while((gr = (TGraph*)next())){
    cout << vars[j]<< endl;
    char title[200] = "";
    sprintf(title,"%s ;%s;%s %s", plottitle.c_str(), xtitle.c_str(), vars[j], varsunit[j]);
    TCanvas *c = new TCanvas("c", plottitle.c_str(), 0, 0, 1100, 600);
    gr->SetTitle(title);
    gr->SetMarkerStyle(5);
    //gr->SetMinimum(miny[i]);
    //gr->SetMaximum(maxy[i]);
    gr->Draw("AP");
    c->SaveAs((nameofpdf +"_"+vars[j]+ ".pdf").c_str());
    c->Close();
    j++;
    }
  */
}

// rootfile is "rootfolder/TuningScanCol" where the measurement of the function TuneEnabledChipCol() is stored.
// all other strips except of col are enabled. In clumn col only the pixel in row row is enabled and scanned through the 32 configs
void TunePixwisePerCol(string rootfile, unsigned int col, unsigned int row, string nameofpdf="TunePixwise", string add_totitle=""){
  char rfile[200] = "";
  sprintf(rfile,"%s%02d.root", rootfile.c_str(), col); // here could loop over all strips that were pixtuned
  TFile *f = new TFile(rfile);
  TGraph *gr_Std = new TGraph(32);
  TGraph *gr_Mean = new TGraph(32);
  TGraph *gr_Maximum = new TGraph(32);
  TGraph *gr_Integral = new TGraph(32);
  char histoname[200] = "";
  unsigned int count = 0;
  for(int pol = 0; pol < 2; pol++){
    for(int dac = 0; dac < 16; dac++){
      sprintf(histoname,"TuningScanCol%02dRow%02dDAC%02dPol%01d", col, row, dac, pol);
      TH2D *histo = (TH2D*)f->Get(histoname);
      TH1D *projy = histo->ProjectionY("histo_py", 63-col +1, 63-col +1);// only look at col with one enabled pixel
      float std = projy->GetStdDev();
      float mean = projy->GetMean();
      float maximum = projy->GetMaximum();
      float integral = projy->Integral();
      int dacpol  = int((2*pol-1)*(dac +0.5) +15.5);
      gr_Std->SetPoint(count, dacpol, std);
      gr_Mean->SetPoint(count, dacpol, mean);
      gr_Maximum->SetPoint(count, dacpol, maximum);
      gr_Integral->SetPoint(count, dacpol, integral);
      count++;
    }
  }
  nameofpdf = nameofpdf+ "_Col"+to_string(col) + "_Row"+to_string(row);
  char title[200] = "";
  sprintf(title,"Pixelwise Scan through DAC config %s,  Col %02d, Row %02d",add_totitle.c_str(),col, row);
  create_four_variablePlots(gr_Std, gr_Mean, gr_Maximum, gr_Integral, string(title), "DAC Config", nameofpdf);
}


// This function takes the rootfile from TuneEnabledChipCol() and creates for one pixel (defined by row and col) 32 plots.
// Each plot corresponts to one of 32 DAC configs that were scanned through.
// For comparison the sum of y-projections from all 64 strips are plotted on a second scale.
// Good example for twoscale plot is twoscales.C macro.
void PlotAllConfigsPixwisePerCol(string rootfile, unsigned int col, unsigned int row, string nameofpdf="", float xmin=1.1, float xmax=1.26, string histprefix = "TuningScanCol", string col_label = "one Pixel", int y_max =-1){
  char rfile[200] = "";
  sprintf(rfile,"%s%02d.root", rootfile.c_str(), col); // here could loop over all strips that were pixtuned
  TFile *f = new TFile(rfile);
  char histoname[200] = "";
  unsigned int count = 0;
  int ALLy_max = 0;
  for(int dacpol = 0; dacpol < 32; dacpol++){ // loop over all 32 configurations
    int pol = dacpol/16; // obtain polarity
    int dac = int((dacpol -15.5)*(dacpol/16 *2-1) -0.5); // obtain dac from dacpol
    sprintf(histoname,"%sRow%02dDAC%02dPol%01d",histprefix.c_str(), row, dac, pol);
    cout << histoname << endl;
    TH2D *histo = (TH2D*)f->Get(histoname);
    const char * hname = (string("dac config ")+to_string(dacpol)).c_str();
    TH1D *projy = histo->ProjectionY("col_py", 63-col +1, 63-col +1);// only look at col with one enabled pixel
    // Creating cutg to take out one column for second histo doesnt work yet. Hence, workaround by adding to histos.
    // Note that problems coul occur, if col is at edges so = 0 or 63.
    /*
    TCutG *cutg = new TCutG("mycut",5); // idea to cut out one column from projyALL
    cutg->SetVarX("y");
    cutg->SetVarY("x");
    cutg->SetPoint(0,63-col+1,xmin);
    cutg->SetPoint(1,63-col+1,xmax);
    cutg->SetPoint(2,63-col+2,xmax);
    cutg->SetPoint(3,63-col+2,xmin);
    cutg->SetPoint(4,63-col+1,xmin);
    */
    TH1D *projyALL;
    if(col == 0) projyALL = histo->ProjectionY("All_py", 1, 63-col);
    else if (col == 63) projyALL = histo->ProjectionY("All_py", 63-col+2, 64);
    else {
      projyALL = histo->ProjectionY("All_py", 1, 63-col);
      projyALL->Add(histo->ProjectionY("All_py2", 63-col+2, 64));
    }
    projy->SetName(hname);
    cout<< hname << endl;
    TCanvas *c = new TCanvas("c", "y-projection of pixel counts", 1000,600);//0, 0, 1000, 600);
    gStyle->SetOptStat(kFALSE);
    if (y_max > 0) projy->SetMaximum(y_max);
    projy->GetXaxis()->SetRangeUser(xmin, xmax);
    projy->SetFillColorAlpha(kBlue, 0.0);
    projy->SetLineColor(kBlue);
    char title[200] = "";
    sprintf(title,"DAC Config %02d (Pol %01d, DAC %02d) Variation for %s in Col %02d and Row %02d;Threshold voltage (V);Counts",dacpol, pol, dac, col_label.c_str(), col, row);
    projy->SetTitle( title);
    projy->Draw("HIST"); // if "AXIS", no histo is drawn but also title is not plotted
    c->Update();
    if (count==0) ALLy_max = projyALL->GetMaximum();
    Float_t rightmax = 1.1*ALLy_max;
    Float_t scale = gPad->GetUymax()/rightmax;

    projyALL->SetFillColor(kOrange +2);
    projyALL->SetLineColor(kRed);
    projyALL->Scale(scale);
    projyALL->Draw("HIST SAME");
    projy->SetFillColorAlpha(kBlue, 0.6);
    projy->Draw("HIST SAME"); // To show in foreground

    TLegend *legend = new TLegend(0.7,0.8,0.89,0.89);
    legend->AddEntry(projy, col_label.c_str());
    legend->AddEntry(projyALL,"All Columns");
    legend->Draw();
    //draw an axis on the right side
    TGaxis *axis = new TGaxis(gPad->GetUxmax(),gPad->GetUymin(), gPad->GetUxmax(), gPad->GetUymax(),0,rightmax,510,"+L");
    axis->SetLineColor(kOrange+2);
    axis->SetLabelColor(kOrange+2);
    axis->Draw();
    count++;
    nameofpdf = "PlotPixwise_Col"+to_string(col) + "_Row"+to_string(row);
    string stringpath = "pngs_Col" + to_string(col);
    mkdir(stringpath.c_str(),0777);
    c->SaveAs((stringpath + "/" + nameofpdf + "_Dacpol"+to_string(dacpol) + ".png").c_str()); // save single pngs
    c->SaveAs((stringpath + "/" + nameofpdf + ".gif+50").c_str()); // creates gif from all 32 plots where + number is delay in 10 ms
    c->Close();
  }
}


// read in the txt file from Fit_Rows_Cols_Tuning() and obtain the range to which all pixels can be tuned to.
// That is where does highest pixel for lowest dacpol = 0 (DAC=15, pol=0) lie and where lowest pixel for dacpol = 31 (DAC=15, pol=1)

void globalTunerange(string fitresultstxt, bool header = true){
  std::ifstream infile(fitresultstxt);
  std::string line;
  if(header)
    getline(infile, line); // skips header line
  int row , col, dac, pol, dacpol;
  float constant, constant_err, mean;
  std::vector<float> means_p0;
  std::vector<float> means_p1;

  while(getline(infile, line)) {
    std::istringstream iss(line);
    iss >> row >> col >> dac >> pol >> constant >> constant_err >> mean;
    dacpol  = int((2*pol-1)*(dac +0.5) +15.5);
    if (dac==15) {
      if (pol == 0) means_p0.push_back(mean);
      else means_p1.push_back(mean);
    }
  }
  //auto min_p0 = std::min_element(std::begin(means_p0), std::end(means_p0));
  auto max_p0 = std::max_element(std::begin(means_p0), std::end(means_p0));
  auto min_p1 = std::min_element(std::begin(means_p1), std::end(means_p1));
  //auto max_p1 = std::max_element(std::begin(means_p1), std::end(means_p1));

  cout << "Highest DAC= 15, Pol = 0 mean lies at " << *max_p0 << endl;
  cout << "Lowest DAC= 15, Pol = 1 mean lies at " << *min_p1 << endl;
}

// write a config file were all pixels are set to the same config.
void CreateConstantConfigtxt(unsigned int dac, unsigned int pol, string outfname="constantconfig.txt"){
  ofstream outfile (outfname);
  outfile <<  "Row Column DAC Polarity" << endl;
  for(int row = 0; row < 64; row++){
    for(int col = 0; col < 64; col++){
      outfile << row << " " << col +1  << " " << dac   << " " <<  pol << endl;
    }
  }
}
// write a config file were all pixels are set to the same config within one block.
// 32 (number of configs) should be divisable by nblocks
void CreateFixedDacBlockConfigtxt(unsigned int nblocks, string outfname="fixedDACblockconfig.txt"){
  ofstream outfile (outfname);
  outfile <<  "Row Column DAC Polarity" << endl;
  int dacpol =0;
  unsigned int dac, pol;
  unsigned int blocksize = 64/nblocks;
  unsigned int confstep = 32/nblocks;
  for(int row = 0; row < 64; row++){
    for(int col = 0; col < 64; col++){
      dacpol = col/blocksize *confstep; // assign dacpol and extract dac and pol from it. Note that integer deivison is wanted here
      dac = int((dacpol -15.5)*(dacpol/16 *2-1) -0.5);
      pol = dacpol/16;
      outfile << row << " " << col +1  << " " << dac   << " " <<  pol << endl;
    }
  }
}


// Tune blocks of columns with width nblocks together to nominal voltage values that increase stepwise inside of range.
// Note that 64 should be divisable by nblocks and all pixels should be possible to be tuned within the V_max and V_min range.
// V_max and V_min can be determined by globalTunerange()
// fitresultstxt is the txt file that is produced via Fit_Rows_Cols_Tuning()
void CreateBlockwiseTuneConfig(string fitresultstxt, float V_min, float V_max, unsigned int nblocks = 4, string outfname="BlockwiseConfig.txt", bool header = true){
  float V_step = (V_max - V_min)/ (nblocks-1);
  ofstream outfile (outfname); // open txt file for final stepwise config
  for(int n = 0; n < nblocks; n++){
    char filetxt[200] = "";
    sprintf(filetxt,"Deleteme_ConfigofBlock_%02d.txt", n);
    Find_DAC_at_Thresh(fitresultstxt, V_min + n*V_step, filetxt);
    std::ifstream infile(filetxt); // now read in created file with all pixels configured to same value
    std::string line;
    if(header)
      getline(infile, line); // skips header line
    int row , col, dac, pol, dacpol;
    float constant, constant_err, mean, mean_err, sigma, sigma_err;

    if(n==0) outfile <<  "Row Column DAC Polarity Constant Constant_err Mean Mean_err Sigma Sigma_err" << endl;
    while(getline(infile, line)) {
      std::istringstream iss(line);
      iss >> row >> col >> dac >> pol >> constant >> constant_err >> mean >> mean_err >> sigma >> sigma_err;
      if (col-1 >= 64/nblocks*n && col-1 < 64/nblocks*(n+1)){ // choose only columns inside block
        outfile << row << " " << col  << " " << dac   << " " <<  pol << " " <<  constant << " " <<  constant_err << " " << mean << " " << mean_err << " " << sigma << " "<< sigma_err <<endl;
      }
    }
  }
}

// Create config of strips tuned to alternating nominal values. Example: Every third strip is tuned to the same voltage.
// nblocks gives the amount of voltage levels that are tuned to.
// fitresultstxt is the txt file that is produced via Fit_Rows_Cols_Tuning()
void CreateAlternatingTuneConfig(string fitresultstxt, float V_min, float V_max, unsigned int nblocks = 2, string outfname="AlternatingConfig.txt", bool header = true){
  ofstream outfile (outfname); // open txt file for final stepwise config
  float V_step = (V_max - V_min)/ (nblocks-1);
  for(int n = 0; n < nblocks; n++){
    char filetxt[200] = "";
    sprintf(filetxt,"Deleteme_ConfigofAlternation_%02d.txt", n);
    Find_DAC_at_Thresh(fitresultstxt, V_min + n*V_step, filetxt);
    std::ifstream infile(filetxt); // now read in created file with all pixels configured to same value
    std::string line;
    if(header)
      getline(infile, line); // skips header line
    int row , col, dac, pol, dacpol;
    float constant, constant_err, mean, mean_err, sigma, sigma_err;
    if(n==0) outfile <<  "Row Column DAC Polarity Constant Constant_err Mean Mean_err Sigma Sigma_err" << endl;
    while(getline(infile, line)) {
      std::istringstream iss(line);
      iss >> row >> col >> dac >> pol >> constant >> constant_err >> mean >> mean_err >> sigma >> sigma_err;
      if (col%nblocks==n){ // choose columns alternatingly
        outfile << row << " " << col  << " " << dac   << " " <<  pol << " " <<  constant << " " <<  constant_err << " " << mean << " " << mean_err << " " << sigma << " "<< sigma_err <<endl;
      }
    }
  }
}

void UnmaskingStrips_VariablePlots(string rootfile, unsigned int stripstart = 1,unsigned int stripend = 64,  bool startat0 = true, string nameofpdf="UnmkaskingStrips"){
  char rfile[200] = "";
  sprintf(rfile,"%s", rootfile.c_str());
  TFile *f = new TFile(rfile);
  TGraph *gr_Std = new TGraph(64);
  TGraph *gr_Mean = new TGraph(64);
  TGraph *gr_Maximum = new TGraph(64);
  TGraph *gr_Integral = new TGraph(64);
  TGraph *gr_Std_sum = new TGraph(64);
  TGraph *gr_Mean_sum = new TGraph(64);
  TGraph *gr_Maximum_sum = new TGraph(64);
  TGraph *gr_Integral_sum = new TGraph(64);

  unsigned int strip;
  for(unsigned int i = 0; i<64; i++){
    if(startat0) strip = i;
    else strip = 63 - i;
    char histoname[200] = "";
    sprintf(histoname,"counterThresholdScanUnmaskedStrip%02d",strip);
    TH2D *histo = (TH2D*)f->Get(histoname);
    TH1D *projy = histo->ProjectionY("histo_py", stripstart, stripend);
    TH1D *projy_sum = histo->ProjectionY("histo_pysum", 1, 64);

    float std = projy->GetStdDev()*1000;
    float mean = projy->GetMean();
    float maximum = projy->GetMaximum();
    float integral = projy->Integral();
    gr_Std->SetPoint(i, i+1, std);
    gr_Mean->SetPoint(i, i+1, mean);
    gr_Maximum->SetPoint(i, i+1, maximum);
    gr_Integral->SetPoint(i, i+1, integral);
    float std_sum = projy_sum->GetStdDev()*1000;
    float mean_sum = projy_sum->GetMean();
    float maximum_sum = projy_sum->GetMaximum();
    float integral_sum = projy_sum->Integral();
    gr_Std_sum->SetPoint(i, i+1, std_sum);
    gr_Mean_sum->SetPoint(i, i+1, mean_sum);
    gr_Maximum_sum->SetPoint(i, i+1, maximum_sum);
    gr_Integral_sum->SetPoint(i, i+1, integral_sum);
  }
  create_four_variablePlots(gr_Std, gr_Mean, gr_Maximum, gr_Integral, "Unmasking strips", "Number of unmasked strips", nameofpdf);
  create_combinedPlot(gr_Std, gr_Mean,gr_Std_sum, gr_Mean_sum);
}



// this fct takes in a rootfile and creates plots that compare the std, mean.... of the 64 strips.
// These variables are plotted over the strip number in order to look for trends over the chip geometrie.
void Compare_AllStripsUnmasked_VariablePlots(string rootfile, string histname, unsigned int stripstart = 0,unsigned int stripend = 64, string nameofpdf="CompareStrips"){
  char rfile[200] = "";
  sprintf(rfile,"%s", rootfile.c_str());
  TFile *f = new TFile(rfile);
  char histoname[200] = "";
  sprintf(histoname,"%s",histname.c_str());
  TH2D * histo = (TH2D*)f->Get(histoname);
  TGraph *gr_Std = new TGraph(64);
  TGraph *gr_Mean = new TGraph(64);
  TGraph *gr_Maximum = new TGraph(64);
  TGraph *gr_Integral = new TGraph(64);

  for(unsigned int i = stripstart; i<stripend; i++){

    TH1D *projy = histo->ProjectionY("histo_py", i+1,i+1);
    float std = projy->GetStdDev();
    float mean = projy->GetMean();
    float maximum = projy->GetMaximum();
    float integral = projy->Integral();
    gr_Std->SetPoint(i, i, std);
    gr_Mean->SetPoint(i, i, mean);
    gr_Maximum->SetPoint(i, i, maximum);
    gr_Integral->SetPoint(i, i, integral);
  }
  create_four_variablePlots(gr_Std, gr_Mean, gr_Maximum, gr_Integral, "Compare Strips", "Strip number", nameofpdf);
}

// This function takes the rootfile from UnmaskStripafterStrip() and creates for one strip 64 plots.
// Each plot corresponts to a y-projection of a threshold scan where one strip after the other is unmasked.
// For comparison the sum of y-projections from the other 63 strips are plotted on a second scale.
// Good example for twoscale plot is twoscales.C macro.
void UnmaskingStrips_pngs(string rootfile, unsigned int strip,  int y_max =-1, string nameofpdf="", float xmin=1.1, float xmax=1.26, string histprefix = "TuningScanCol", string col_label = "one Strip"){
  char rfile[200] = "";
  sprintf(rfile,"%s", rootfile.c_str());
  TFile *f = new TFile(rfile);
  char histoname[200] = "";
  unsigned int count = 0;
  int ALLy_max = 0;
  for(int i = 0; i < 64; i++){
    char histoname[200] = "";
    sprintf(histoname,"counterThresholdScanUnmaskedStrip%02d",i);
      cout << histoname << endl;
      TH2D *histo = (TH2D*)f->Get(histoname);
      const char * hname = (string("Unmasked strip ")+to_string(i)).c_str();
      TH1D *projy = histo->ProjectionY("col_py", 63-strip +1, 63-strip +1);// only look at specific strip
      TH1D *projyALL = histo->ProjectionY("All_py", 1, 64);
      projy->SetName(hname);
      TCanvas *c = new TCanvas("c", "y-projection of pixel counts", 1000,600);//0, 0, 1000, 600);
      gStyle->SetOptStat(kFALSE);
      if (y_max > 0) projy->SetMaximum(y_max);
      projy->GetXaxis()->SetRangeUser(xmin, xmax);
      projy->SetFillColorAlpha(kBlue, 0.0);
      projy->SetLineColor(kBlue);
      char title[200] = "";
      sprintf(title,"Unmasking strip %02d ;Threshold voltage (V);Counts", i);
      projy->SetTitle( title);
      projy->Draw("HIST"); // if "AXIS", no histo is drawn but also title is not plotted
      c->Update();
      ALLy_max = projyALL->GetMaximum();
      Float_t rightmax = 1.1*ALLy_max;
      Float_t scale = gPad->GetUymax()/rightmax;

      projyALL->SetFillColor(kOrange +2);
      projyALL->SetLineColor(kRed);
      projyALL->Scale(scale);
      projyALL->Draw("HIST SAME");
      projy->SetFillColorAlpha(kBlue, 0.6);
      projy->ResetStats();
      projy->Draw("HIST SAME"); // To show in foreground
      char stdlegend[200] = "";
      sprintf(stdlegend,"Std of strip = %2.2f mV", projy->GetStdDev()*1000);
      char intlegend[200] = "";
      sprintf(intlegend,"Integral of strip = %.7e", projy->Integral());

      TLegend *legend = new TLegend(0.12,0.7,0.35,0.89);
      legend->AddEntry(projyALL,"All Strips");
      legend->AddEntry(projy, col_label.c_str());
      legend->AddEntry((TObject*)0, stdlegend,"");
      legend->AddEntry((TObject*)0, intlegend, "");
      legend->Draw();
      //draw an axis on the right side
      TGaxis *axis = new TGaxis(gPad->GetUxmax(),gPad->GetUymin(), gPad->GetUxmax(), gPad->GetUymax(),0,rightmax,510,"+L");
      axis->SetLineColor(kOrange+2);
      axis->SetLabelColor(kOrange+2);
      axis->Draw();
      count++;
      nameofpdf = "UnmaskingStrip";
      string stringpath = "pngs_Strip" + to_string(strip);
      mkdir(stringpath.c_str(),0777);
      c->SaveAs((stringpath + "/" + nameofpdf +to_string(i)+ ".png").c_str());
      c->SaveAs((stringpath + "/" + nameofpdf + ".gif+25").c_str());
      c->Close();
  }
}

// This function takes th2d histogram with filename and histname as input.
// It combines neighboring strips of width groupstrips together (for higher statistics) and applies a Fit to it.
// fitparameters should have Nparameters*3 entries (here 3*3 = 9).
void Fit1scurveToGroupedStrips(string rootfile, unsigned int groupstrips=4, string histname = "counterThresholdScanRow100", std::vector<Double_t> fitparameters={0., 0., 0., -1, 1, -1, 1, -1, 1}, string nameofpdf = "",string bkgr_rootfile = "", string bkgr_histoname ="", string part_source="Americium-241", int signal_hist_color = 802, double bkgr_scale=1.0, float Vstartfit=0.7, float Vendfit=1.17, float maxyrange = 2000, double minyerror = 1, bool plot_Fitresults = true, int maxdig = 3, float fontsize=0.04, float titlesize=0.045, float titleoffsety=1.1, float titleoffsetx=0.87){
  char rfile[200] = "";
  sprintf(rfile,"%s", rootfile.c_str());
  TFile *f = new TFile(rfile);
  TF1 *fitf = new TF1("fFit","[2]*erf((x-[0])/[1]) + [2]", Vstartfit, Vendfit); //0.7, 1.155 //0.76, 0.88)
  const Int_t n = 64/groupstrips;
  Double_t Mean[n], eMean[n], Sigma[n], eSigma[n], Height[n], eHeight[n], Group[n], eGroup[n];
  int Nparameters = 3; // 3 parameters for 1 s-curve

  for(unsigned int i =0; i < n; i++){
    for(unsigned int p =0; p < Nparameters; p++){
      fitf->SetParameter(p,fitparameters[p]); // set starting value
      fitf->SetParLimits(p, fitparameters[Nparameters+2*p], fitparameters[Nparameters+2*p+1]); // set limits
    }

    /*
    fitf->SetParameters(1.0, 0.1, 5000);
    fitf->SetParLimits(0,Vstartfit, Vendfit); // turn-on inside of interval
    fitf->SetParLimits(1, 0.001, 0.5); // sigma can be larger than interval width
    fitf->SetParLimits(2, 1000, 10000); // size of total signal shoulder can go up higher than maximal value in interval
    */

    /*
    fitf->SetParameters(0.8, 0.1,100);
    fitf->SetParLimits(0,0.77, 0.87); // turn-on inside of interval
    fitf->SetParLimits(1, 0.001, 0.1); // sigma can be larger than interval width
    fitf->SetParLimits(2, 1, 1000); // size of total signal shoulder can go up higher than maximal value in interval
    */
    TCanvas *c = new TCanvas("c1", "yprojections", 0, 0, 900, 600);
    char histoname[200] = "";
    sprintf(histoname,"%s",histname.c_str());
    TH2D *histo = (TH2D*)f->Get(histoname);
    char py_name[20] = "";
    sprintf(py_name,"Histgroup%02d_yprojection",i);
    TH1D *histogroup = histo->ProjectionY(py_name, i*groupstrips+1, (i+1)*groupstrips);
    // Set bin error to square root of bin content
    for(int b = 0 ; b < histogroup->GetNbinsX(); b++){ // set all errors to same value
      histogroup->SetBinError(b, max(sqrt(histogroup->GetBinContent(b)), minyerror));
    }
    histogroup->Fit("fFit", "R"); //,"Q") // option "R" takes TF1 x-interval as fit range

    TH1D *bkgr_histosum;
    // Fill backgr histo if defined:
    if(!(bkgr_rootfile=="")){
      char bkgr_rfile[200] = "";
      sprintf(bkgr_rfile,"%s", bkgr_rootfile.c_str());
      TFile *bkgr_f = new TFile(bkgr_rfile);
      char bkgr_charhistoname[200] = "";
      sprintf(bkgr_charhistoname,"%s", bkgr_histoname.c_str());
      TH2D *bkgr_histo = (TH2D*)bkgr_f->Get(bkgr_charhistoname);
      bkgr_histosum = bkgr_histo->ProjectionY("background", 1, 64);
    }

    if(plot_Fitresults){
      Mean[i]=fitf->GetParameter(0);
      eMean[i]=fitf->GetParError(0);
      Sigma[i]=fitf->GetParameter(1);
      eSigma[i]=fitf->GetParError(1);
      Height[i]=fitf->GetParameter(2);
      eHeight[i]=fitf->GetParError(2);
      Group[i] = i+1; // to start numbering with 1
      eGroup[i] = 0.;
    }

    histogroup->SetXTitle("Threshold voltage (V)");
    histogroup->SetYTitle("Counts");
    histogroup->SetFillColor(kBlue);
    histogroup->SetTitle(""); // plot without title
    if((nameofpdf==""))
      nameofpdf = "p_y_1scurvefit_allrows";
    histogroup->SetName(nameofpdf.c_str());
    histogroup->SetStats(0);
    histogroup->SetLineColorAlpha(signal_hist_color, 0.0001);
    histogroup->SetFillColorAlpha(signal_hist_color, 0.5);

    histogroup->Draw("hist");
    fitf->Draw("same");
    char model[200] = "h #times erf((x #minus #mu) /#sigma) + h ";
    char meanlegend[200] = "";
    sprintf(meanlegend,"#mu = %.4f #pm %.4f V", fitf->GetParameter(0),fitf->GetParError(0));
    char sigmalegend[200] = "";
    sprintf(sigmalegend,"#sigma = %.1f #pm %.1f mV", fitf->GetParameter(1)*1000,fitf->GetParError(1)*1000);
    char heightlegend[200] = "";
    sprintf(heightlegend,"h = %.1f #pm %.1f ", fitf->GetParameter(2),fitf->GetParError(2));

    //TLegend *legend = new TLegend(0.12,0.65,0.4,0.89);
    TLegend *legend = new TLegend(0.12,0.49,0.5,0.94);
    //TLegend *legend = new TLegend(0.12,0.5,0.45,0.89);
    legend->AddEntry(histogroup, (part_source + " data").c_str());
    if(!(bkgr_rootfile=="")) {
      bkgr_histosum->Scale(bkgr_scale);
      // Set bin error to square root of bin content
      for(int b = 0 ; b < bkgr_histosum->GetNbinsX(); b++){ // set all errors to sqrt of counts and at least minyerror
        bkgr_histosum->SetBinError(b, max(sqrt(bkgr_histosum->GetBinContent(b)), minyerror));
        }
      bkgr_histosum->SetLineColor(kBlue);
      //bkgr_histosum->SetFillColorAlpha(kBlue, 0.5);
      bkgr_histosum->Draw("hist same");
      legend->AddEntry(bkgr_histosum,"Background");
      }
    legend->AddEntry(fitf, model);
    legend->AddEntry((TObject*)0, heightlegend, "");
    legend->AddEntry((TObject*)0, meanlegend, "");
    legend->AddEntry((TObject*)0, sigmalegend, "");
    //legend->SetTextSize(0.04);
    legend->SetBorderSize(0);
    legend->SetFillStyle(0);
    legend->Draw();

    TLatex *text = new TLatex();
    text->DrawLatexNDC(0.46, 0.91,"DECAL");
    text->Draw();

    histogroup->GetYaxis()->SetMaxDigits(maxdig);
    histogroup->GetXaxis()->SetLabelSize(fontsize);
    histogroup->GetYaxis()->SetLabelSize(fontsize);

    histogroup->GetXaxis()->SetTitleOffset(titleoffsetx);
    histogroup->GetYaxis()->SetTitleOffset(titleoffsety);

    histogroup->GetXaxis()->SetTitleSize(titlesize);
    histogroup->GetYaxis()->SetTitleSize(titlesize);

    c->SetRightMargin(0.05);
    c->SetTopMargin(0.05);

    c->SaveAs((nameofpdf +to_string(groupstrips)+ "perGroup" + to_string(i+1) +"of"+to_string(n)+ ".pdf").c_str());
    c->SaveAs((nameofpdf +to_string(groupstrips)+ "perGroup" + to_string(i+1) +"of"+to_string(n)+ ".root").c_str());
    c->SetLogy();
    c->SaveAs((nameofpdf +to_string(groupstrips)+ "perGroup" + to_string(i+1) +"of"+to_string(n)+ "_logy.pdf").c_str());
    c->SaveAs((nameofpdf +to_string(groupstrips)+ "perGroup" + to_string(i+1) +"of"+to_string(n)+ "_logy.root").c_str());
    c->SetLogy(0); // reset to lin scale
    histogroup->SetMaximum(maxyrange); // sets ymax for another linear plot that shows signal counts next to noise peak
    c->SaveAs((nameofpdf +to_string(groupstrips)+ "perGroup" + to_string(i+1) +"of"+to_string(n)+ "_smallyrange.pdf").c_str());
    c->SaveAs((nameofpdf +to_string(groupstrips)+ "perGroup" + to_string(i+1) +"of"+to_string(n)+ "_smallyrange.root").c_str());
    c->Close();
  }
  if(plot_Fitresults){
    auto gr_Sigma = new TGraphErrors(n,Group,Sigma,eGroup,eSigma);
    auto gr_Mean = new TGraphErrors(n,Group,Mean,eGroup,eMean);
    auto gr_Height = new TGraphErrors(n,Group,Height,eGroup,eHeight);

    TList *myGraphs = new TList();
    myGraphs->Add(gr_Sigma);
    myGraphs->Add(gr_Mean);
    myGraphs->Add(gr_Height);
    const char *vars[3] = { "sigma", "mean", "height" };
    const char *varsunit[3] = { "(V)", "(V)", "(counts)" };
    TIter next(myGraphs);
    TGraph *gr;
    unsigned int j=0;
    while((gr = (TGraph*)next())){
      cout << vars[j]<< endl;
      char title[200] = "";
      sprintf(title,"%s of the signal turn-on curves for the %02d groups ;Strip group;%s %s", vars[j], groupstrips, vars[j], varsunit[j]);
      TCanvas *c = new TCanvas("c", "Grouped_turnon", 0, 0, 1000, 600);
      gr->SetTitle(title);
      gr->SetMarkerStyle(21);
      gr->SetMarkerColor(4);
      gr->Draw("AP");
      c->SaveAs((nameofpdf +to_string(groupstrips) +"perGroup_"+vars[j]+ ".pdf").c_str());
      c->Close();
      j++;
    }
  }
}


// This function takes th2d histogram with filename and histname as input.
// It combines neighboring strips of width groupstrips together (for higher statistics) and applies a Fit to it.
// fitparameters should have Nparameters*3 entries
vector<double> Fit2scurvesToGroupedStrips(string rootfile, unsigned int groupstrips=4, string histname = "counterThresholdScanRow100", std::vector<Double_t> fitparameters={0., -1., 1., 0., -1., 1.}, string nameofpdf = "", string bkgr_rootfile = "", string bkgr_histoname ="", string part_source="Americium-241", int signal_hist_color = 802, double bkgr_scale=1.0, float Vstartfit=0.7, float Vendfit=1.17, float maxyrange = 2000, double minyerror = 1, bool plot_Fitresults = true, int maxdig = 3, vector<Double_t> chippar={0.,0.,0.,0.,0.,0.}, float fontsize=0.04, float titlesize=0.045, float titleoffsety=1.1, float titleoffsetx=0.87){

  char rfile[200] = "";
  sprintf(rfile,"%s", rootfile.c_str());
  TFile *f = new TFile(rfile);
  // TF1 *fitf = new TF1("fFit","[2]*erf((x-[0])/[1]) + [2]",0.7, 1.13); // s-curve turn on
  //TF1 *fit_s_gaus = new TF1("fFit","[2]*0.5*(erf((x-[0])/[1])-erf((x-[3])/[1]))+exp(-1*((x-[3])/[4])^2)*[5]",0.7, 1.3); // s-curve and gauss
  TF1 *fit_2s = new TF1("fFit","[2]*erf((x-[0])/[1])+ [2]+[5] + [5]*erf((x-[3])/[4])", Vstartfit, Vendfit);// 0.7, 1.155); // two s-curves

  const Int_t n = 64/groupstrips;
  Double_t Mean[n], eMean[n], Sigma[n], eSigma[n], Height[n], eHeight[n], Group[n], eGroup[n];
  Double_t Mean2[n], eMean2[n], Sigma2[n], eSigma2[n], Height2[n], eHeight2[n];
  int Nparameters = 6; // 6 parameters for 2 s-curves
  for(unsigned int i =0; i < n; i++){
    for(unsigned int p =0; p < Nparameters; p++){ // fitparameters should have Nparameters*3 entries
      fit_2s->SetParameter(p,fitparameters[p]);
      fit_2s->SetParLimits(p, fitparameters[Nparameters+2*p], fitparameters[Nparameters+2*p+1]);
    }

    TCanvas *c = new TCanvas("c1", "yprojections", 0, 0, 900, 600);
    char histoname[200] = "";
    sprintf(histoname,"%s",histname.c_str());
    TH2D *histo = (TH2D*)f->Get(histoname);
    char py_name[20] = "";
    sprintf(py_name,"Histgroup%02d_yprojection",i);
    TH1D *histogroup = histo->ProjectionY(py_name, i*groupstrips+1, (i+1)*groupstrips);
    // Set bin error to square root of bin content
    for(int b = 0 ; b < histogroup->GetNbinsX(); b++){ // set all errors to sqrt of counts and at least minyerror
      histogroup->SetBinError(b, max(sqrt(histogroup->GetBinContent(b)), minyerror));
    }
    histogroup->Fit("fFit","R"); //,"Q")
    TH1D *bkgr_histosum;
    // Fill backgr histo if defined:
    if(!(bkgr_rootfile=="")){
      char bkgr_rfile[200] = "";
      sprintf(bkgr_rfile,"%s", bkgr_rootfile.c_str());
      TFile *bkgr_f = new TFile(bkgr_rfile);
      char bkgr_charhistoname[200] = "";
      sprintf(bkgr_charhistoname,"%s", bkgr_histoname.c_str());
      TH2D *bkgr_histo = (TH2D*)bkgr_f->Get(bkgr_charhistoname);
      bkgr_histosum = bkgr_histo->ProjectionY("background", 1, 64);
    }


    Mean[i]=fit_2s->GetParameter(0);
    eMean[i]=fit_2s->GetParError(0);
    Sigma[i]=fit_2s->GetParameter(1)*1000;
    eSigma[i]=fit_2s->GetParError(1)*1000;
    Height[i]=fit_2s->GetParameter(2);
    eHeight[i]=fit_2s->GetParError(2);
    Mean2[i]=fit_2s->GetParameter(3);
    eMean2[i]=fit_2s->GetParError(3);
    Sigma2[i]=fit_2s->GetParameter(4)*1000;
    eSigma2[i]=fit_2s->GetParError(4)*1000;
    Height2[i]=fit_2s->GetParameter(5);
    eHeight2[i]=fit_2s->GetParError(5);

    Group[i] = i+1; // to start numbering with 1
    eGroup[i] = 0.;


    histogroup->SetXTitle("Threshold voltage (V)");
    histogroup->SetYTitle("Counts");
    histogroup->SetFillColor(kBlue);
    //histogroup->SetTitle(py_name);
    histogroup->SetTitle(""); // plot without title
    if((nameofpdf==""))
      nameofpdf = "p_y_2scurvefit_allrow";
    histogroup->SetName(nameofpdf.c_str());
    histogroup->SetStats(0);
    histogroup->SetLineColorAlpha(signal_hist_color, 0.0001);
    histogroup->SetFillColorAlpha(signal_hist_color, 0.5);
    histogroup->Draw("hist");
    fit_2s->Draw("same");
    char model1[200] = "f(x) =   h_{1} #times erf((x #minus #mu_{1})/#sigma_{1}) + h_{1}" ;
    char model2[200] = "       + h_{2} #times erf((x #minus #mu_{2})/#sigma_{2}) + h_{2}";

    char meanlegend1[200] = "";
    sprintf(meanlegend1,"#mu_{1} = %.4f #pm %.4f V", fit_2s->GetParameter(0),fit_2s->GetParError(0));
    char sigmalegend1[200] = "";
    //sprintf(sigmalegend1,"#sigma_{1} = %.1f #pm %.1f mV", fit_2s->GetParameter(1)*1000,fit_2s->GetParError(1)*1000);
    sprintf(sigmalegend1,"#sigma_{1} = %.1f #pm %.1f mV", fit_2s->GetParameter(1)*1000,fit_2s->GetParError(1)*1000);
    char heightlegend1[200] = "";
    sprintf(heightlegend1,"h_{1}  = %.1f #pm %.1f", fit_2s->GetParameter(2),fit_2s->GetParError(2));
    char meanlegend2[200] = "";
    sprintf(meanlegend2,"#mu_{2} = %.4f #pm %.4f V", fit_2s->GetParameter(3),fit_2s->GetParError(3));
    char sigmalegend2[200] = "";
    sprintf(sigmalegend2,"#sigma_{2} = %.1f #pm %.1f mV", fit_2s->GetParameter(4)*1000,fit_2s->GetParError(4)*1000);
    //sprintf(sigmalegend2,"#sigma_{2} = %.4f #pm %.4f V", fit_2s->GetParameter(4),fit_2s->GetParError(4));
    char heightlegend2[200] = "";
    sprintf(heightlegend2,"h_{2}  = %.1f #pm %.1f", fit_2s->GetParameter(5),fit_2s->GetParError(5));

    TLegend *legend = new TLegend(0.12,0.4,0.5,0.94); //(0.12,0.49,0.5,0.94);
    legend->AddEntry(histogroup,  (part_source + " data").c_str());
    if(!(bkgr_rootfile=="")) {
      bkgr_histosum->Scale(bkgr_scale);
      // Set bin error to square root of bin content
      for(int b = 0 ; b < bkgr_histosum->GetNbinsX(); b++){
        bkgr_histosum->SetBinError(b, max(sqrt(bkgr_histosum->GetBinContent(b)), minyerror));
        }
      bkgr_histosum->SetLineColor(kBlue);
      //bkgr_histosum->SetFillColorAlpha(kBlue, 0.5);
      bkgr_histosum->Draw("hist same");
      legend->AddEntry(bkgr_histosum,"Background");
      }
    legend->AddEntry(fit_2s, model1);
    legend->AddEntry((TObject*)0, model2,"");
    legend->AddEntry((TObject*)0, heightlegend1, "");
    legend->AddEntry((TObject*)0, meanlegend1, "");
    legend->AddEntry((TObject*)0, sigmalegend1, "");
    legend->AddEntry((TObject*)0, heightlegend2, "");
    legend->AddEntry((TObject*)0, meanlegend2, "");
    legend->AddEntry((TObject*)0, sigmalegend2, "");
    //legend->SetTextSize(0.03);
    legend->SetBorderSize(0);
    legend->SetFillStyle(0);
    legend->Draw();
    TLatex *text = new TLatex();
    text->DrawLatexNDC(0.46, 0.91,"DECAL");
    text->Draw();

    histogroup->GetXaxis()->SetLabelSize(fontsize);
    histogroup->GetYaxis()->SetLabelSize(fontsize);

    histogroup->GetXaxis()->SetTitleOffset(titleoffsetx);
    histogroup->GetYaxis()->SetTitleOffset(titleoffsety);
    histogroup->GetYaxis()->SetMaxDigits(maxdig);

    histogroup->GetXaxis()->SetTitleSize(titlesize);
    histogroup->GetYaxis()->SetTitleSize(titlesize);

    c->SetRightMargin(0.05);
    c->SetTopMargin(0.05);

    c->SaveAs((nameofpdf +to_string(groupstrips)+ "perGroup" + to_string(i+1) +"of"+to_string(n)+ ".pdf").c_str());
    c->SaveAs((nameofpdf +to_string(groupstrips)+ "perGroup" + to_string(i+1) +"of"+to_string(n)+ ".root").c_str());
    c->SetLogy();
    c->SaveAs((nameofpdf +to_string(groupstrips)+ "perGroup" + to_string(i+1) +"of"+to_string(n)+ "_logy.pdf").c_str());
    c->SaveAs((nameofpdf +to_string(groupstrips)+ "perGroup" + to_string(i+1) +"of"+to_string(n)+ "_logy.root").c_str());
    c->SetLogy(0); // reset to lin scale
    histogroup->SetMaximum(maxyrange); // sets ymax for another linear plot that shows signal counts next to noise peak
    c->SaveAs((nameofpdf +to_string(groupstrips)+ "perGroup" + to_string(i+1) +"of"+to_string(n)+ "smallyrange.pdf").c_str());
    c->SaveAs((nameofpdf +to_string(groupstrips)+ "perGroup" + to_string(i+1) +"of"+to_string(n)+ "smallyrange.root").c_str());
    c->Close();
  }
  if(plot_Fitresults){
    auto gr_Sigma = new TGraphErrors(n,Group,Sigma,eGroup,eSigma);
    auto gr_Mean = new TGraphErrors(n,Group,Mean,eGroup,eMean);
    auto gr_Height = new TGraphErrors(n,Group,Height,eGroup,eHeight);
    auto gr_Sigma2 = new TGraphErrors(n,Group,Sigma2,eGroup,eSigma2);
    auto gr_Mean2 = new TGraphErrors(n,Group,Mean2,eGroup,eMean2);
    auto gr_Height2 = new TGraphErrors(n,Group,Height2,eGroup,eHeight2);

    TList *myGraphs = new TList();
    myGraphs->Add(gr_Height);
    myGraphs->Add(gr_Mean);
    myGraphs->Add(gr_Sigma);
    myGraphs->Add(gr_Height2);
    myGraphs->Add(gr_Mean2);
    myGraphs->Add(gr_Sigma2);

    const char *vars[6] = { "h_{1}", "#mu_{1}", "#sigma_{1}", "h_{2}", "#mu_{2}", "#sigma_{2}" };
    const char *varspdf[6] = { "h_1", "mu_1", "sigma_1", "h_2", "mu_2", "sigma_2" };
    const char *varsunit[6] = { "(counts)", "(V)", "(mV)", "(counts)", "(V)", "(mV)" };
    const char *varsunitleg2[6] = { "", "mV", "mV", "counts", "mV", "mV" };
    double varsfactorleg2[6] = { 1., 1000., 1., 1., 1000., 1.};
    int varsNdiv[6] = {510, 505, 510, 510, 510, 510};
    TIter next(myGraphs);
    TGraph *gr;
    unsigned int j=0;
    while((gr = (TGraph*)next())){
      cout << vars[j]<< endl;
      char title[200] = "";
      sprintf(title," ;Strip group;%s %s", vars[j], varsunit[j]);
      TCanvas *c = new TCanvas("c", "Grouped_turnon", 0, 0, 1000, 600);
      gr->SetTitle(title);
      gr->SetMarkerStyle(21);
      gr->SetMarkerColor(4);
      c->SetRightMargin(0.05);
      c->SetTopMargin(0.05);
      gr->GetXaxis()->SetRangeUser(0, n +1);
      gr->GetXaxis()->SetLabelSize(fontsize);
      gr->GetYaxis()->SetLabelSize(fontsize);

      gr->GetXaxis()->SetTitleOffset(titleoffsetx);
      gr->GetYaxis()->SetTitleOffset(titleoffsety);

      gr->GetXaxis()->SetTitleSize(titlesize);
      gr->GetYaxis()->SetTitleSize(titlesize);
      gr->Draw("AP");
      c->Update(); // necessary to get correct gPad-> xmin and xmax
      TLine *line = new TLine(gPad->GetUxmin(),chippar[j], gPad->GetUxmax(),chippar[j]);
      line->SetLineColor(803);
      line->SetLineWidth(2);
      line->Draw("same");
      c->SaveAs((nameofpdf +to_string(groupstrips) +"perGroup_"+varspdf[j]+ ".pdf").c_str());
      c->Close();

      // Fillhistogram with y-data of tgraph:

      Double_t graph_varY[n];
      for(int i=0; i < n; ++i) {
        graph_varY[i]= gr->GetPointY(i);
      }
      cout << *min_element(graph_varY, graph_varY + n) << " " << *max_element(graph_varY, graph_varY + n) << endl;
      double min_el = *min_element(graph_varY, graph_varY + n);
      double max_el = *max_element(graph_varY, graph_varY + n);
      double width = max_el - min_el;
      char hist_title[200] = "";
      sprintf(hist_title,"%s", vars[j]);
      TH1D *h = new TH1D(hist_title, "Histogram of graph y - data", 15, min_el - 0.2 *width, max_el + 0.2*width);
      for(int i=0; i < n; ++i) {
        h->Fill(graph_varY[i]);
      }
      TCanvas *c2 = new TCanvas("c2", "Histo_ygraph", 0, 0, 800, 900);
      h->Draw();
      char h_title[200] = "";
      sprintf(h_title,";%s %s; Counts", vars[j], varsunit[j]);

      h->SetStats(0);
      TLegend *legend2 = new TLegend(0.64,0.73,1.03,0.96);

      char entrieslegend2[200] = "";
      sprintf(entrieslegend2,"Entries  %.0f", h->GetEntries());
      char meanlegend2[200] = "";
      sprintf(meanlegend2,"Mean  %.1f %s", h->GetMean()*varsfactorleg2[j], varsunitleg2[j]);
      char stdlegend2[200] = "";
      sprintf(stdlegend2,"Std Dev %.1f %s", h->GetStdDev()*varsfactorleg2[j], varsunitleg2[j]);
      legend2->AddEntry((TObject*)0, entrieslegend2,"");
      legend2->AddEntry((TObject*)0, meanlegend2, "");
      legend2->AddEntry((TObject*)0, stdlegend2, "");
      legend2->SetFillStyle(0);
      legend2->SetBorderSize(0);
      legend2->Draw();
      TAxis* a = h->GetXaxis();
      a->SetNdivisions(varsNdiv[j]);

      h->SetTitle(h_title);
      h->SetFillColor(kBlue);
      c2->SetLeftMargin(0.11);
      c2->SetRightMargin(0.05);
      c2->SetTopMargin(0.05);
      h->GetXaxis()->SetLabelSize(fontsize);
      h->GetYaxis()->SetLabelSize(fontsize);

      h->GetXaxis()->SetTitleOffset(titleoffsetx);
      h->GetYaxis()->SetTitleOffset(titleoffsety);

      h->GetXaxis()->SetTitleSize(titlesize);
      h->GetYaxis()->SetTitleSize(titlesize);
      c2->SaveAs((nameofpdf +to_string(groupstrips) +"perGroup_"+varspdf[j]+ "_histogram.pdf").c_str());
      c2->Close();

      j++;
    }
  }
  // return only first fit result as output. So far only this was used for row-comparison.
  vector<Double_t> result_vec = {Height[0], eHeight[0], Mean[0], eMean[0], Sigma[0], eSigma[0], Height2[0], eHeight2[0], Mean2[0], eMean2[0], Sigma2[0], eSigma2[0] };
  return result_vec;
}

// This function takes th2d histogram with filename and histname as input.
// It combines neighboring strips of width groupstrips together (for higher statistics) and applies a Fit to it.
// The size of fitparameters must be 3*Nparameters because the first third is for the initial values followed by the lower and the upper limit of the parameters.
void FitPassedfctToGroupedStrips(string rootfile, unsigned int groupstrips=4, string histname = "counterThresholdScanRow100", string fitfct= "[0]+[1]*x", int Nparameters=2, std::vector<Double_t> fitparameters={0., -1., 1., 0., -1., 1.},  string nameofpdf = "", string bkgr_rootfile = "", string bkgr_histoname ="", string part_source="Americium-241", int signal_hist_color = 802, double bkgr_scale=1.0, float Vstartfit=0.7, float Vendfit=1.17, float maxyrange = 2000, double minyerror = 1, bool plot_Fitresults = true){
  char rfile[200] = "";
  sprintf(rfile,"%s", rootfile.c_str());
  TFile *f = new TFile(rfile);
  TF1 *fitf = new TF1("fFit",fitfct.c_str(), Vstartfit, Vendfit);
  //TF1 *fit_2s = new TF1("fFit","[2]*erf((x-[0])/[1])+ [2]+[5] + [5]*erf((x-[3])/[4])", Vstartfit, Vendfit);// 0.7, 1.155); // two s-curves

  const Int_t n = 64/groupstrips;
  Double_t Mean[n], eMean[n], Sigma[n], eSigma[n], Height[n], eHeight[n], Group[n], eGroup[n];
  for(unsigned int i =0; i < n; i++){
    for(unsigned int p =0; p < Nparameters; p++){
      fitf->SetParameter(p,fitparameters[p]); // set starting value
      fitf->SetParLimits(p, fitparameters[Nparameters+2*p], fitparameters[Nparameters+2*p+1]); // set limits
    }

    TCanvas *c = new TCanvas("c1", "yprojections", 0, 0, 900, 600);
    char histoname[200] = "";
    sprintf(histoname,"%s",histname.c_str());
    TH2D *histo = (TH2D*)f->Get(histoname);
    char py_name[20] = "";
    sprintf(py_name,"Histgroup%02d_yprojection",i);
    TH1D *histogroup = histo->ProjectionY(py_name, i*groupstrips+1, (i+1)*groupstrips);
    // Set bin error to square root of bin content
    for(int b = 0 ; b < histogroup->GetNbinsX(); b++){ // set all errors to same value
      histogroup->SetBinError(b, max(sqrt(histogroup->GetBinContent(b)), minyerror));
    }
    histogroup->Fit("fFit","R"); //,"Q")

    TH1D *bkgr_histosum;
    // Fill backgr histo if defined:
    if(!(bkgr_rootfile=="")){
      char bkgr_rfile[200] = "";
      sprintf(bkgr_rfile,"%s", bkgr_rootfile.c_str());
      TFile *bkgr_f = new TFile(bkgr_rfile);
      char bkgr_charhistoname[200] = "";
      sprintf(bkgr_charhistoname,"%s", bkgr_histoname.c_str());
      TH2D *bkgr_histo = (TH2D*)bkgr_f->Get(bkgr_charhistoname);
      bkgr_histosum = bkgr_histo->ProjectionY("background", 1, 64);
    }

    if(plot_Fitresults){
      Mean[i]=fitf->GetParameter(0);
      eMean[i]=fitf->GetParError(0);
      Sigma[i]=fitf->GetParameter(1);
      eSigma[i]=fitf->GetParError(1);
      Height[i]=fitf->GetParameter(2);
      eHeight[i]=fitf->GetParError(2);
      Group[i] = i+1; // to start numbering with 1
      eGroup[i] = 0.;
    }


    histogroup->SetXTitle("Threshold voltage (V)");
    histogroup->SetYTitle("Counts");
    histogroup->SetFillColor(kBlue);
    histogroup->SetTitle(py_name);
    if((nameofpdf==""))
      nameofpdf = "p_y_2scurvefit_allrow";
    histogroup->SetName(nameofpdf.c_str());
    histogroup->SetStats(0);
    histogroup->SetLineColorAlpha(signal_hist_color, 0.0001);
    histogroup->SetFillColorAlpha(signal_hist_color, 0.5);
    histogroup->Draw("hist");
    fitf->Draw("same");

    char par0[200] = "";
    sprintf(par0,"p0 = %.3f #pm %.3f", fitf->GetParameter(0),fitf->GetParError(0));

    char par1[200] = "";
    sprintf(par1,"p1 = %.3f #pm %.3f", fitf->GetParameter(1),fitf->GetParError(1));
    /*
    char heightlegend1[200] = "";
    sprintf(heightlegend1,"h_{1} = %.1f #pm %.1f ", fitf->GetParameter(2),fitf->GetParError(2));
    char meanlegend2[200] = "";
    sprintf(meanlegend2,"#mu_{2} = %.3f #pm %.3f", fitf->GetParameter(3),fitf->GetParError(3));
    char sigmalegend2[200] = "";
    sprintf(sigmalegend2,"#sigma_{2} = %.3f #pm %.3f", fitf->GetParameter(4),fitf->GetParError(4));
    char heightlegend2[200] = "";
    sprintf(heightlegend2,"h_{2} = %.1f #pm %.1f ", fitf->GetParameter(5),fitf->GetParError(5));
    */

    TLegend *legend = new TLegend(0.12,0.5,0.45,0.89);
    legend->AddEntry(histogroup,  (part_source + " data").c_str());
    if(!(bkgr_rootfile=="")) {
      bkgr_histosum->Scale(bkgr_scale);
      // Set bin error to square root of bin content
      for(int b = 0 ; b < bkgr_histosum->GetNbinsX(); b++){ // set all errors to same value
        bkgr_histosum->SetBinError(b, max(sqrt(bkgr_histosum->GetBinContent(b)), minyerror));
        }
      bkgr_histosum->SetLineColor(kBlue);
      //bkgr_histosum->SetFillColorAlpha(kBlue, 0.5);
      bkgr_histosum->Draw("hist same");
      legend->AddEntry(bkgr_histosum,"Background");
      }

    legend->AddEntry(fitf, fitfct.c_str());

    for(unsigned int p =0; p < Nparameters; p++){
    char par[200] = "";
    sprintf(par,"p%01d = %.3f #pm %.3f", p, fitf->GetParameter(p),fitf->GetParError(p));
    legend->AddEntry((TObject*)0, par, "");
    }
    /*
    legend->AddEntry((TObject*)0, par0, "");
    legend->AddEntry((TObject*)0, par1, "");
    legend->AddEntry((TObject*)0, sigmalegend1, "");
    legend->AddEntry((TObject*)0, heightlegend1, "");
    legend->AddEntry((TObject*)0, meanlegend2, "");
    legend->AddEntry((TObject*)0, sigmalegend2, "");
    legend->AddEntry((TObject*)0, heightlegend2, "");
    */
    legend->SetTextSize(0.03);
    legend->SetBorderSize(0);
    legend->Draw();
    c->SaveAs(("plots/"+nameofpdf +to_string(groupstrips)+ "perGroup" + to_string(i+1) +"of"+to_string(n)+ ".pdf").c_str());
    c->SaveAs(("plots/"+nameofpdf +to_string(groupstrips)+ "perGroup" + to_string(i+1) +"of"+to_string(n)+ ".root").c_str());
    c->SetLogy();
    c->SaveAs(("plots/"+nameofpdf +to_string(groupstrips)+ "perGroup" + to_string(i+1) +"of"+to_string(n)+ "_logy.pdf").c_str());
    c->SaveAs(("plots/"+nameofpdf +to_string(groupstrips)+ "perGroup" + to_string(i+1) +"of"+to_string(n)+ "_logy.root").c_str());
    c->SetLogy(0); // reset to lin scale
    histogroup->SetMaximum(maxyrange); // sets ymax for another linear plot that shows signal counts next to noise peak
    c->SaveAs(("plots/"+nameofpdf +to_string(groupstrips)+ "perGroup" + to_string(i+1) +"of"+to_string(n)+ "smallyrange.pdf").c_str());
    c->SaveAs(("plots/"+nameofpdf +to_string(groupstrips)+ "perGroup" + to_string(i+1) +"of"+to_string(n)+ "smallyrange.root").c_str());
    c->Close();
  }
  if(plot_Fitresults){
    auto gr_Sigma = new TGraphErrors(n,Group,Sigma,eGroup,eSigma);
    auto gr_Mean = new TGraphErrors(n,Group,Mean,eGroup,eMean);
    auto gr_Height = new TGraphErrors(n,Group,Height,eGroup,eHeight);

    TList *myGraphs = new TList();
    myGraphs->Add(gr_Sigma);
    myGraphs->Add(gr_Mean);
    myGraphs->Add(gr_Height);
    const char *vars[3] = { "sigma", "mean", "height" };
    const char *varsunit[3] = { "(V)", "(V)", "(counts)" };
    TIter next(myGraphs);
    TGraph *gr;
    unsigned int j=0;
    while((gr = (TGraph*)next())){
      cout << vars[j]<< endl;
      char title[200] = "";
      sprintf(title,"%s of the signal turn-on curves for the %02d groups ;Strip group;%s %s", vars[j], groupstrips, vars[j], varsunit[j]);
      TCanvas *c = new TCanvas("c", "Grouped_turnon", 0, 0, 1000, 600);
      gr->SetTitle(title);
      gr->SetMarkerStyle(21);
      gr->SetMarkerColor(4);
      gr->GetXaxis()->SetRangeUser(0, n +1);
      gr->Draw("AP");
      c->SaveAs(("plots/"+nameofpdf +to_string(groupstrips) +"perGroup_"+vars[j]+ ".pdf").c_str());
      c->Close();
      j++;
    }
  }
}


// THis fct produces the double s-curve fits for multiple files. Different Row numbers can be passed in the filelist variable.
// The fits are plotted and also the parameters plotted over the row index.
void Compare_Rows_Cu(string filepath="", std::vector<string> filelist={"11_Cu_Row01"} ,std::vector<double> Rows={2},  string filesuffix = "_1e8_MainScan.root", std::vector<Double_t> fitparameters={0., -1., 1., 0., -1., 1.}, string pdfprefix="", string bkgr_rfile = "", string bkgr_hist ="", string part_source="Americium-241", int signal_hist_color = 802, double bkgr_scale=1.0, float Vstartfit=1.0, float Vendfit=1.17, float maxyrange = 2000, vector<Double_t> chippar={0.,0.,0.,0.,0.,0.}, float fontsize=0.04, float titlesize=0.045, float titleoffsety=1.21, float titleoffsetx=0.87){

  const Int_t n = filelist.size();
  Double_t Mean[n], eMean[n], Sigma[n], eSigma[n], Height[n], eHeight[n], Rowindex[n], eRowindex[n];
  Double_t Mean2[n], eMean2[n], Sigma2[n], eSigma2[n], Height2[n], eHeight2[n];

  for(unsigned int i=0; i<n; i++){
    char histname[200] = "";
    sprintf(histname,"counterThresholdScanRow%s",filelist[i].substr(9,2).c_str()); // to obtain Row number
    char pdfname[200] = "";
    sprintf(pdfname,"%s_Row%s_1e8_2scurvefit",pdfprefix.c_str(), filelist[i].substr(9,2).c_str()); // to obtain Row number
    vector<double> fitresults=Fit2scurvesToGroupedStrips(filepath+filelist[i]+filesuffix, 64, histname,fitparameters, pdfname,
    bkgr_rfile, bkgr_hist,part_source, signal_hist_color,bkgr_scale, Vstartfit, Vendfit, maxyrange,1, false);
    Height[i]=fitresults[0];
    eHeight[i]=fitresults[1];
    Mean[i]=fitresults[2];
    eMean[i]=fitresults[3];
    Sigma[i]=fitresults[4];
    eSigma[i]=fitresults[5];
    Height2[i]=fitresults[6];
    eHeight2[i]=fitresults[7];
    Mean2[i]=fitresults[8];
    eMean2[i]=fitresults[9];
    Sigma2[i]=fitresults[10];
    eSigma2[i]=fitresults[11];
    Rowindex[i] = Rows[i];
    eRowindex[i] = 0.;
  }

    auto gr_Height = new TGraphErrors(n,Rowindex,Height,eRowindex,eHeight);
    auto gr_Mean = new TGraphErrors(n,Rowindex,Mean,eRowindex,eMean);
    auto gr_Sigma = new TGraphErrors(n,Rowindex,Sigma,eRowindex,eSigma);
    auto gr_Height2 = new TGraphErrors(n,Rowindex,Height2,eRowindex,eHeight2);
    auto gr_Mean2 = new TGraphErrors(n,Rowindex,Mean2,eRowindex,eMean2);
    auto gr_Sigma2 = new TGraphErrors(n,Rowindex,Sigma2,eRowindex,eSigma2);

    TList *myGraphs = new TList();
    myGraphs->Add(gr_Height);
    myGraphs->Add(gr_Mean);
    myGraphs->Add(gr_Sigma);
    myGraphs->Add(gr_Height2);
    myGraphs->Add(gr_Mean2);
    myGraphs->Add(gr_Sigma2);

    const char *vars[6] = { "h_{1}", "#mu_{1}", "#sigma_{1}", "h_{2}", "#mu_{2}", "#sigma_{2}" };
    const char *varspdf[6] = { "h_1", "mu_1", "sigma_1", "h_2", "mu_2", "sigma_2" };
    const char *varsunit[6] = { "(counts)", "(V)", "(mV)", "(counts)", "(V)", "(mV)" };
    const char *varsunitleg2[6] = { "", "mV", "mV", "counts", "mV", "mV" };
    double varsfactorleg2[6] = { 1., 1000., 1., 1., 1000., 1.};
    int varsNdiv[6] = {510, 510, 510, 510, 510, 510}; // i.g. 505 for less axis labels
    TIter next(myGraphs);
    TGraph *gr;
    unsigned int j=0;
    while((gr = (TGraph*)next())){
      cout << vars[j]<< endl;
      char title[200] = "";
      sprintf(title," ;Row number;%s %s", vars[j], varsunit[j]);
      TCanvas *c = new TCanvas("c", "Grouped_turnon", 0, 0, 1000, 600);
      gr->SetTitle(title);
      gr->SetMarkerStyle(21);
      gr->SetMarkerColor(4);
      c ->SetLeftMargin(0.11);
      c->SetRightMargin(0.05);
      c->SetTopMargin(0.05);
      //gr->GetXaxis()->SetRangeUser(0, n +1);
      gr->GetXaxis()->SetLabelSize(fontsize);
      gr->GetYaxis()->SetLabelSize(fontsize);

      gr->GetXaxis()->SetTitleOffset(titleoffsetx);
      gr->GetYaxis()->SetTitleOffset(titleoffsety);

      gr->GetXaxis()->SetTitleSize(titlesize);
      gr->GetYaxis()->SetTitleSize(titlesize);
      gr->Draw("AP");

      // Fillhistogram with y-data of tgraph
      // take data here already, to plot std and mean in graph plot (canvas c)

      Double_t graph_varY[n];
      for(int i=0; i < n; ++i) {
        graph_varY[i]= gr->GetPointY(i);
      }
      cout << *min_element(graph_varY, graph_varY + n) << " " << *max_element(graph_varY, graph_varY + n) << endl;
      double min_el = *min_element(graph_varY, graph_varY + n);
      double max_el = *max_element(graph_varY, graph_varY + n);
      double width = max_el - min_el;
      char hist_title[200] = "";
      sprintf(hist_title,"%s", vars[j]);
      TH1D *h = new TH1D(hist_title, "Histogram of graph y - data", 15, min_el - 0.2 *width, max_el + 0.2*width);
      for(int i=0; i < n; ++i) {
        h->Fill(graph_varY[i]);
      }
      TLegend *legend1 = new TLegend(0.08,0.85,1.0,1.0);
      legend1->SetNColumns(3);

      char entrieslegend[200] = "";
      sprintf(entrieslegend,"Entries  %.0f", h->GetEntries());
      char meanlegend[200] = "";
      sprintf(meanlegend,"Mean  %.1f %s", h->GetMean()*varsfactorleg2[j], varsunitleg2[j]);
      char stdlegend[200] = "";
      sprintf(stdlegend,"Std Dev  %.1f %s", h->GetStdDev()*varsfactorleg2[j], varsunitleg2[j]);

      legend1->AddEntry((TObject*)0, entrieslegend,"");
      legend1->AddEntry((TObject*)0, meanlegend, "");
      legend1->AddEntry((TObject*)0, stdlegend, "");
      legend1->SetFillStyle(0);
      legend1->SetBorderSize(0);

      legend1->Draw();
      c->Update(); // necessary to get correct gPad-> xmin and xmax

      TLine *line = new TLine(gPad->GetUxmin(),chippar[j], gPad->GetUxmax(),chippar[j]);
      line->SetLineColor(803);
      line->SetLineWidth(2);
      line->Draw("same");


      c->SaveAs((pdfprefix + "RowComparison_of_" +to_string(n) +"Rows_"+varspdf[j]+ ".pdf").c_str());
      c->Close();

      // plotting histogram

      TCanvas *c2 = new TCanvas("c2", "Histo_ygraph", 0, 0, 800, 900);
      h->Draw();
      char h_title[200] = "";
      sprintf(h_title,";%s %s; Counts", vars[j], varsunit[j]);

      h->SetStats(0);
      TLegend *legend = new TLegend(0.64,0.73,1.03,0.96);
      legend->AddEntry((TObject*)0, entrieslegend,"");
      legend->AddEntry((TObject*)0, meanlegend, "");
      legend->AddEntry((TObject*)0, stdlegend, "");
      legend->SetFillStyle(0);
      legend->SetBorderSize(0);
      legend->Draw();
      TAxis* a = h->GetXaxis();
      a->SetNdivisions(varsNdiv[j]);

      h->SetTitle(h_title);
      h->SetFillColor(kBlue);
      c2->SetLeftMargin(0.11);
      c2->SetRightMargin(0.05);
      c2->SetTopMargin(0.05);
      h->GetXaxis()->SetLabelSize(fontsize);
      h->GetYaxis()->SetLabelSize(fontsize);

      h->GetXaxis()->SetTitleOffset(titleoffsetx);
      h->GetYaxis()->SetTitleOffset(titleoffsety);

      h->GetXaxis()->SetTitleSize(titlesize);
      h->GetYaxis()->SetTitleSize(titlesize);
      c2->SaveAs((pdfprefix + "RowComparison_of_" +to_string(n) +"Rows_"+varspdf[j]+ "_histogram.pdf").c_str());
      c2->Close();

      j++;
    }
  }



// analyse rootfile from VarycTScanparameters(), where either maxcap or nsleep from capturecounters is varied.
// Pass the same val arguments as in the measurement function
void cTScan_VariableVariation(string rootfile, string histoprefix, unsigned int valstart, unsigned int valend, unsigned int valstep, string variable, string nameofpdf="Variation"){
  char rfile[200] = "";
  sprintf(rfile,"%s", rootfile.c_str());
  TFile *f = new TFile(rfile);
  unsigned int count = 0;
  unsigned int valnum = (valend-valstart)/valstep + 1;
  TGraph *gr_Std = new TGraph(valnum);
  TGraph *gr_Mean = new TGraph(valnum);
  TGraph *gr_Maximum = new TGraph(valnum);
  TGraph *gr_Integral = new TGraph(valnum);
  for (unsigned int val = valstart; val <= valend; val = val+valstep){
    char histoname[200] = "";
    sprintf(histoname,"%s%04d",histoprefix.c_str(), val);
    TH2D *histo = (TH2D*)f->Get(histoname);
    TH1D *projy = histo->ProjectionY("histo_py", 1,64); // sum all strips together
    float std = projy->GetStdDev();
    float mean = projy->GetMean();
    float maximum = projy->GetMaximum();
    float integral = projy->Integral();
    gr_Std->SetPoint(count, val, std);
    gr_Mean->SetPoint(count, val, mean);
    gr_Maximum->SetPoint(count, val, maximum);
    gr_Integral->SetPoint(count, val, integral);
    count++;
  }
  create_four_variablePlots(gr_Std, gr_Mean, gr_Maximum, gr_Integral, "Variation of " + variable, variable, nameofpdf);
}

// function to animate randomn noise behavior for nstrobes cycles.
// p is the probability to change from above to below threshold or vice versa per time step.
unsigned int animate_Noise(unsigned int nStrobes, float p, unsigned int max_counts=3, int pixels = 64){
  //bool x = 0;
  std::vector<bool> x(pixels,0);
  unsigned int n = 0;
  unsigned int counts=0;
  unsigned int cycle_counts;

  while (n<nStrobes){
    cycle_counts = 0;
    for(unsigned int i = 0; i< pixels; i++){ // loop over all pixels in strip/ pad.
      if ((float) rand()/RAND_MAX < p){
        if(x[i]==0){ // with probability p and if x=0 change state and count
          x[i]=1;
          cycle_counts++;
        }
        else x[i]=0; // only change state without counting
      }
    }
    counts = counts + std::min(cycle_counts, max_counts);
    n++;
  }
  cout << p << " "<< counts << endl;
  return counts;
}

void plot_pVariation(unsigned int nStrobes, float p_min, float p_max, float p_step, unsigned int npixels = 64){
  unsigned int p_num = int((p_max-p_min)/p_step) + 1;
  TGraph *gr_Counts_3 = new TGraph(p_num);
  TGraph *gr_Counts_15 = new TGraph(p_num);
  TCanvas *c = new TCanvas("c", "title", 0, 0, 1100, 600);

  for(unsigned int i = 0; i< p_num; i++){
    gr_Counts_3->SetPoint(i, p_min + i*p_step, (double)animate_Noise(nStrobes, p_min + i*p_step,3, npixels));
    gr_Counts_15->SetPoint(i, p_min + i*p_step, (double)animate_Noise(nStrobes, p_min + i*p_step,15, npixels));

  }
  char title[200] = "";
  sprintf(title,"Variation of saturation for a %d pixel strip during %d clock cycles ;probability of transition; Counts", npixels, nStrobes);
  gr_Counts_15->SetTitle(title);
  gr_Counts_15->SetLineColor(kOrange -3);
  gr_Counts_15->Draw();
  gr_Counts_3->SetLineStyle(9);
  gr_Counts_3->SetLineColor(38);
  gr_Counts_3->Draw("same");
  TLegend *legend = new TLegend(0.15,0.75,0.35,0.9);
  legend->AddEntry(gr_Counts_15, "max 15 counts per strip");
  legend->AddEntry(gr_Counts_3,"max 3 counts per strip");
  legend->Draw();
  c->SaveAs(("Saturation_Variation_" + to_string(npixels)+  "Pixels.pdf").c_str());
  c->Close();
}

// Analysis function to measurement of Current_cTScanparameter_Variation()
// the drift per pixel/ strip is determined for various currents.
void Current_cTScanparameter_Analysis(string rootfolder, unsigned int ISteps, float Irange, float Ioffset, unsigned int valstart, unsigned int valend, unsigned int valstep, string parameter ="nsleep", string whatenabled = "Chip", string histoprefix="counterThresholdScannsleep"){
  TF1 *flin = new TF1("flin", "[0]+[1]*x", -1, 85000);
  unsigned int valnum = (valend-valstart)/valstep + 1;
  Double_t V_gradient[ISteps+1], V_gradient_errors[ISteps+1], V_offset[ISteps+1], V_offset_errors[ISteps+1], IShaperBias[ISteps+1], eXDummy[ISteps+1];
  for (unsigned int i=0; i< ISteps+1; i++){
    float Ival = Ioffset + i*Irange/ISteps;
    char filename[200] = "";
    sprintf(filename,"%s/Current_nsleepVariation_%s_Ival%.0f.root", rootfolder.c_str(), whatenabled.c_str(),  Ival);
    TFile *f = new TFile(filename);
    TGraph *gr_Std = new TGraph(valnum);
    TGraph *gr_Mean = new TGraph(valnum);
    char title[200] = "";
    sprintf(title,"Nsleep variation for %s enabled and I_{ShaperBias} = %.0f #muA  ; %s (80 MHz clock cycles); Mean (V)", whatenabled.c_str(), Ival, parameter.c_str());
    cout << title << endl;
    unsigned int count = 0;
    for (unsigned int val = valstart; val <= valend; val = val+valstep){
      char histoname[200] = "";
      sprintf(histoname,"%s%04d",histoprefix.c_str(), val);
      TH2D *histo = (TH2D*)f->Get(histoname);
      TH1D *projy = histo->ProjectionY("histo_py", 1,64); // sum all strips together
      float std = projy->GetStdDev();
      float mean = projy->GetMean();
      gr_Std->SetPoint(count, val, std);
      gr_Mean->SetPoint(count, val, mean);
      count++;
    }
    TCanvas *c1 = new TCanvas("c1", "title", 0, 0, 1100, 600);
    flin->SetParameters(1.17, - 0.03/80000);
    flin->SetLineColor(kOrange+7);
    gr_Mean->Fit("flin");
    gr_Mean->SetTitle(title);
    gr_Mean->SetMarkerStyle(5);
    gr_Mean->SetMarkerColor(kBlue+3);
    gr_Mean->SetMarkerSize(2); // default is 1
    gr_Mean->Draw("AP");
    char model[200] = "f(x) = mx + b  ";
    char mlegend[200] = "";
    sprintf(mlegend,"m = %.3e #pm %.3e", flin->GetParameter(1),flin->GetParError(1));
    char blegend[200] = "";
    sprintf(blegend,"b = %.3e #pm %.3e", flin->GetParameter(0),flin->GetParError(0));
    char Vdriftlegend[200]= "";
    sprintf(Vdriftlegend,"V_{Drift} = %.1f #pm %.1f (mV/ms)", 80000000*flin->GetParameter(1), 80000000*flin->GetParError(1));
    TLegend *legend = new TLegend(0.55,0.65,0.9,0.89);
    legend->AddEntry(flin, model);
    legend->AddEntry((TObject*)0, mlegend, "");
    legend->AddEntry((TObject*)0, blegend, "");
    legend->AddEntry((TObject*)0, Vdriftlegend, "");
    legend->SetTextSize(0.04);
    legend->SetTextFont(52);
    legend->Draw();
    c1->SaveAs(("CurrentVariation_"+whatenabled+"_IVal" + to_string((int) Ival)+  ".pdf").c_str());
    c1->Close();
    cout << flin->GetParameter(0) << " " << flin->GetParError(0) << " "  << flin->GetParameter(1)<< " " << flin->GetParError(1)<< endl;
    // Fill the vectors with the fit parameters for plotting
    V_gradient[i]=80000000*flin->GetParameter(1); // from unit (V/ clock cycle) to (mV/ms)
    V_gradient_errors[i]=80000000*flin->GetParError(1);
    V_offset[i]=flin->GetParameter(0);
    V_offset_errors[i]=flin->GetParError(0);
    IShaperBias[i]=Ival;
    eXDummy[i]=0.;
  }
  // Plot fit parameters:
  auto *gr_Voffset =  new TGraphErrors(ISteps+1,IShaperBias,V_offset,eXDummy, V_offset_errors);
  auto *gr_Vgradient =  new TGraphErrors(ISteps+1,IShaperBias,V_gradient,eXDummy, V_gradient_errors);

  TList *myGraphs = new TList();
  myGraphs->Add(gr_Voffset);
  myGraphs->Add(gr_Vgradient);
  const char *vars[2] = { "Voltage_offset", "Voltage_gradient"};
  const char *varsunit[2] = { "(V)", "(mV/ms)" };
  TIter next(myGraphs);
  TGraph *gr;
  unsigned int j=0;
  while((gr = (TGraph*)next())){
    cout << vars[j]<< endl;
    char title[200] = "";
    sprintf(title,"%s under I_{ShaperBias} variation for %s enabled ; I_{ShaperBias} (#muA) ; %s %s",vars[j], whatenabled.c_str(), vars[j], varsunit[j]);
    TCanvas *c2 = new TCanvas("c2", "Drift_CurrentVariation", 0, 0, 1000, 600);
    gr->SetTitle(title);
    //gr->GetXaxis()->SetTitleSize(0.6); Fails to set Size, because Title not visible
    gr->SetMarkerStyle(21);
    gr->SetMarkerColor(kBlue+3);
    gr->Draw("AP");
    c2->SaveAs((to_string(j)+vars[j]+ "_ShaperBiasVariation_" + whatenabled+ ".pdf").c_str());
    //c->SaveAs((nameofpdf +to_string(groupstrips) +"perGroup_"+vars[j]+ ".pdf").c_str());
    c2->Close();
    j++;
  }
}

//Fct to analyse dependence between voltage drift of a pixel and its tuning DAC value.
//It was observed that some pixels drift particularly quickly.
//Are these pixels the one with an extreme DAC value? Let's see....
// rootfolder is the one where the root files for each row is stored that was measured using VarycTScanparameters().

void VDrift_TuningDAC_Correlation(string rootfolder, string configtxt, unsigned int valstart, unsigned int valend, unsigned int valstep, string pdfprefix="", unsigned int PlotRow=65, unsigned int PlotCol=65, string parameter ="nsleep", string histoprefix="counterThresholdScannsleep", bool header = true, float fontsize=0.04, float titlesize=0.045, float titleoffsety=1.21, float titleoffsetx=0.87, float titleoffsetz=0.5){
  TF1 *flin = new TF1("flin", "[0]+[1]*x", -1, 85000);
  unsigned int valnum = (valend-valstart)/valstep + 1;
  vector<vector<Double_t>> Drifts( 64 , vector<Double_t> (64));
  for (unsigned int row=0; row< 64; row++){
    char filename[200] = "";
    sprintf(filename,"%s/nsleepVariationScanRow%02d.root", rootfolder.c_str(),  row);
    TFile *f = new TFile(filename);
    for (unsigned int col=0; col< 64; col++){
      TGraph *gr_Mean = new TGraph(valnum);
      unsigned int count = 0;
      for (unsigned int val = valstart; val <= valend; val = val+valstep){
        char histoname[200] = "";
        sprintf(histoname,"%s%04d",histoprefix.c_str(), val);
        TH2D *histo = (TH2D*)f->Get(histoname);
        TH1D *projy = histo->ProjectionY("histo_py", col+1,col+1); // take individual rows
        float mean = projy->GetMean();
        gr_Mean->SetPoint(count, val, mean);
        count++;
      }
      flin->SetParameters(1.17, - 0.03/80000);
      flin->SetLineColor(kOrange+7);
      gr_Mean->Fit("flin", "Q");
      if (col== PlotCol and row == PlotRow){
        TCanvas *clinfit = new TCanvas("clinfit", "title", 0, 0, 1100, 600);
        gr_Mean->SetMarkerStyle(5);
        gr_Mean->SetMarkerColor(kBlue+3);
        gr_Mean->SetMarkerSize(3); // default is 1
        char title[200] = "; nsleep (80 MHz clock cycles); Mean of noise peak (V)";
        //sprintf(title,"Voltage drift of pixel in Row %02d, Col %02d; nsleep (80 MHz clock cycles); Mean (V)", PlotRow, PlotCol);
        gr_Mean->SetTitle(title);
        gr_Mean->Draw("AP");
        char model[200] = "f(x) = mx + b  ";
        char mlegend[200] = "";
        sprintf(mlegend,"m = %.2e #pm %.2e V/c.c.", flin->GetParameter(1),flin->GetParError(1));
        char blegend[200] = "";
        sprintf(blegend,"b  =  %.2e #pm %.2e V", flin->GetParameter(0),flin->GetParError(0));
        char Vdriftlegend[200]= "";
        sprintf(Vdriftlegend,"V_{Drift} = %.1f #pm %.1f mV/ms", 80000000*flin->GetParameter(1), 80000000*flin->GetParError(1));
        TLegend *legendfit = new TLegend(0.32,0.6,0.95,0.95);
        legendfit->AddEntry(flin, model);
        legendfit->AddEntry((TObject*)0, mlegend, "");
        legendfit->AddEntry((TObject*)0, blegend, "");
        legendfit->AddEntry((TObject*)0, Vdriftlegend, "");
        legendfit->SetFillStyle(0);
        legendfit->SetBorderSize(0);
        //legendfit->SetTextSize(0.04);
        //legendfit->SetTextFont(52);
        legendfit->Draw();
        gr_Mean->GetXaxis()->SetLabelSize(fontsize);
        gr_Mean->GetYaxis()->SetLabelSize(fontsize);

        gr_Mean->GetXaxis()->SetTitleOffset(titleoffsetx);
        gr_Mean->GetYaxis()->SetTitleOffset(titleoffsety-0.1);

        gr_Mean->GetXaxis()->SetTitleSize(titlesize);
        gr_Mean->GetYaxis()->SetTitleSize(titlesize);

        clinfit->SetRightMargin(0.05);
        clinfit->SetTopMargin(0.05);

        clinfit->SaveAs((pdfprefix+"Voltagedrift_Row"+ to_string(PlotRow)+"_Col" + to_string(PlotCol)+  ".pdf").c_str());
        clinfit->Close();
      }



/*
flin->SetLineColor(kOrange+7);
gr_Mean->Fit("flin");
gr_Mean->SetTitle(title);
gr_Mean->SetMarkerStyle(5);
gr_Mean->SetMarkerColor(kBlue+3);
gr_Mean->SetMarkerSize(2); // default is 1
gr_Mean->Draw("AP");
char model[200] = "f(x) = mx + b  ";
char mlegend[200] = "";
sprintf(mlegend,"m = %.3e #pm %.3e", flin->GetParameter(1),flin->GetParError(1));
char blegend[200] = "";
sprintf(blegend,"b = %.3e #pm %.3e", flin->GetParameter(0),flin->GetParError(0));

TLegend *legend = new TLegend(0.55,0.65,0.9,0.89);
legend->AddEntry(flin, model);
legend->AddEntry((TObject*)0, mlegend, "");
legend->AddEntry((TObject*)0, blegend, "");
legend->SetTextSize(0.04);
legend->SetTextFont(52);
legend->Draw();
c1->SaveAs(("CurrentVariation_"+whatenabled+"_IVal" + to_string((int) Ival)+  ".pdf").c_str());
c1->Close();
*/


      Drifts[row][col] = 80000000*flin->GetParameter(1); // convert from 80 MHz clock cycle to second
    }
  }
  TH2D *DAC_Drift = new TH2D("dac_drift", "Voltage drift over DAC values of pixels", 16, -0.5, 15.5, 90, -90, 0.0);  //-0.000001, 0.0);
  TH2D *Pol_Drift = new TH2D("pol_drift", "Voltage drift over polarity of pixels", 2, -0.5, 1.5, 90, -90, 0.0);
  TH2D *DACPol_Drift = new TH2D("dacpol_drift", "Voltage drift over tuning configuration per pixel", 32, -0.5, 31.5, 90, -90, 0.0);
  TH2D *Row_Drift = new TH2D("row_drift", "Voltage drift over row of pixels", 64, -0.5, 63.5, 90, -90, 0.0);
  TH2D *Col_Drift = new TH2D("col_drift", "Voltage drift over column of pixels", 64, -0.5, 63.5, 90, -90, 0.0);
  TH2D *heatmap_Drift = new TH2D("Heatmap drifts", "Voltage drift over chip geometry", 64, -0.5, 63.5, 64, -0.5, 63.5);
  TH1D *TH1D_Drift = new TH1D("drift", "Voltage drift of pixels", 80, -80, 0.0);

  // Read in DAC and Pol values from Tuning
  vector<vector<int>> DACs( 64 , vector<int> (64));
  vector<vector<int>> Pols( 64 , vector<int> (64));
  std::ifstream infile(configtxt);
  std::string line;
  if(header)
    getline(infile, line); // skips header line
  int row , col, dac, pol, dacpol;

  while(getline(infile, line)) {
    std::istringstream iss(line);
    iss >> row >> col >> dac >> pol;
    DACs[row][col-1]= dac;
    Pols[row][col-1]= pol;
    //cout << row << " " << col << " " << DACs[row][col-1] << " " << dac << "" << Pols[row][col-1] << " " << pol<< endl;
  }
  for (unsigned int Row=0; Row< 64; Row++){
    for (unsigned int Col=0; Col< 64; Col++){
      DAC_Drift->Fill( DACs[Row][Col], Drifts[Row][Col]); // Drift in (V)
      Pol_Drift->Fill( Pols[Row][Col], Drifts[Row][Col]);
      DACPol_Drift->Fill( (2*Pols[Row][Col]-1)*(DACs[Row][Col] +0.5) +15.5, Drifts[Row][Col]);
      Row_Drift->Fill( Row, Drifts[Row][Col]);
      Col_Drift->Fill( Col, Drifts[Row][Col]);
      heatmap_Drift->Fill(Col, Row, Drifts[Row][Col]);
      TH1D_Drift->Fill(Drifts[Row][Col]);

    }
  }
  TList *myHistos = new TList();
  myHistos->Add(DAC_Drift);
  myHistos->Add(Pol_Drift);
  myHistos->Add(DACPol_Drift);
  myHistos->Add(Row_Drift);
  myHistos->Add(Col_Drift);
  const char *xvars[5] = { "DAC", "Pol" , "DACPol", "Row", "Col"};
  TIter next(myHistos);
  TH2D *histo;
  unsigned int j=0;
  while((histo = (TH2D*)next())){
    cout << xvars[j]<< endl;
    TCanvas *c = new TCanvas("c", "Driftplots", 0, 0, 1000, 600);
    histo->SetXTitle(xvars[j]);
    histo->SetYTitle("Voltage drift ( mV / ms)");
    histo->SetZTitle("Counts");
    histo->SetTitle("");
    histo->Draw("colz");
    string pdfname = xvars[j];
    cout << " MARGIN " << c->GetRightMargin() << " " << c->GetTopMargin()<<  endl;

    histo->GetXaxis()->SetLabelSize(fontsize);
    histo->GetYaxis()->SetLabelSize(fontsize);
    histo->GetZaxis()->SetLabelSize(fontsize);

    histo->GetXaxis()->SetTitleOffset(titleoffsetx);
    histo->GetYaxis()->SetTitleOffset(titleoffsety-0.1);
    histo->GetZaxis()->SetTitleOffset(titleoffsetz+0.2);

    histo->GetXaxis()->SetTitleSize(titlesize);
    histo->GetYaxis()->SetTitleSize(titlesize);
    histo->GetZaxis()->SetTitleSize(titlesize);

    c->SetTopMargin(0.33);
    c->SetRightMargin(0.15);
    c->SaveAs((pdfprefix + pdfname + ".pdf").c_str());
    c->Close();
    j++;
    }
  // DRAW DRIFTS over chip geometry as heatmap
  TCanvas *c0 = new TCanvas("c","Graph2D",900, 600);
  heatmap_Drift->GetXaxis()->SetTitle("Column");
  heatmap_Drift->GetYaxis()->SetTitle("Row");
  heatmap_Drift->SetZTitle("Voltage drift (mV / ms)");
  heatmap_Drift->SetTitle("");
  heatmap_Drift->GetXaxis()->SetLabelSize(fontsize);
  heatmap_Drift->GetYaxis()->SetLabelSize(fontsize);
  heatmap_Drift->GetZaxis()->SetLabelSize(fontsize);

  heatmap_Drift->GetXaxis()->SetTitleOffset(titleoffsetx);
  heatmap_Drift->GetYaxis()->SetTitleOffset(titleoffsety-0.3);
  heatmap_Drift->GetZaxis()->SetTitleOffset(titleoffsetz+0.4);

  heatmap_Drift->GetXaxis()->SetTitleSize(titlesize);
  heatmap_Drift->GetYaxis()->SetTitleSize(titlesize);
  heatmap_Drift->GetZaxis()->SetTitleSize(titlesize);

  c0->SetTopMargin(0.05);
  c0->SetRightMargin(0.38);
  heatmap_Drift->Draw("COLZ");
  c0->SaveAs((pdfprefix+"Drift_Heatmap.pdf").c_str());
  c0->Close();

  // DRAW TH1D of Drifts
  TCanvas *c = new TCanvas("c", "Drifts", 0, 0, 1000, 600);
  TH1D_Drift->SetXTitle("Voltage drift ( mV / ms) ");
  TH1D_Drift->SetYTitle("Counts");
  TH1D_Drift->SetTitle("");
  TH1D_Drift->SetStats(0);
  TH1D_Drift->SetMinimum(-5);
  TH1D_Drift->SetFillColorAlpha(kBlue+3, 0.6);
  TH1D_Drift->SetLineColorAlpha(kBlue+3, 0.0001);
  TH1D_Drift->Draw();

  TH1D_Drift->GetXaxis()->SetLabelSize(fontsize);
  TH1D_Drift->GetYaxis()->SetLabelSize(fontsize);

  TH1D_Drift->GetXaxis()->SetTitleOffset(titleoffsetx);
  TH1D_Drift->GetYaxis()->SetTitleOffset(titleoffsety-0.1);

  TH1D_Drift->GetXaxis()->SetTitleSize(titlesize);
  TH1D_Drift->GetYaxis()->SetTitleSize(titlesize);

  c->SetRightMargin(0.05);
  c->SetTopMargin(0.05);

  TLegend *legend = new TLegend(0.11,0.5,0.7,0.96);

  char entrieslegend[200] = "";
  sprintf(entrieslegend,"Entries    %.0f", TH1D_Drift->GetEntries());
  char meanlegend[200] = "";
  sprintf(meanlegend,   "Mean     %.1f mV/ms", TH1D_Drift->GetMean());
  char stdlegend[200] = "";
  sprintf(stdlegend,    "Std Dev   %.1f mV/ms", TH1D_Drift->GetStdDev());
  legend->AddEntry((TObject*)0, entrieslegend,"");
  legend->AddEntry((TObject*)0, meanlegend, "");
  legend->AddEntry((TObject*)0, stdlegend, "");
  legend->SetFillStyle(0);
  legend->SetBorderSize(0);
  legend->Draw();

  c->SaveAs((pdfprefix + "Drifts.pdf").c_str());
  c->SaveAs((pdfprefix + "Drifts.root").c_str());
  c->Close();

  // Plot mean and std of DAC_Drift y projections.
  int n = 16;
  Double_t Mean[n], Sigma[n], gr_DACs[n], eDACs[n];
  for(int d = 0; d < n; d++){
    Sigma[d] = DAC_Drift->ProjectionY("s0", d+1, d+1)->GetStdDev();
    Mean[d] = DAC_Drift->ProjectionY("m0", d+1, d+1)->GetMean();
    gr_DACs[d] = d;
    eDACs[d] = 0.0;
  }
  auto gr = new TGraphErrors(n,gr_DACs,Mean,eDACs,Sigma);
  TCanvas *c_gr = new TCanvas("cgr", "DAC_Drift_Graph", 0, 0, 1000, 600);
  char title[200] = "";
  sprintf(title,";DAC ; Voltage drift ( mV / ms)"); // Mean of voltage drift for each DAC value
  gr->SetTitle(title);

  gr->SetMarkerStyle(21);
  gr->SetMarkerColor(4);
  gr->GetXaxis()->SetRangeUser(-0.5, 16);
  gr->SetMaximum(-5);
  TF1 *flin2 = new TF1("flin2", "[0]+[1]*x", -1, 17);
  flin2->SetParameters(-20, 0.6);
  gr->Fit("flin2");
  gr->Draw("AP");
  flin2->SetLineColor(kOrange+7);
  flin2->Draw("same");

  gr->GetXaxis()->SetLabelSize(fontsize);
  gr->GetYaxis()->SetLabelSize(fontsize);

  gr->GetXaxis()->SetTitleOffset(titleoffsetx);
  gr->GetYaxis()->SetTitleOffset(titleoffsety-0.3);

  gr->GetXaxis()->SetTitleSize(titlesize);
  gr->GetYaxis()->SetTitleSize(titlesize);
  TLegend *legend2 = new TLegend(0.4,0.12,1.0,0.4);

  char model[200] = "f(x) = mx + b  ";
  char mlegend[200] = "";
  sprintf(mlegend,"m = %.3f #pm %.3f mV/ms", flin2->GetParameter(1), flin2->GetParError(1));
  char blegend[200] = "";
  sprintf(blegend,"b  =  %.2f #pm %.2f mV/ms", flin2->GetParameter(0), flin2->GetParError(0));
  legend2->AddEntry((TObject*)0, mlegend, "");
  legend2->AddEntry((TObject*)0, blegend, "");
  legend2->AddEntry(flin2, model);
  legend2->SetFillStyle(0);
  legend2->SetBorderSize(0);
  legend2->Draw();

  c_gr->SetRightMargin(0.05);
  c_gr->SetTopMargin(0.05);

  c_gr->SaveAs((pdfprefix + "TGraphErrors.pdf").c_str());
  c_gr->Close();
}


//Fct to analyse dependence between voltage drift of a pixel and its tuning DAC value as well as the calibration DAC current.
// This id analysis-function "2", and analysis a scan of each pixel through different DAC values.
// The other (above) analysis function has taken only one DAC value for each pixel. It was the "optimal" one from the tuning routine.
//It was observed that some pixels drift particularly quickly.
//Are these pixels the one with an extreme DAC value? Let's see....
// rootfolder is the one where the root files for each row is stored that was measured using VarycTScanparameters().

void VDrift_TuningDAC_Correlation_2(string rootfolder, unsigned int row, unsigned int Inumvalues, float Istepsize, float Ioffset, unsigned int valstart, unsigned int valend, unsigned int valstep, unsigned int polstart=0, unsigned int DACstart = 0, unsigned int DACstop = 15, unsigned int DACstep =1, string pdfprefix="", unsigned int PlotRow=65, unsigned int PlotCol=65, string parameter ="nsleep", string histoprefix="counterThresholdScannsleep", bool header = true, float fontsize=0.04, float titlesize=0.045, float titleoffsety=1.21, float titleoffsetx=0.87, float titleoffsetz=0.5){
	TF1 *flin = new TF1("flin", "[0]+[1]*x", -1, 85000);
	unsigned int valnum = (valend-valstart)/valstep + 1;
	for (unsigned int i=0; i< Inumvalues; i++){ // Variation of current
    	float Ival = Ioffset + i*Istepsize;
	vector<vector<Double_t>> Drifts( 32 , vector<Double_t> (64)); // 32 config values for 64 pixels in same row.
	TH2D *DAC_Drift = new TH2D("dac_drift", "Voltage drift over DACpol values and column number of pixels", 64, -0.5, 63.5, 32, -0.5, 31.5);

	for (int pol =polstart; pol<2; pol++){
	for (int DAC = DACstart; DAC<DACstop+1; DAC += DACstep){
	//for (unsigned int row=0; row< 64; row++)
	char filename[200] = "";
	sprintf(filename,"%s/Current_nsleepVariation_Row%02d_Ival%.0f_DAC%02dPol%d.root", rootfolder.c_str(),  row, Ival, DAC, pol);
	TFile *f = new TFile(filename);
	for (unsigned int col=0; col< 64; col++){
	  TGraph *gr_Mean = new TGraph(valnum);
	  unsigned int count = 0;
	  for (unsigned int val = valstart; val <= valend; val = val+valstep){
		char histoname[200] = "";
		sprintf(histoname,"%s%04d",histoprefix.c_str(), val);
		TH2D *histo = (TH2D*)f->Get(histoname);
		TH1D *projy = histo->ProjectionY("histo_py", col+1,col+1); // take individual rows
		float mean = projy->GetMean();
		gr_Mean->SetPoint(count, val, mean);
		count++;
	  }
	  flin->SetParameters(1.17, - 0.03/80000);
	  flin->SetLineColor(kOrange+7);
	  gr_Mean->Fit("flin", "Q");
	  
	  if (col== PlotCol and row == PlotRow){
		TCanvas *clinfit = new TCanvas("clinfit", "title", 0, 0, 1100, 600);
		gr_Mean->SetMarkerStyle(5);
		gr_Mean->SetMarkerColor(kBlue+3);
		gr_Mean->SetMarkerSize(3); // default is 1
		char title[200] = "; nsleep (80 MHz clock cycles); Mean of noise peak (V)";
		//sprintf(title,"Voltage drift of pixel in Row %02d, Col %02d; nsleep (80 MHz clock cycles); Mean (V)", PlotRow, PlotCol);
		gr_Mean->SetTitle(title);
		gr_Mean->Draw("AP");
		char model[200] = "f(x) = mx + b  ";
		char mlegend[200] = "";
		sprintf(mlegend,"m = %.2e #pm %.2e V/c.c.", flin->GetParameter(1),flin->GetParError(1));
		char blegend[200] = "";
		sprintf(blegend,"b  =  %.2e #pm %.2e V", flin->GetParameter(0),flin->GetParError(0));
		char Vdriftlegend[200]= "";
		sprintf(Vdriftlegend,"V_{Drift} = %.1f #pm %.1f mV/ms", 80000000*flin->GetParameter(1), 80000000*flin->GetParError(1));
		TLegend *legendfit = new TLegend(0.32,0.6,0.95,0.95);
		legendfit->AddEntry(flin, model);
		legendfit->AddEntry((TObject*)0, mlegend, "");
		legendfit->AddEntry((TObject*)0, blegend, "");
		legendfit->AddEntry((TObject*)0, Vdriftlegend, "");
		legendfit->SetFillStyle(0);
		legendfit->SetBorderSize(0);
		//legendfit->SetTextSize(0.04);
		//legendfit->SetTextFont(52);
		legendfit->Draw();
		gr_Mean->GetXaxis()->SetLabelSize(fontsize);
		gr_Mean->GetYaxis()->SetLabelSize(fontsize);

		gr_Mean->GetXaxis()->SetTitleOffset(titleoffsetx);
		gr_Mean->GetYaxis()->SetTitleOffset(titleoffsety-0.1);

		gr_Mean->GetXaxis()->SetTitleSize(titlesize);
		gr_Mean->GetYaxis()->SetTitleSize(titlesize);

		clinfit->SetRightMargin(0.05);
		clinfit->SetTopMargin(0.05);

		clinfit->SaveAs((pdfprefix+"Voltagedrift_Row"+ to_string(PlotRow)+"_Col" + to_string(PlotCol)+"_DAC" + to_string(DAC) + "_Pol"+to_string(pol)+".pdf").c_str());
	clinfit->Close();
		  }
		  
		  int dacpol  = int((2*pol-1)*(DAC +0.5) +15.5);
		  Drifts[dacpol][col] = 80000000*flin->GetParameter(1); // convert from 80 MHz clock cycle to second
		  DAC_Drift->Fill(col, dacpol, Drifts[dacpol][col]);
		}
		}
		}
		TCanvas *c0 = new TCanvas("c","Graph2D",900, 600);
		DAC_Drift->GetXaxis()->SetTitle("Column");
		DAC_Drift->GetYaxis()->SetTitle("DACpol configuration");
		DAC_Drift->SetZTitle("Voltage drift (mV / ms)");
		c0->SetTopMargin(0.1);
		c0->SetRightMargin(0.38);
		DAC_Drift->Draw("COLZ");
		c0->SaveAs(("plots/"+pdfprefix+"VDrift_Heatmap_DACpol_col.pdf").c_str());
		c0->Close();
		
		// Draw Drifts over dacpol value and also only over dac value
		// Plot mean and std of DAC_Drift y projections.
		  int n = 32;
		  Double_t Mean[n], Sigma[n], gr_DACs[n], eDACs[n];
		  for(int d = 0; d < n; d++){
			Mean[d] = std::accumulate(Drifts[d].begin(), Drifts[d].end(), 0.0)/Drifts[d].size();
			double sigma = 0;
			for(int p = 0; p < 64; p++){sigma+= pow(Drifts[d][p]-Mean[d],2);}
			Sigma[d] = std::sqrt(sigma/Drifts[d].size());
			gr_DACs[d] = d;
			eDACs[d] = 0.0;
		  }

		auto gr = new TGraphErrors(n,gr_DACs,Mean,eDACs,Sigma);
		TCanvas *c_gr = new TCanvas("cgr", "DAC_Drift_Graph", 0, 0, 1000, 600);
		char title[200] = "";
		sprintf(title,";Tuning DAC configuration ; Voltage drift ( mV / ms)"); // Mean of voltage drift for each DAC value
		gr->SetTitle(title);

		gr->SetMarkerStyle(21);
		gr->SetMarkerColor(4);
		gr->GetXaxis()->SetRangeUser(-0.5, n);
		//gr->SetMaximum(-5);
		gr->Draw("AP");
		gr->GetXaxis()->SetLabelSize(fontsize);
		gr->GetYaxis()->SetLabelSize(fontsize);

		gr->GetXaxis()->SetTitleOffset(titleoffsetx);
		gr->GetYaxis()->SetTitleOffset(titleoffsety-0.3);

		gr->GetXaxis()->SetTitleSize(titlesize);
		gr->GetYaxis()->SetTitleSize(titlesize);
		c_gr->SetRightMargin(0.05);
		c_gr->SetTopMargin(0.05);

		c_gr->SaveAs(("plots/"+pdfprefix + "Vdrift_DACconfig_variation.pdf").c_str());
		c_gr->Close();


}
}


// This function reads in a TH2D histogram, produces the y-projections of it and then calculates the difference between adjacent bins.
// This corresponds to numerically differentiating the signal count histograms in order to obtain an energy spectrum.
// variable binning gives the amount of bins that are added together in order to decrease fluctuations between neighboring bins.
// The x-axis can be inverted by passing the parameter invert_Vvalue with a different value than 0.
// The point is to pass the mean of the noise peak and then see a voltage scale where the noise peak is centered around 0 and higher voltages correspond to higher energies.
// V_beforepeak gives the voltage that coresponds to 4 keV og photon energy. This value is used for the measured/ expected DECAL counts comparison
TH1D* Differentiate_spectrum(string rootfile, unsigned int rowmin = 0, unsigned int rowmax = 64, int binning = 3, double xmin = -0.05, double xmax = 0.25, double set_pm_ylim = 200, string target_mat="", int signal_hist_color = 802, string nameofpdf = "", double h1=0, double mu1=0, double sig1=0, double h2=0, double mu2=0, double sig2=0, double invert_Vvalue=0., bool plot_title= true, TH1D *bkgrhist = new TH1D(), double V_beforepeak=0.019591, string bkgr_rootfile = "", string bkgr_histoname ="", double bkgr_scale=1.0, string histprefix="counterThresholdScanRow", double minyerror = 1, float fontsize=0.04, float titlesize=0.045, float titleoffsety=1.21, float titleoffsetx=0.87){
  char rfile[200] = "";
  sprintf(rfile,"%s", rootfile.c_str());
  TFile *f = new TFile(rfile);
  TF1 *f2gaus, *fgaus;
  TH1D *histosum, *bkgr_histosum;
  TCanvas *c = new TCanvas("c1", "yprojections", 0, 0, 900, 600);
  unsigned int count = 0;

  for(int row = rowmin; row < rowmax; row++){ // 64
    cout << "Row: "<< row << endl;
    char histoname[200] = "";
    sprintf(histoname,"%s%02d",histprefix.c_str(),row);
    TH2D *histo = (TH2D*)f->Get(histoname);
    char py_name[20] = "";
    sprintf(py_name,"Row%02d",row);
    if(count == 0)
      {histosum = histo->ProjectionY(py_name, 1, 64);
      count++;}
    else
      {
      histosum->Add(histo->ProjectionY(py_name, 1, 64));
      }
  }

  float V_step = (histosum->GetXaxis()->GetXmax() - histosum->GetXaxis()->GetXmin() )/histosum->GetNbinsX();
  cout << V_step << " " <<histosum->GetNbinsX() << " XMAX: " << histosum->GetXaxis()->GetXmax() << "XMIN: "<<  histosum->GetXaxis()->GetXmin() << endl;
  //cout << histosum->GetBinCenter(0) << " " << histosum->GetBinCenter(1) << " " << histosum->GetBinCenter(2) << " " <<(histosum->GetBinCenter(1)+histosum->GetBinCenter(1+1-1))/2. << endl;
  // define diffhisto with same amount of bins, if Nbins is divisible by binning. If not divisible, take away bins at high energy voltage (which are normally empty anyways)
  TH1D *diffhisto;
  TLegend *legend, *legend2;
  char meanlegend1[200] = "";
  // both means depend on whether x-axis in inverted
  char sigmalegend1[200] = "";
  sprintf(sigmalegend1,"#sigma_{1}   = %.3f V", sig1);
  char heightlegend1[200] = "";
  sprintf(heightlegend1,"h_{1}   = %.1f", h1);
  char meanlegend2[200] = "";
  char sigmalegend2[200] = "";
  sprintf(sigmalegend2,"#sigma_{2}   = %.3f V", sig2);
  char heightlegend2[200] = "";
  sprintf(heightlegend2,"h_{2}   = %.1f", h2);

  if (invert_Vvalue!=0.) {
    diffhisto = new TH1D("differentiated histo", "Differentiated threshold scan", histosum->GetNbinsX()/binning, invert_Vvalue - (histosum->GetXaxis()->GetXmax()-histosum->GetNbinsX()%binning*V_step) , invert_Vvalue - histosum->GetXaxis()->GetXmin());
    diffhisto->SetXTitle("Inverted voltage (V)");
    legend = new TLegend(0.58, 0.65, 0.94,0.89);
    legend2 = new TLegend(0.5 ,0.13,0.94 ,0.45);
    f2gaus = new TF1("f2Gaus","[0]*exp(-0.5*((([6]-x)-[1])/[2])**2) + [3]*exp(-0.5*((([6]-x)-[4])/[5])**2)",invert_Vvalue-1.3, invert_Vvalue-0.7); // gaus(0)= [0]*exp(-0.5*((x-[1])/[2])**2)
    fgaus = new TF1("fGaus","[0]*exp(-0.5*((([3]-x)-[1])/[2])**2)",invert_Vvalue-1.3, invert_Vvalue-0.7);
    fgaus->SetParameters(2*h1/TMath::Sqrt(TMath::Pi())/sig1, mu1, sig1/TMath::Sqrt(2), invert_Vvalue);
    f2gaus->SetParameters(2*h1/TMath::Sqrt(TMath::Pi())/sig1, mu1, sig1/TMath::Sqrt(2), 2*h2/TMath::Sqrt(TMath::Pi())/sig2, mu2, sig2/TMath::Sqrt(2), invert_Vvalue);
    sprintf(meanlegend1,"#mu_{1,S} = %.3f V", invert_Vvalue-mu1);
    sprintf(meanlegend2,"#mu_{2,S} = %.3f V ", invert_Vvalue-mu2);

    // this is to calculate the signal height close to the noise peak from the fit parameters:
    TF1 *fit_2s = new TF1("fFit","[2]*erf((x-[0])/[1])+ [2]+[5] + [5]*erf((x-[3])/[4])", 0, 2); // two s-curves
    fit_2s->SetParameters(mu1, sig1, h1, mu2, sig2, h2);
    cout << "DECAL counts close to noise peak evaluating at V= " << invert_Vvalue-V_beforepeak << " V as C_D = "<< fit_2s->Eval(invert_Vvalue-V_beforepeak) << endl;
    }
  else {
    diffhisto = new TH1D("differentiated histo", "Differentiated threshold scan", histosum->GetNbinsX()/binning,histosum->GetXaxis()->GetXmin() , histosum->GetXaxis()->GetXmax()-histosum->GetNbinsX()%binning*V_step);
    diffhisto->SetXTitle("Threshold voltage (V)");
    legend = new TLegend(0.15,0.77,0.4,0.91);
    legend2 = new TLegend(0.15,0.55,0.4,0.75);
    f2gaus = new TF1("f2Gaus","gaus(0) + gaus(3)",0.7, 1.3); // gaus(0)= [0]*exp(-0.5*((x-[1])/[2])**2)
    fgaus = new TF1("fGaus","gaus(0)",0.7, 1.3);
    fgaus->SetParameters(2*h1/TMath::Sqrt(TMath::Pi())/sig1, mu1, sig1/TMath::Sqrt(2));
    f2gaus->SetParameters(2*h1/TMath::Sqrt(TMath::Pi())/sig1, mu1, sig1/TMath::Sqrt(2), 2*h2/TMath::Sqrt(TMath::Pi())/sig2, mu2, sig2/TMath::Sqrt(2));
    sprintf(meanlegend1,"#mu_{1} = %.3f V", mu1);
    sprintf(meanlegend2,"#mu_{2} = %.3f V ", mu2);
    }

  std::vector<Double_t> binerrors; // store bin errors here.
  std::vector<Double_t> xvals_errors; // and corresponding x_values here

  for(int b = 0; b < histosum->GetNbinsX()-2*binning; b+=binning){ // loop over bins
    double diff_value = histosum->GetBinContent(b+binning) - histosum->GetBinContent(b);
    double diff_error = histosum->GetBinContent(b+binning) + histosum->GetBinContent(b);
    for(int addbin = 1; addbin < binning; addbin++)
      {diff_value += histosum->GetBinContent(b+addbin+binning) - histosum->GetBinContent(b+addbin);
      diff_error  += histosum->GetBinContent(b+addbin+binning) + histosum->GetBinContent(b+addbin);
    }
    binerrors.push_back(sqrt(diff_error)/(V_step*binning*binning));
    // diff_error gives the squared error of the differentiated result, which both still have to be normalized by V_step*binning*binning
    // it is propagated from assuming poisson statistics of the bin counts (error = sqrt of counts)
    // below either inverts the x-axis and sets noise peak voltage to 0 (at invert_Vvalue) or keeps original voltage range of scan
    // Note that keeping the same bin intervals (for binning = 1) means that the differentiation must be asigned to either of the two bins.
    // subtraction of  V_step/2. leads to diff_value being asigned to the lower bin.
    // Also, normalize numerical differentiation by interval range (V_step*binning) and also sampling (binning).
    if (invert_Vvalue!=0.){
      diffhisto->Fill(invert_Vvalue - ((histosum->GetBinCenter(b) + histosum->GetBinCenter(b+2*binning-1))/2. - V_step/2.), diff_value/(V_step*binning*binning));
      xvals_errors.push_back(invert_Vvalue - ((histosum->GetBinCenter(b) + histosum->GetBinCenter(b+2*binning-1))/2. - V_step/2.));
    }
    else { // takes mean of x values from first and last bin entering the differentiation
      diffhisto->Fill((histosum->GetBinCenter(b) + histosum->GetBinCenter(b+2*binning-1))/2. - V_step/2. , diff_value/(V_step*binning*binning));
      xvals_errors.push_back((histosum->GetBinCenter(b) + histosum->GetBinCenter(b+2*binning-1))/2. - V_step/2. );
    }
  }
  cout << diffhisto->GetNbinsX() << " XMAX: " << diffhisto->GetXaxis()->GetXmax() << "XMIN: "<<  diffhisto->GetXaxis()->GetXmin() << endl;

  // now set here the correct error propageted bin errors:
  diffhisto->Sumw2();
  for(int k = 0; k < binerrors.size(); k++){
    diffhisto->SetBinError(diffhisto->GetXaxis()->FindBin(xvals_errors[k]), binerrors[k]);
  }

  diffhisto->SetYTitle("#Delta counts / bin width (1 / V)");
  if (plot_title) diffhisto->SetTitle(("Differentiated spectrum, grouped in bins of " + to_string(binning)).c_str() );
  else diffhisto->SetTitle("");
  if((nameofpdf==""))
    nameofpdf = "2gaus_differentiated";
  TH1D *errordraw = (TH1D*)diffhisto->Clone("stat. unc."); // to plot the error of the hist
  diffhisto->SetLineColorAlpha(signal_hist_color, 0.0001); // basically transparent
  diffhisto->SetFillColorAlpha(signal_hist_color, 0.5);
  c->SetLeftMargin(0.11);
  diffhisto->Draw("hist");

  errordraw->SetFillColor(kGray+1);
  errordraw->SetLineColor(0);
  errordraw->SetFillStyle(3135);
  errordraw->Draw("E2 same");

  diffhisto->GetYaxis()->SetMaxDigits(4);
  char difflabel[200] = "";
  sprintf(difflabel,"Differentiated %s data", target_mat.c_str());

  legend->AddEntry(diffhisto, difflabel);
  legend->AddEntry(errordraw, "stat. unc.");

  // plot sum of gaus from s-curve fit to spectrum only when height is specified
  if (h1 > 0){

    if (h2 > 0){ // plot second gauss if specified
      f2gaus->Draw("same");
      char model[200] = "f(x)";
      if (invert_Vvalue!=0.) sprintf(model," 2h_{1} N(#mu_{1,S},#frac{#sigma_{1}^{2}}{2})+ 2h_{2} N(#mu_{2,S},#frac{#sigma_{2}^{2}}{2})");
      else sprintf(model," 2h_{1} N(#mu_{1},#frac{#sigma_{1}^{2}}{2})+ 2h_{2} N(#mu_{2},#frac{#sigma_{2}^{2}}{2})");
      legend->AddEntry(f2gaus, model);
    }
    else {
      fgaus->Draw("same");
      char model[200] = "f(x)";
      if (invert_Vvalue!=0.) sprintf(model," 2h_{1} N(#mu_{1, S},#frac{#sigma_{1}^{2}}{2})");
      else sprintf(model," 2h_{1} N(#mu_{1},#frac{#sigma_{1}^{2}}{2})");
      legend->AddEntry(fgaus, model);
    }
    //legend->SetTextSize(0.025);
    legend2->SetNColumns(2);

    //legend2->SetTextSize(0.03);
    if (h2 > 0){
      legend2->AddEntry((TObject*)0, heightlegend1, "");
      legend2->AddEntry((TObject*)0, heightlegend2, "");
      legend2->AddEntry((TObject*)0, meanlegend1, "");
      legend2->AddEntry((TObject*)0, meanlegend2, "");
      legend2->AddEntry((TObject*)0, sigmalegend1, "");
      legend2->AddEntry((TObject*)0, sigmalegend2, "");

    }
    else {
      legend2->AddEntry((TObject*)0, heightlegend1, "");
      legend2->AddEntry((TObject*)0, meanlegend1, "");
      legend2->AddEntry((TObject*)0, sigmalegend1, "");
    }
  }

  // plots bkgr histo if specified
  if (bkgrhist->GetEntries()>0) {
    bkgrhist->SetLineColorAlpha(kBlue, 0.00001); // basically invisible
    bkgrhist->SetLineColor(kBlue);
    //bkgrhist->SetLineStyle(3);
    bkgrhist->SetFillColor(0);
    //bkgrhist->SetFillColorAlpha(kBlue, 0.2);
    //bkgrhist->SetFillColor(kBlue);
    //bkgrhist->SetMarkerStyle(20);
    bkgrhist->Draw("hist same");
    legend->AddEntry(bkgrhist,"Background");
    }

  diffhisto->SetStats(0);
  legend->SetBorderSize(0);
  legend->SetFillStyle(0);
  legend2->SetBorderSize(0);
  legend2->SetFillStyle(0);
  legend->Draw();
  legend2->Draw();

  TLatex *text = new TLatex();
  text->DrawLatexNDC(0.46, 0.86,"DECAL");
  text->Draw();

  diffhisto->GetXaxis()->SetLabelSize(fontsize);
  diffhisto->GetYaxis()->SetLabelSize(fontsize);

  diffhisto->GetXaxis()->SetTitleOffset(titleoffsetx);
  diffhisto->GetYaxis()->SetTitleOffset(titleoffsety-0.2);

  diffhisto->GetXaxis()->SetTitleSize(titlesize);
  diffhisto->GetYaxis()->SetTitleSize(titlesize);
  if (invert_Vvalue!=0.) diffhisto->GetXaxis()->SetRangeUser(xmin, xmax);

  c->SetRightMargin(0.05);

  c->SaveAs((nameofpdf + "binning_" +to_string(binning) + ".pdf").c_str());
  c->SaveAs((nameofpdf + "binning_" +to_string(binning) + ".root").c_str());
  c->SetLogy();
  c->SaveAs((nameofpdf + "binning_" +to_string(binning) + "_logy.pdf").c_str());
  c->SaveAs((nameofpdf + "binning_" +to_string(binning) + "_logy.root").c_str());
  c->SetLogy(0); // reset to lin scale
  diffhisto->SetMaximum(set_pm_ylim); // sets ymax for another linear plot that shows signal counts next to noise peak
  diffhisto->SetMinimum(-set_pm_ylim);
  c->SaveAs((nameofpdf + "binning_" +to_string(binning) + "_smallyrange.pdf").c_str());
  c->SaveAs((nameofpdf + "binning_" +to_string(binning) + "_smallyrange.root").c_str());
  c->Close();

  return diffhisto;
}


// analyse rootfiles in rootpath + "repetition_number".root from counterThresholdScanMultipleRows(), that rescans the same row many times.
// for plotting over time rather than the repetition number the total amount of time taken for all Nrepetitions measurements has to be specified.
// The max and min for the voltage y-axis are taken automatically but for the temperature yaxis TMax and TMin have to be specified manually.
void cTScan_RepeatRow(string rootpath, unsigned int row, unsigned int col1, unsigned int col2, unsigned int Nrepetitions, float total_seconds=0., unsigned int toffset_seconds=0, string tempdata="", float TMax=1.0, float TMin=1.0, string histoprefix="counterThresholdScanRow", string variable="N_repetitions", string nameofpdf="Variation", bool header = true, float fontsize=0.04, float titlesize=0.045, float titleoffsety=1.3){
  float tscale = 1.0;
  if (!(tempdata=="")) tscale = total_seconds/Nrepetitions;
  TF1 *fitexp = new TF1("fitexp","[1]*exp(x/[0]) + [2]",0, Nrepetitions*tscale);
  TF1 *fitexp2 = new TF1("fitexp2","-[1]*exp(x/[0]) + [2]",0, Nrepetitions*tscale);
  fitexp->SetParameters(-300.*tscale, 0.0015, 1.166);
  fitexp2->SetParameters(-300.*tscale, 0.0015, 1.17);
  fitexp->SetParLimits(0, -600.*tscale, -200.*tscale);
  fitexp2->SetParLimits(0, -600.*tscale, -200.*tscale);
  //fitexp->SetParLimits(1,0.0001, 0.1);
  //fitexp->SetParLimits(2,1.0, 1.5);
  TGraph *gr_Mean_sum = new TGraph(Nrepetitions);
  TGraph *gr_Std_sum = new TGraph(Nrepetitions);
  TGraph *gr_Maximum_sum = new TGraph(Nrepetitions);
  TGraph *gr_Integral_sum = new TGraph(Nrepetitions);
  TGraph *gr_Mean_col1 = new TGraph(Nrepetitions);
  TGraph *gr_Mean_col2 = new TGraph(Nrepetitions);
  TGraph *gr_T = new TGraph();
  TMultiGraph *mg = new TMultiGraph();
  char histoname[200] = "";
  sprintf(histoname,"%s%02d",histoprefix.c_str(), row); // same name for all histos
  for (unsigned int nrep = 0; nrep <= Nrepetitions; nrep++){
    char rfile[200] = "";
    sprintf(rfile,"%s_%02d.root", rootpath.c_str(), nrep);
    TFile *f = new TFile(rfile);
    TH2D *histo = (TH2D*)f->Get(histoname);
    TH1D *projy_col1 = histo->ProjectionY("histo_py_c1", col1, col1+1); // sum all strips together
    TH1D *projy_col2 = histo->ProjectionY("histo_py_c2", col2, col2+1); // sum all strips together
    TH1D *projy_sum = histo->ProjectionY("histo_py_sum", 1, 64); // sum all strips together
    gr_Std_sum->SetPoint(nrep, nrep*tscale, projy_sum->GetStdDev());
    gr_Mean_sum->SetPoint(nrep, nrep*tscale, projy_sum->GetMean());
    gr_Maximum_sum->SetPoint(nrep, nrep*tscale, projy_sum->GetMaximum());
    gr_Integral_sum->SetPoint(nrep, nrep*tscale, projy_sum->Integral());
    gr_Mean_col1->SetPoint(nrep, nrep*tscale, projy_col1->GetMean());
    gr_Mean_col2->SetPoint(nrep, nrep*tscale, projy_col2->GetMean());
    //count++;
  }
  gr_Mean_sum->Fit("fitexp","R");
  gr_Mean_sum->SetMarkerColor(kAzure);
  gr_Mean_sum->SetLineColor(kWhite);
  gr_Mean_sum->SetMarkerStyle(5);
  gr_Mean_sum->SetMarkerSize(0.7);
  gr_Mean_col1->SetMarkerColor(kCyan+3);
  gr_Mean_col1->SetLineColor(kWhite);
  gr_Mean_col1->SetMarkerStyle(5);
  gr_Mean_col1->SetMarkerSize(0.7);
  gr_Mean_col2->SetMarkerColor(kRed);
  gr_Mean_col2->SetLineColor(kWhite);
  gr_Mean_col2->SetMarkerStyle(5);
  gr_Mean_col2->SetMarkerSize(0.7);
  mg->Add(gr_Mean_sum);
  mg->Add(gr_Mean_col1);
  mg->Add(gr_Mean_col2);
  TCanvas *c = new TCanvas("c", "means_warmup", 0, 0, 900, 600);
  c->SetLeftMargin(0.12);
  c->SetTopMargin(0.05);
  //mg->SetTitle("Repetition of row-wise threshold scan;Repetitions;Mean (V)");
  mg->SetTitle(";time (s);Mean of noise peak (V)");
  mg->Draw("AP");
  mg->GetXaxis()->SetLabelSize(fontsize);
  mg->GetXaxis()->SetTitleSize(titlesize);
  mg->GetYaxis()->SetLabelSize(fontsize);
  mg->GetYaxis()->SetTitleSize(titlesize);
  mg->GetYaxis()->SetTitleOffset(titleoffsety);

  if (!(tempdata=="")){
    //mg->SetTitle("Repetition of row-wise threshold scan;time (s);Mean (V)");
    //mg->SetTitle("this;time (s);Mean of noise peak (V)");
    mg->GetXaxis()->SetRangeUser(0, total_seconds-1);
    std::ifstream infile(tempdata);
    std::string line;
    if(header)
      getline(infile, line); // skips header line
    int sec;
    float tmax, tmin, tave; // max min and average temperature
    std::vector<Double_t> seconds, Tmax, Tmin, Tave;
    //int count = 0;
    while(getline(infile, line)) {
      std::istringstream iss(line);
      iss >> sec >> tmax >> tmin >> tave;
      seconds.push_back(sec-int(toffset_seconds)); // for plotting, start timing t=0 at toffset
      Tmax.push_back((tmax-TMin)*(gPad->GetUymax()-gPad->GetUymin())/(TMax-TMin)+gPad->GetUymin());
      Tmin.push_back((tmin-TMin)*(gPad->GetUymax()-gPad->GetUymin())/(TMax-TMin)+gPad->GetUymin());
      Tave.push_back((tave-TMin)*(gPad->GetUymax()-gPad->GetUymin())/(TMax-TMin)+gPad->GetUymin());
      //gr_Tmax->SetPoint(count, sec-int(toffset_seconds), tmax*.....); // equivalent to filling below.
      //count++;
    }
    gr_T = new TGraph(seconds.size(), &(seconds[0]), &(Tmax[0])); // or fill it instead above in loop
    gr_T->SetMarkerStyle(21);
    gr_T->SetLineColor(kWhite);
    gr_T->SetMarkerColor(kOrange+2);


    //mg->Add(gr_Tmax);
  }
  TLegend *legend = new TLegend(0.23,0.75,0.85,0.95);
  legend->SetNColumns(2);
  legend->AddEntry(gr_Mean_col1, ("Pixel " + to_string(col1)+" in row").c_str());
  char model[200] = "V(x) = V_{0} + #DeltaV #times exp(x/#tau)";
  char taulegend[200] = "";
  sprintf(taulegend,"#tau = %.3f #pm %.2e s", fitexp->GetParameter(0),fitexp->GetParError(0));
  char dVlegend[200] = "";
  sprintf(dVlegend,"#DeltaV = %.3f #pm %.3f mV", fitexp->GetParameter(1)*1000,fitexp->GetParError(1)*1000);
  char V0legend[200] = "";
  sprintf(V0legend,"V_{0} = %.5f #pm %.2e V", fitexp->GetParameter(2),fitexp->GetParError(2));

  legend->AddEntry(fitexp, model);
  legend->AddEntry(gr_Mean_col2, ("Pixel " + to_string(col2)+" in row").c_str());
  legend->AddEntry((TObject*)0, V0legend, "");
  legend->AddEntry(gr_Mean_sum, "Average in row");
  legend->AddEntry((TObject*)0, taulegend, "");
  legend->SetFillStyle(0);
  legend->SetBorderSize(0);

  if (!(tempdata=="")){
    //mg->GetYaxis()->SetRangeUser(VMin, VMax);
    legend->AddEntry(gr_T, "Temperature");
    fitexp2->SetLineColor(kOrange+2);
    //gr_T->Fit(fitexp2,"R");
    gr_T->Draw("p same");

    cout << gPad->GetUxmin() << " " << gPad->GetUxmax() << " "<< gPad->GetUymin() << " "<< gPad->GetUymax() << endl;
    //TGaxis *axis = new TGaxis(gPad->GetUxmax(),gPad->GetUymin(), gPad->GetUxmax(), gPad->GetUymax(),0,rightmax,510,"+L");
    //TGaxis *axis = new TGaxis(total_seconds,VMin, total_seconds, VMax,TMin,TMax,510,"+L");
    TGaxis *axis = new TGaxis(total_seconds,gPad->GetUymin(),total_seconds, gPad->GetUymax(),TMin,TMax,510,"+L");
    axis->SetLineColor(kOrange+2);
    axis->SetLabelColor(kOrange+2);
    axis->SetTitleColor(kOrange+2);
    axis->SetLabelSize(fontsize);
    axis->SetTitleSize(titlesize);
    axis->SetTitle("Temperature (#circ C)");
    axis->Draw();
  }
  legend->AddEntry((TObject*)0, dVlegend, "");

  /*

  projyALL->SetFillColor(kOrange +2);
  projyALL->SetLineColor(kRed);
  projyALL
  projyALL->Draw("HIST SAME");
  projy->SetFillColorAlpha(kBlue, 0.6);
  projy->Draw("HIST SAME"); // To show in foreground

  TLegend *legend = new TLegend(0.7,0.8,0.89,0.89);
  legend->AddEntry(projy, col_label.c_str());
  legend->AddEntry(projyALL,"All Columns");
  legend->Draw();
  //draw an axis on the right side

  */
  legend->Draw();

  c->SaveAs("Means_drifting_nrepeat.pdf");
  c->SaveAs("Means_drifting_nrepeat.root");
  c->Close();
  create_four_variablePlots(gr_Std_sum, gr_Mean_sum, gr_Maximum_sum, gr_Integral_sum, variable, variable, nameofpdf);
}


double interpolateme( double xvalue, std::vector<Double_t> binned_xvalues, std::vector<Double_t> v_variable ){
  unsigned int bin = 0;
  // go to next value while xvalue is larger than previous one and bin hasnt reached the end yet.
  while(xvalue > binned_xvalues[bin] and bin < binned_xvalues.size()-1) bin++;
  if (bin==0 or bin == binned_xvalues.size()-1) return(v_variable[bin]); // if at start/ end or outside of data points take first or last value
  else return(v_variable[bin-1] +  (xvalue-binned_xvalues[bin-1])/(binned_xvalues[bin] - binned_xvalues[bin-1]) *(v_variable[bin]-v_variable[bin-1]));
}

double Rescale(const TGraph* gr){
  double gr_Integral = 0;
  for (int i=0;i<gr->GetN();i++) {
    gr_Integral+= gr->GetY()[i];
    cout << gr->GetY()[i] << endl;
  }
  return gr_Integral;
}

double Rescale2(const TGraph* gr, double scalefactor = 1){
  double gr_Integral = gr->Integral();
  return gr_Integral;
}

double Integrate_vector(vector<double> vec){
  double vec_integral=0;
  for (int i=0;i<vec.size();i++) vec_integral += vec[i];
  return vec_integral;
}

vector<double> Normalize_vector(vector<double> vec, double integral){
  for (int i=0;i<vec.size();i++) vec[i] /=integral;
return vec;
}


// correct an energy spectrum read in from E_counts with the absorption probability in silicon of a defined thickness in um.
// The attenuation coefficients are downloaded from NIST and stored in mu_file
TGraph* Correct_Energy_spectrum(vector<string> E_counts_csv, vector<string> leg_labels, vector<int> colors, string mu_filetxt, bool calibrate_E=true, double par_m = 0., double par_b = 0., double xmax = 0., double xmin = 0., string nameofpdf= "plots/energyspectrum.pdf", float Si_thick_um=25, bool separatcanvas = true, bool header = true, float fontsize=0.04, float titlesize=0.045, float titleoffsety=1.1, float titleoffsetx=0.87){
  TCanvas *c = new TCanvas("c", "energy spectrum", 0, 0, 900, 600);
  TCanvas *c_corr = new TCanvas("c_corr", "corrected energy spectrum", 0, 0, 900, 600);
  TLegend *legend1, *legend2;
  TGraph *gr_Ecorr;
  ifstream infile2(mu_filetxt);
  string line;
  if(header)
    getline(infile2, line); // skips header line
  std::vector<double> v_energy2;
  std::vector<double> v_mu;
  float energy2, mu, mu_en;
  double rho_Si = 2.329; //   in unit g/cm^3
  while(getline(infile2, line)) {
    std::istringstream iss(line);
    iss >> energy2 >> mu >> mu_en;
    //cout << energy2 << " " << mu << " " << mu_en << endl;
    v_energy2.push_back(energy2); // approximate energy calibration
    v_mu.push_back(mu);
  }
  infile2.close();
  // change v_mu from attenuation coefficient to absorption probability
  //and take its initial unit into account
  for(unsigned int i=0; i<v_mu.size(); i++) v_mu[i] = 1 - exp(-v_mu[i]*0.001*Si_thick_um*0.0001*rho_Si); // thickness here converted to cm

  for (unsigned int s= 0; s<E_counts_csv.size(); s++){
    ifstream infile1(E_counts_csv[s]);
    //if(header)
    //  getline(infile1, line); // skips header line. but there is none in hexitec files
    double energy, counts, count_integral, count_integral_corr;
    std::vector<double> v_energy;
    std::vector<double> v_counts;
    std::vector<double> v_counts_corr;

    while(getline(infile1, line)) {
      std::istringstream iss(line);
      iss >> energy >> counts; // somehow read in order is inverted here.
      //cout << energy<< " " <<0.028*energy +2 << " " << counts << endl;

      if(calibrate_E) energy = par_m*energy +par_b; //0.028*energy +2;
      v_energy.push_back(energy); // approximate energy calibration
      v_counts.push_back(counts);
    }
    infile1.close();
    count_integral = Integrate_vector(v_counts);
    v_counts = Normalize_vector(v_counts,count_integral);
    TGraph *gr_E = new TGraph(v_energy.size(), &(v_energy[0]), &(v_counts[0]));

    for(unsigned int i=0; i<v_energy.size(); i++) v_counts_corr.push_back(v_counts[i]*interpolateme(v_energy[i], v_energy2, v_mu ));
    count_integral_corr = Integrate_vector(v_counts_corr);
    v_counts_corr = Normalize_vector(v_counts_corr,count_integral_corr);
    gr_Ecorr = new TGraph(v_energy.size(), &(v_energy[0]), &(v_counts_corr[0]));

    // plotting
    gr_E->SetLineColor(colors[s]);
    gr_Ecorr->SetLineColor(colors[s]);
    gr_E->SetLineWidth(3);
    gr_Ecorr->SetLineWidth(3);
    //gr_Ecorr->SetLineStyle(2);
    if(xmax>0. or xmin >0.)
      {gr_E->GetXaxis()->SetRangeUser(xmin, xmax);
      gr_Ecorr->GetXaxis()->SetRangeUser(xmin, xmax);
      }
    c->cd();
    if (s==0){
      gr_E->SetTitle(";Energy (keV);Normalized counts");
      gr_E->GetXaxis()->SetLabelSize(fontsize);
      gr_E->GetYaxis()->SetLabelSize(fontsize);

      gr_E->GetXaxis()->SetTitleOffset(titleoffsetx);
      gr_E->GetYaxis()->SetTitleOffset(titleoffsety);

      gr_E->GetXaxis()->SetTitleSize(titlesize);
      gr_E->GetYaxis()->SetTitleSize(titlesize);
      if(!calibrate_E) gr_E->GetXaxis()->SetTitle("Bin");
      gr_E->Draw();
      if(separatcanvas) {
        c_corr->cd();
        gr_Ecorr->SetTitle(";Energy (keV);Normalized counts");
        gr_Ecorr->GetXaxis()->SetLabelSize(fontsize);
        gr_Ecorr->GetYaxis()->SetLabelSize(fontsize);

        gr_Ecorr->GetXaxis()->SetTitleOffset(titleoffsetx);
        gr_Ecorr->GetYaxis()->SetTitleOffset(titleoffsety);

        gr_Ecorr->GetXaxis()->SetTitleSize(titlesize);
        gr_Ecorr->GetYaxis()->SetTitleSize(titlesize);

        if(!calibrate_E) gr_Ecorr->GetXaxis()->SetTitle("Bin");
        gr_Ecorr->Draw();
        }
      else {
        gr_Ecorr->SetLineStyle(7); // plot dashed if in same canvas
        gr_Ecorr->Draw("same");
        }

      legend1 = new TLegend(0.4,0.72,0.94,0.94);
      legend2 = new TLegend(0.4,0.72,0.94,0.94);
      legend1->SetNColumns(2);
      legend2->SetNColumns(2);
      }
    else {gr_E->Draw("same");
      if(separatcanvas) c_corr->cd();
      else gr_Ecorr->SetLineStyle(7); // plot dashed if in same canvas
      gr_Ecorr->Draw("same");
    }
    char legentry1[200] = "";
    sprintf(legentry1,"%s [%.3e]",leg_labels[s].c_str(), count_integral );
    //if (s==0) sprintf(legentry1,"%s [1.975 #times 10^{8}]",leg_labels[s].c_str());
    //if (s==1) sprintf(legentry1,"%s [7.455 #times 10^{7}]",leg_labels[s].c_str() );
    legend1->AddEntry(gr_E, legentry1);
    char legentry2[200] = "";
    sprintf(legentry2,"%s corr [%.3e]",leg_labels[s].c_str(), count_integral*count_integral_corr );
    //if (s==0) sprintf(legentry2,"%s corr [7.237 #times 10^{7}]",leg_labels[s].c_str());
    //if (s==1) sprintf(legentry2,"%s corr [3.816 #times 10^{6}]",leg_labels[s].c_str());
    if(separatcanvas) legend2->AddEntry(gr_Ecorr, legentry2);
    else legend1->AddEntry(gr_Ecorr, legentry2);
  }
  legend1->SetBorderSize(0);
  legend2->SetBorderSize(0);

  c->SetRightMargin(0.05);
  c->SetTopMargin(0.05);
  if(separatcanvas) legend2->Draw();
  c->cd();
  legend1->Draw(); // draw in both canvases
  c->SaveAs((nameofpdf+".pdf").c_str());
  c->Close();
  if(separatcanvas) {
    c_corr->SetRightMargin(0.05);
    c_corr->SetTopMargin(0.05);
    c_corr->SaveAs((nameofpdf+"_corrected.pdf").c_str());
    c_corr->Close();
    }
  return gr_Ecorr;

}

// error3rdcol = "x" or "y" reads in another column from the txtfile and understands it as the corresponding error on x or y.
// if not specified, xerror and yerror are taken as constant for each data point
void energy_calibration(string E_calibtxt, string nameofpdf="Energycalibration_HEXITEC", string plot_title = "Energy calibration", string xtitle = "Bin",
string ytitle = "Energy (keV)", string m_unit = "keV/Bin", string b_unit = "keV" , double xmin = -1, double ymin=-1, string error3rdcol = "", double xerror = 10, double yerror = 0.3, double x1leg = 0.11, double y1leg = 0.65, double x2leg = 0.85, double y2leg = 0.97, double par1 = 1.103, double par2 = 0.02782, bool header = true, float fontsize=0.04, float titlesize=0.045, float titleoffsety=1., float titleoffsetx=0.87){
  TF1 *flin = new TF1("flin", "[0]+[1]*x", -1, 4001);
  TCanvas *c = new TCanvas("c", "energy calibration HEXITEC", 0, 0, 900, 600);
  ifstream infile(E_calibtxt);
  string line;
  if(header)
    getline(infile, line); // skips header line
  std::vector<double> v_ydata;
  std::vector<double> v_xdata;
  std::vector<double> e_v_ydata;
  std::vector<double> e_v_xdata;
  double ydata;
  double xdata;
  double xory_error;
  string comment;
  while(getline(infile, line)) {
    std::istringstream iss(line);
    if (error3rdcol=="") iss >> xdata >> ydata >> comment;
    else iss >> xdata >> ydata >> xory_error >> comment;
    cout << comment << " " << ydata << " " << xdata << endl;
    v_ydata.push_back(ydata);
    v_xdata.push_back(xdata);
    if (error3rdcol=="x") e_v_xdata.push_back(xory_error);
    else e_v_xdata.push_back(xerror);
    if (error3rdcol=="y") e_v_ydata.push_back(xory_error);
    if (error3rdcol=="yoversqrt2") e_v_ydata.push_back(xory_error/sqrt(2.));
    else e_v_ydata.push_back(yerror);
  }
  infile.close();

//TGraph *gr_Ecalib = new TGraph(v_ydata.size(), &(v_xdata[0]), &(v_ydata[0]));
auto gr_Ecalib = new TGraphErrors(v_ydata.size(),&(v_xdata[0]),&(v_ydata[0]),&(e_v_xdata[0]),&(e_v_ydata[0]));
//flin->SetParameters(2.0, 0.0028); // more or less calib
flin->SetParameters(par1, par2);
flin->SetLineColor(kOrange+7);
gr_Ecalib->Fit("flin");
char title[200] = "";
sprintf(title,"%s ; %s; %s", plot_title.c_str(), xtitle.c_str(), ytitle.c_str());
gr_Ecalib->SetTitle(title);
gr_Ecalib->SetMarkerStyle(21);
//gr->SetMarkerStyle(21);
gr_Ecalib->SetMarkerColor(kBlue+3);
gr_Ecalib->SetMarkerSize(1); // default is 1

gr_Ecalib->GetXaxis()->SetLabelSize(fontsize);
gr_Ecalib->GetYaxis()->SetLabelSize(fontsize);

gr_Ecalib->GetXaxis()->SetTitleOffset(titleoffsetx);
gr_Ecalib->GetYaxis()->SetTitleOffset(titleoffsety);

gr_Ecalib->GetXaxis()->SetTitleSize(titlesize);
gr_Ecalib->GetYaxis()->SetTitleSize(titlesize);
flin->Draw();
gr_Ecalib->Draw("AP");
if (ymin != -1) gr_Ecalib->SetMinimum(ymin);
c->Update(); // necessary to get correct gPad-> xmin and xmax
if (xmin != -1) gr_Ecalib->GetXaxis()->SetRangeUser(xmin, gPad->GetUxmax());
//cout << "xmin: " << gPad->GetUxmin()<< "xmax: " << gPad->GetUxmax() << endl;
char model[200] = "f(x) = mx + b  ";
char mlegend[200] = "";
sprintf(mlegend,"m = %.3e #pm %.2e %s", flin->GetParameter(1),flin->GetParError(1), m_unit.c_str());
char blegend[200] = "";
sprintf(blegend,"b = %.3e #pm %.2e %s", flin->GetParameter(0),flin->GetParError(0), b_unit.c_str());
TLegend *legend = new TLegend(x1leg, y1leg, x2leg, y2leg);
legend->AddEntry((TObject*)0, mlegend, "");
legend->AddEntry((TObject*)0, blegend, "");
legend->AddEntry(flin, model);
legend->SetBorderSize(0);
legend->SetFillStyle(0);
legend->Draw();

c->SetRightMargin(0.05);
c->SetTopMargin(0.05);
c->SaveAs((nameofpdf+".pdf").c_str());
}

void plot_absorp_prob(string mu_filetxt, string plot_title= "photon absorption probability in 25 #mum silicon", string nameofpdf="plots/absorp_prob_Silicon", float Si_thick_um=25, float xmax = 37, bool header=true, float fontsize=0.04, float titlesize=0.045, float titleoffsety=0.9, float titleoffsetx=0.87){
  ifstream infile(mu_filetxt);
  string line;
  if(header)
    getline(infile, line); // skips header line
  std::vector<double> v_energy;
  std::vector<double> v_mu;
  float energy, mu, mu_en;
  double rho_Si = 2.329; //   in unit g/cm^3
  while(getline(infile, line)) {
    std::istringstream iss(line);
    iss >> energy >> mu >> mu_en;
    //cout << energy << " " << mu << " " << mu_en << endl;
    v_energy.push_back(energy); // approximate energy calibration
    v_mu.push_back(mu);
  }
  infile.close();
  // change v_mu from attenuation coefficient to absorption probability
  // and take its initial unit into account
  for(unsigned int i=0; i<v_mu.size(); i++) {
    v_mu[i] = 1 - exp(-v_mu[i]*0.001*Si_thick_um*0.0001*rho_Si); // thickness here converted to cm
    cout << v_energy[i] << " " << v_mu[i] << endl;
    }
  TCanvas *c = new TCanvas("c", "absorption prob in Silicon", 0, 0, 900, 600);
  TGraph *gr_absorp = new TGraph(v_energy.size(), &(v_energy[0]), &(v_mu[0]));
  gr_absorp->GetXaxis()->SetRangeUser(0, xmax);
  char title[200] = "";
  sprintf(title,"%s;Photon energy (keV); Absorption probability", plot_title.c_str());
  gr_absorp->SetTitle(title);
  gr_absorp->Draw();

  gr_absorp->GetXaxis()->SetLabelSize(fontsize);
  gr_absorp->GetYaxis()->SetLabelSize(fontsize);

  gr_absorp->GetXaxis()->SetTitleOffset(titleoffsetx);
  gr_absorp->GetYaxis()->SetTitleOffset(titleoffsety);

  gr_absorp->GetXaxis()->SetTitleSize(titlesize);
  gr_absorp->GetYaxis()->SetTitleSize(titlesize);

  c->SetRightMargin(0.05);
  c->SetTopMargin(0.05);
  TLegend *legend = new TLegend(0.5,0.8,0.94,0.94);
  char grlegend[200] = "";
  sprintf(grlegend,"in %.0f #mum silicon", Si_thick_um);
  legend->AddEntry(gr_absorp, grlegend);
  legend->SetBorderSize(0);
  legend->Draw();

  c->SaveAs((nameofpdf+".pdf").c_str());
  c->SetLogy();
  c->SaveAs((nameofpdf+"_logy.pdf").c_str());
}


/**
 * \brief A gaussian filter on a Tgraph.
 * \param gr The Tgraph to be smeared. It is modified, so copy first the original one if you do not want to lose it!
 * \param sX the sigma of the x-Gaussian
 */

TGraph* Gaus1DFilter_Gr(TGraph* gr, const Double_t sX, UInt_t kmin=0)
{
	if(!gr) cout << " No appropriate graph passed in function" << endl;

	TGraph* gr2 = (TGraph*)gr->Clone(); // so that original graph remains unchanged
	//h->Reset();
	double content;
	double x, w, z;
  double point_dist = gr->GetPointX(2)- gr->GetPointX(1);
  //cout << "Point Distance: " << point_dist << endl;
	const UInt_t nX = gr->GetN();
	//UInt_t kmin, kmax;
  UInt_t kmax = nX-1;

	for(UInt_t i=0;i<nX;++i)
	{
		x = gr->GetPointX(i);

		content = 0;

		//For bin i, we look in the old histogram at all bins in a nearby region and calculate the probability of being smeared in the current one

		//kmin = h2->GetXaxis()->FindBin(x-3*sX);//For being faster, we reduce the range to 3-sigma. Use kmin = 1 if you want the whole range
		//kmax = h2->GetXaxis()->FindBin(x+3*sX);//For being faster, we reduce the range to 3-sigma. Use kmax = nX if you want the whole range

		for(UInt_t k=kmin;k<=kmax;++k)
		{
			w = gr->GetPointX(k);
      //h2->GetXaxis()->GetBinCenter(k);//We approximate the integral of the gaussian function between binLowEdge and binUpEdge by the integrand times the bin width. This bin width and the normalization is divided after the end of the loop. The Error Function could be used instead.
			content+=gr->GetPointY(k)*TMath::Gaus(x,w,sX,true);
		}
		//content=content*h->GetXaxis()->GetBinWidth(i);
		//gr2->SetBinContent(i,content);

    //cout << i << " " << x << " " << content*point_dist << endl;
    gr2->SetPoint(i, x, content*point_dist);
	}
	// keep same normalization than before smearing
	double constant = gr->Integral() / gr2->Integral();
  //cout << " sigE: " << sX << " this should stay same: " << gr->Integral()  << " "<< gr->GetPointX(50) << " "<< gr2->Integral() << endl;
	for (int i=0;i<gr2->GetN();i++) gr2->GetY()[i] *= constant;
  return gr2;
}

void GausSmearGraph(double sigX = 1, string E_counts_csv="Messdaten/2207_RAL/HEXITEC_all/reprocessed/Cd_35kV_1_5mA_CSM_Discrimination_3x3grid_total.csv", bool calibrate_E=true, double par_m = 0.02782, double par_b = 1.103, double xmax = 50){
  TCanvas *c_Smear = new TCanvas("cHEX_Smear", "Gaus smear spectrum", 0, 0, 900, 600);
  ifstream infile1(E_counts_csv);
  string line;
  //if(header)
  //  getline(infile1, line); // skips header line. but there is none in hexitec files
  double energy, counts, count_integral, count_integral_corr;
  std::vector<double> v_energy;
  std::vector<double> v_counts;
  std::vector<double> v_counts_corr;

  while(getline(infile1, line)) {
    std::istringstream iss(line);
    iss  >> energy >> counts; // somehow read in order is inverted here.
    //cout << energy<< " " <<0.028*energy +2 << " " << counts << endl;

    if(calibrate_E) energy = par_m*energy +par_b; //0.028*energy +2;
    v_energy.push_back(energy); // approximate energy calibration
    v_counts.push_back(counts);
    //cout << energy << " " << counts << endl;
  }
  infile1.close();
  //count_integral = Integrate_vector(v_counts);
  //v_counts = Normalize_vector(v_counts,count_integral);

  TGraph *gr_E = new TGraph(v_energy.size(), &(v_energy[0]), &(v_counts[0]));
	cout << "Original counts: " << gr_E->Integral() << endl; // original counts
  gr_E = Gaus1DFilter_Gr(gr_E, sigX); // smear it.
  gr_E->Draw();
	gr_E->GetXaxis()->SetRangeUser(0,xmax);
	cout << "Smeared Counts: " << gr_E->Integral() << endl;
}

// This fct should create a spline to the HEXITEC energy spectrum and then fit it to the differentiated decal data.
void Spline_spectrum(string E_counts_csv, TH1D *diffh, string material="Cu", bool fit_E_corr=true, double sig_E_start = 0., double yscale=1., double xfitmin = 0.027, double xfitmax = 0.1, double xplotmin=-0.05, int mat_color=803, string nameofpdf="test", bool gaussmear=true, bool fix_par2=true, bool calibrate_E=true, double par_m = 0.02782, double par_b = 1.103){
  ifstream infile1(E_counts_csv);
  string line;
  //if(header)
  //  getline(infile1, line); // skips header line. but there is none in hexitec files
  double energy, counts, count_integral, count_integral_corr;
  std::vector<double> v_energy;
  std::vector<double> v_counts;
  std::vector<double> v_counts_corr;

  while(getline(infile1, line)) {
    std::istringstream iss(line);
    iss  >> energy >> counts; // somehow read in order is inverted here.
    //cout << energy<< " " <<0.028*energy +2 << " " << counts << endl;
    if(calibrate_E) energy = par_m*energy +par_b; //0.028*energy +2;
    v_energy.push_back(energy); // approximate energy calibration
    v_counts.push_back(counts);
    //cout << energy << " " << counts << endl;
  }
  infile1.close();
  count_integral = Integrate_vector(v_counts);
  v_counts = Normalize_vector(v_counts,count_integral);

  // try to get function from graph/ spline:
  double xmin = 0.;
  double xmax = 50.;
  TGraph *gr_E, *gr_E_corr;
  TF1 *fsp_E, *fsp_E_corr, *fsp_E_corr_smear;
  gr_E = new TGraph(v_energy.size(), &(v_energy[0]), &(v_counts[0]));
  /*
  if (gaussmear)
    {fsp_E = new TF1("fsp_E",[&](double*x, double *p){ return p[0]*Gaus1DFilter_Gr(gr_E, p[3])->Eval((x[0]- p[2])/p[1]); }, xmin , xmax , 4);
    fsp_E->SetParameters(1., 1., 0., 0.01);
  }
  */

  fsp_E = new TF1("fsp_E",[&](double*x, double *p){ return p[0]*gr_E->Eval((x[0]- p[2])/p[1]); }, xmin , xmax , 3);
  fsp_E->SetParameters(yscale, 0.0055, 0.);
  fsp_E->SetLineColor(kBlue);
  fsp_E->SetLineStyle(2);

  if (fit_E_corr) {
    gr_E_corr = Correct_Energy_spectrum({E_counts_csv}, {material},{mat_color}, "Messdaten/2207_RAL/attenuation_coeff.txt",  calibrate_E, par_m, par_b, xmax,xmin, "plots/test");
    fsp_E_corr = new TF1("fsp_E_corr",[&](double*x, double *p){ return p[0]*gr_E_corr->Eval((x[0]- p[2])/p[1]); }, xmin , xmax , 3);
    fsp_E_corr->SetParameters(yscale, 0.0055, 0.);
    fsp_E_corr->SetLineColor(kBlack);
    fsp_E_corr->SetLineStyle(3);
    if (gaussmear) {
      fsp_E_corr_smear = new TF1("fsp_E_corr_smear",[&](double*x, double *p){ return p[0]*Gaus1DFilter_Gr(gr_E_corr, p[3])->Eval((x[0]- p[2])/p[1]); }, xmin , xmax , 4);
      //fsp_E_corr_smear->SetParameters(yscale, 0.0055, 0., 1.5);
      fsp_E_corr_smear->SetParameters(yscale, 0.0055, 0., sig_E_start);
      if(material=="Am") fsp_E_corr_smear->SetParLimits(3,1.0, 10.0 ); // in unit keV
      else fsp_E_corr_smear->SetParLimits(3,0.3, 3.5 ); // in unit keV
      fsp_E_corr_smear->SetLineColor(kRed);
      }
  }

  TList *myfitfcts = new TList();
  myfitfcts->Add(fsp_E);
  if (fit_E_corr) {
    myfitfcts->Add(fsp_E_corr);
    if (gaussmear) myfitfcts->Add(fsp_E_corr_smear);
    }
  TIter next(myfitfcts);
  TF1 *fitf;
  while((fitf = (TF1*)next())){
    //fitf->SetParLimits(0,0.05*yscale, 20*yscale );
    fitf->SetParLimits(0,0.1*yscale, 10*yscale );
    fitf->SetParLimits(1,0.0048, 0.0062); // restrict to 20% in both directions
    if (fix_par2) fitf->FixParameter(2,0.);
    else  fitf->SetParLimits(2,-0.01, 0.01 );
  }


  /*
  // Plot spline interpolation of HEXITEC spectra
  TCanvas *c_HEX = new TCanvas("cHEX", "spline spectrum", 0, 0, 900, 600);
  //cout << fsp->Eval(30) << endl;

  fsp_E->Draw();
  c_HEX->SaveAs((nameofpdf+"_HEXITEC.pdf").c_str());
  c_HEX->Close();
  */

  TCanvas *c_h = new TCanvas("ch", "fit_to_hist", 0, 0, 900, 600);
  double Verror = diffh->GetBinCenter(2) - diffh->GetBinCenter(1);

  double Counterror = 0;
  for(int k = 0; k<diffh->GetNbinsX(); k++){
    if (diffh->GetBinCenter(k)> xfitmin) Counterror = max(Counterror, 0.05*diffh->GetBinContent(k));
  }

  // Errors of diffh have been propagated and passed already from Differentiate_spectrum function.
  // assumption that original threshold scan counts follow poission statistics. i.e. error = sqrt(counts).
  // These are then propagated through the numerical differentiation
  // To include an estimate of the Error on HEXITEC, the error for the histogram data is multiplied by 2 per bin.
  for(int k = 0; k<diffh->GetNbinsX(); k++) diffh->SetBinError(k, 2*diffh->GetBinError(k));

  /*
  //cout << "Error on V is " << Verror << endl;
  cout << "Error on V is " << Verror << " V, error on Counts is " << Counterror << endl;
  std::vector<double> Diffcounts;
  std::vector<double> eDiffcounts;
  std::vector<double> Volts;
  std::vector<double> eVolts;
  for(int k = 0; k<diffh->GetNbinsX(); k++){
    double bincont = diffh->GetBinContent(k);
    if(bincont!=0.){ // exclude empty bins from fit
      Diffcounts.push_back(bincont);
      //eDiffcounts.push_back(max(0.1*abs(bincont), sqrt(2*abs(bincont))));
      eDiffcounts.push_back(diffh->GetBinError(k));
      Volts.push_back(diffh->GetBinCenter(k));
      eVolts.push_back(Verror/2);
    }
    //else cout << "Nothing HERE " << k << endl;
  }

  auto diff_gre = new TGraphErrors(Volts.size(), &(Volts[0]), &(Diffcounts[0]), &(eVolts[0]), &(eDiffcounts[0]));
  */
  TH1D *errordraw = (TH1D*)diffh->Clone("stat. unc.");
  errordraw->SetFillColor(kGray);
  errordraw->SetFillStyle(3135);
  diffh->Fit("fsp_E", "E", "", xfitmin, xfitmax);
  cout << "Fit Chi^2: " << fsp_E->GetChisquare() << " NDF: " << fsp_E->GetNDF() << " Prob: " << fsp_E->GetProb() << endl;
  diffh->Draw("hist"); // hist, E2, E
  errordraw->Draw("E2 same");

  diffh->GetXaxis()->SetRangeUser(xplotmin, xfitmax);
  //diff_gre->Draw("same"); // to check, that graph actually describes histogram data
  fsp_E->Draw("same");
  if (fit_E_corr){
    diffh->Fit("fsp_E_corr", "E", "", xfitmin, xfitmax);
    fsp_E_corr->Draw("same");
    cout << "corrected Fit Chi^2: " << fsp_E_corr->GetChisquare() << " NDF: " << fsp_E_corr->GetNDF() << " Prob: " << fsp_E_corr->GetProb() << endl;
    if (gaussmear){
      diffh->Fit("fsp_E_corr_smear", "E", "", xfitmin, xfitmax);
      fsp_E_corr_smear->Draw("same");
      cout << "corrected Fit Chi^2: " << fsp_E_corr_smear->GetChisquare() << " NDF: " << fsp_E_corr_smear->GetNDF() << " Prob: " << fsp_E_corr_smear->GetProb() << endl;
    }
  }

  c_h->Modified();
  c_h->Update();
  //gPad->Update();

  TLegend *legend = new TLegend(0.6,0.75,1.0,0.94);
  char fitpar0legend[200] = "";
  sprintf(fitpar0legend,"C= %.3e #pm %.3e", fsp_E->GetParameter(0), fsp_E->GetParError(0));
  char fitpar1legend[200] = "";
  sprintf(fitpar1legend,"a = %.4f #pm %.4f mV/keV", fsp_E->GetParameter(1)*1000, fsp_E->GetParError(1)*1000);
  char fitpar2legend[200] = "";
  sprintf(fitpar2legend,"B = %.2f #pm %.2f mV", fsp_E->GetParameter(2)*1000, fsp_E->GetParError(2)*1000);
  char fitpar3legend[200] = "";
  if (gaussmear) sprintf(fitpar3legend,"#sigma_{E} = %.3f #pm %.3f keV", fsp_E->GetParameter(3), fsp_E->GetParError(3));
  legend->AddEntry(diffh, ("DECAL "+ material + " data").c_str());
  legend->AddEntry(errordraw, "stat. unc.");
  TLegendEntry* E1 =legend->AddEntry(fsp_E, "HEXITEC ");
  TLegendEntry* E2 =legend->AddEntry((TObject*)0, fitpar0legend, "");
  TLegendEntry* E3 =legend->AddEntry((TObject*)0, fitpar1legend, "");
  E1->SetTextColor(kBlue);
  E2->SetTextColor(kBlue);
  E3->SetTextColor(kBlue);
  if (!fix_par2){
    TLegendEntry* E4 =legend->AddEntry((TObject*)0, fitpar2legend, "");
    E4->SetTextColor(kBlue);
  }
  legend->SetFillStyle(0);
  legend->SetBorderSize(0);
  legend->Draw();

  if (fit_E_corr){
    TLegend *legend2 = new TLegend(0.6,0.2,1.0,0.45);
    char fitpar0legend2[200] = "";
    sprintf(fitpar0legend2,"C= %.3e #pm %.3e", fsp_E_corr->GetParameter(0), fsp_E_corr->GetParError(0));
    char fitpar1legend2[200] = "";
    sprintf(fitpar1legend2,"a = %.4f #pm %.4f mV/keV", fsp_E_corr->GetParameter(1)*1000, fsp_E_corr->GetParError(1)*1000);
    char fitpar2legend2[200] = "";
    sprintf(fitpar2legend2,"B = %.2f #pm %.2f mV", fsp_E_corr->GetParameter(2)*1000, fsp_E_corr->GetParError(2)*1000);
    char fitpar3legend2[200] = "";
    if (gaussmear) sprintf(fitpar3legend2,"#sigma_{E} = %.3f #pm %.3f keV", fsp_E_corr->GetParameter(3), fsp_E_corr->GetParError(3));
    TLegendEntry* Ec1 =legend2->AddEntry(fsp_E_corr, "HEXITEC corr");
    TLegendEntry* Ec2 =legend2->AddEntry((TObject*)0, fitpar0legend2, "");
    TLegendEntry* Ec3 =legend2->AddEntry((TObject*)0, fitpar1legend2, "");

    Ec1->SetTextColor(kBlack);
    Ec2->SetTextColor(kBlack);
    Ec3->SetTextColor(kBlack);
    if (!fix_par2){
      TLegendEntry* Ec4 =legend2->AddEntry((TObject*)0, fitpar2legend2, "");
      Ec4->SetTextColor(kBlack);
    }
    legend2->SetFillStyle(0);
    legend2->SetBorderSize(0);
    legend2->Draw();

    if (gaussmear) {
      TLegend *legend3 = new TLegend(0.3,0.11,0.6,0.45);
      char fitpar0legend3[200] = "";
      sprintf(fitpar0legend3,"C= %.3e #pm %.3e", fsp_E_corr_smear->GetParameter(0), fsp_E_corr_smear->GetParError(0));
      char fitpar1legend3[200] = "";
      sprintf(fitpar1legend3,"a = %.4f #pm %.4f mV/keV", fsp_E_corr_smear->GetParameter(1)*1000, fsp_E_corr_smear->GetParError(1)*1000);
      char fitpar2legend3[200] = "";
      sprintf(fitpar2legend3,"B = %.2f #pm %.2f mV", fsp_E_corr_smear->GetParameter(2)*1000, fsp_E_corr_smear->GetParError(2)*1000);
      char fitpar3legend3[200] = "";
      sprintf(fitpar3legend3,"#sigma_{E} = %.3f #pm %.3f keV", fsp_E_corr_smear->GetParameter(3), fsp_E_corr_smear->GetParError(3));
      TLegendEntry* Ecs1 =legend3->AddEntry(fsp_E_corr_smear, "HEXITEC corr smeared");
      TLegendEntry* Ecs2 =legend3->AddEntry((TObject*)0, fitpar0legend3, "");
      TLegendEntry* Ecs3 =legend3->AddEntry((TObject*)0, fitpar1legend3, "");
      TLegendEntry* Ecs5 = legend3->AddEntry((TObject*)0, fitpar3legend3, "");

      Ecs1->SetTextColor(kRed);
      Ecs2->SetTextColor(kRed);
      Ecs3->SetTextColor(kRed);
      Ecs5->SetTextColor(kRed);
      if (!fix_par2){
        TLegendEntry* Ecs4 =legend3->AddEntry((TObject*)0, fitpar2legend3, "");
        Ecs4->SetTextColor(kRed);
      }

      legend3->SetFillStyle(0);
      legend3->SetBorderSize(0);
      legend3->Draw();
    }
  }

  c_h->SetRightMargin(0.05);
  c_h->SetTopMargin(0.05);
  c_h->SaveAs((nameofpdf+".pdf").c_str());
  c_h->Close();
}

void Fit_Landau_to_Srdata(double xfitmin = 0.04, double xfitmax=0.5, double yscale = 10000000 ){
  TH1D *diffh = Differentiate_spectrum("Messdaten/220624_Sr90_1e9spectrum/220624_Sr90_stripmode_longterm_1e9.root", 100,101, 3, -0.1, 0.7,150000, "Sr",843, "220923_Sr90_differentiated_nofit_",0,0,0,0,0,0,1.171);
  TF1 *f1 = new TF1("flandau","[0]*TMath::Landau(x,[1],[2])",xfitmin,xfitmax);
  f1->SetParameters(yscale,0.01,0.01);
  f1->SetParLimits(0,0.01*yscale, 100*yscale );
  f1->SetParLimits(1,-2, 2. );
  f1->SetParLimits(2,0.00001,0.5);

  diffh->Fit("flandau");
  diffh->Draw("hist");
  f1->Draw("same");
}



void Current_variation(std::vector<string> rootpath, float fontsize=0.04, float titlesize=0.045, float titleoffsety=1.1, float titleoffsetx=0.87){
  int n = rootpath.size();

  std::vector<int> Istart={0,0,0};
  std::vector<int> Istop={200,600,600};
  std::vector<int> Istep={10,20,10};
  std::vector<int> colors={860,416,634};
  std::vector<int> linestyles = {1,2,8};
  const char *currents[3] = { "I_{ShaperBias}", "I_{FeedbackBias}", "I_{PreaAmpBias}" };

  TMultiGraph *mg_mean = new TMultiGraph();
  TMultiGraph *mg_std = new TMultiGraph();
  TMultiGraph *mg_C = new TMultiGraph();
  TMultiGraph *mg_stdoversqrtC = new TMultiGraph();

  for (unsigned int i = 0; i<n; i++){
    TGraph *gr_mean = new TGraph((Istop[i]-Istart[i])/Istep[i] +1);
    TGraph *gr_std = new TGraph((Istop[i]-Istart[i])/Istep[i] +1);
    TGraph *gr_C = new TGraph((Istop[i]-Istart[i])/Istep[i] +1);
    TGraph *gr_stdoverC = new TGraph((Istop[i]-Istart[i])/Istep[i] +1);
    char rfile[200] = "";
    sprintf(rfile,"%s", rootpath[i].c_str());
    TFile *f = new TFile(rfile);
    int count = 0;
    for(int ival = Istart[i]; ival <= Istop[i]; ival+=Istep[i]){
      char histoname[200] = "";
      sprintf(histoname,"counterThresholdScanRow00Ival%01d",ival);
      TH2D *histo = (TH2D*)f->Get(histoname);
      double mean_av = 0;
      double std_av = 0;
      double C_av = 0;
      double std_overC_av = 0;
      // calculate average of the three above quantities
      for (unsigned int col = 0; col<64; col++){
        TH1D *projy = histo->ProjectionY("histo_py", 1+col,1+col);// 1 to 64 to avoid overflow bins
        mean_av +=projy->GetMean();
        std_av += 1000*projy->GetStdDev();
        C_av += projy->GetMaximum();
        std_overC_av += 1000*projy->GetStdDev()/sqrt(projy->GetMaximum());
      }
      gr_mean->SetPoint(count, ival, mean_av/64.);
      gr_std->SetPoint(count, ival, std_av /64. ); // in mV
      gr_C->SetPoint(count, ival, C_av /64.);
      gr_stdoverC->SetPoint(count, ival, std_overC_av /64. );
      //if(i==0 and ival == 10) gr_std->RemovePoint(count); // to exclude faulty data points
      count++;
    }
    if(i==0) {
      gr_std->RemovePoint(10/Istep[i]); // to exclude faulty data points
      gr_stdoverC->RemovePoint(10/Istep[i]);
      gr_std->RemovePoint(170/Istep[i]-1); // to exclude faulty data points
      gr_stdoverC->RemovePoint(170/Istep[i]-1);
      }
    if(i==1) {
      gr_std->RemovePoint(140/Istep[i]); // to exclude faulty data points
      gr_stdoverC->RemovePoint(140/Istep[i]);
    }

    gr_mean->SetLineColor(colors[i]);
    gr_mean->SetLineStyle(linestyles[i]);
    gr_mean->SetLineWidth(3);
    gr_mean->SetTitle(currents[i]);
    gr_std->SetLineColor(colors[i]);
    gr_std->SetLineStyle(linestyles[i]);
    gr_std->SetLineWidth(3);
    gr_std->SetTitle(currents[i]);
    gr_stdoverC->SetLineColor(colors[i]);
    gr_stdoverC->SetLineStyle(linestyles[i]);
    gr_stdoverC->SetLineWidth(3);
    gr_stdoverC->SetTitle(currents[i]);
    gr_C->SetLineColor(colors[i]);
    gr_C->SetLineStyle(linestyles[i]);
    gr_C->SetLineWidth(3);
    gr_C->SetTitle(currents[i]);

    mg_mean->Add(gr_mean);
    mg_std->Add(gr_std);
    mg_C->Add(gr_C);
    mg_stdoversqrtC->Add(gr_stdoverC);
  }

  TList *myMultigr = new TList();
  myMultigr->Add(mg_std);
  myMultigr->Add(mg_mean);
  myMultigr->Add(mg_C);
  myMultigr->Add(mg_stdoversqrtC);
  const char *vars[4] = { "Std of noise peak (mV)", "Mean of noise peak (V)", "Maximum of noise peak (counts)" , "Std / Sqrt(Maximum) of noise peak (mV)"};
  const char *pdfsuffix[4] = { "Std", "Mean", "Counts", "StdoversqrtC" };

  TIter next(myMultigr);
  TMultiGraph *mg;
  unsigned int k=0;
  while((mg = (TMultiGraph*)next())){
    TCanvas *c = new TCanvas("c", "Current variation", 0, 0, 900, 600);
    char title[200] = "";
    sprintf(title,";Current (#muA);%s", vars[k]);
    mg->SetTitle(title);
    c->SetRightMargin(0.05);
    c->SetTopMargin(0.05);
    mg->GetXaxis()->SetLabelSize(fontsize);
    mg->GetXaxis()->SetTitleSize(titlesize);
    mg->GetYaxis()->SetLabelSize(fontsize);
    mg->GetYaxis()->SetTitleSize(titlesize);
    mg->GetYaxis()->SetTitleOffset(titleoffsety);
    mg->Draw("AL");
    char pdfname[200] = "";
    sprintf(pdfname,"Current_variation_%s.pdf",pdfsuffix[k]);
    TLegend *legend = c->BuildLegend(0.6,0.15,0.95,0.4);
    legend->SetFillStyle(0);
    legend->SetBorderSize(0);
    c->SaveAs(pdfname);
    c->Close();
    k++;
  }
}

void Combine_histograms(std::vector<string> rootpath){
  std::vector<int> Istart={0,50};
  std::vector<int> Istop={40,200};
  std::vector<int> Istep={10,10};
  TList *l = new TList();

  for (unsigned int i = 0; i<rootpath.size(); i++){
    char rfile[200] = "";
    sprintf(rfile,"%s", rootpath[i].c_str());
    TFile *f = new TFile(rfile);
    for(int ival = Istart[i]; ival <= Istop[i]; ival+=Istep[i]){
      char histoname[200] = "";
      sprintf(histoname,"counterThresholdScanRow00Ival%01d",ival);
      TH2D *histo = (TH2D*)f->Get(histoname);
      l->Add(histo);
    }
  }
  TFile *fout = new TFile("histlist.root","RECREATE");
  l->Write();//TObject::kSingleKey);
  fout->ls();
}

void zeropad_histogram(string rootpath, string histname, double Vstart, double Vstop, double Vstep){
  char rfile[200] = "";
  sprintf(rfile,"%s", rootpath.c_str());
  TFile *f = new TFile(rfile);
  char histoname[200] = "";
  sprintf(histoname,"%s",histname.c_str());
  TH2D *histo_orig = (TH2D*)f->Get(histoname);

  int nSteps = (int)round((Vstop - Vstart) / Vstep +1);
  cout << "NSteps: " << nSteps  << endl;
  TH2D *hist = new TH2D(histname.c_str(), histname.c_str(),64, -0.5, 63.5, nSteps, Vstart-Vstep/2., Vstop+Vstep/2.);
  for(unsigned int yind2=0; yind2 < hist->GetNbinsY()+1; yind2++){
    cout << hist->GetYaxis()->GetBinCenter(yind2) << endl;
  }

  for(unsigned int yind=0; yind < histo_orig->GetNbinsY()+1; yind++){
    cout << histo_orig->GetYaxis()->GetBinCenter(yind) << endl;
  }

  for(unsigned int xind=0; xind < histo_orig->GetNbinsX()+1; xind++){
    for(unsigned int yind=0; yind < histo_orig->GetNbinsY()+1; yind++){
      hist->Fill(histo_orig->GetXaxis()->GetBinCenter(xind) , histo_orig->GetYaxis()->GetBinCenter(yind), histo_orig->GetBinContent(xind, yind));
    }
  }
  //hist->Draw("colz");
  TFile *fout = new TFile("hist_zeropadded.root","RECREATE");
  hist->Write();//TObject::kSingleKey);
  fout->ls();
  //hist->SaveAs("this.root")
}


// NEW FROM HERE ON
