DECAL Data Acquisition Software
===============================

This Software is written to allow readout and testing of the DECAL test
chip (photograph below). The software is based on
`ITSDAQ <https://gitlab.cern.ch/atlas-itk-strips-daq/itsdaq-sw>`__ and
just adds functionality to allow running with a `NEXYS
Video <https://reference.digilentinc.com/reference/programmable-logic/nexys-video/start>`__
board and DECAL adapters, configuring these to operate the chip.

.. figure:: img/decalChip.jpg
   :alt: Decal Chip Photo

   Decal Chip Photo

I am trying to keep it up to date by pushing regularly, but please ask
jdopke_at_cern.ch, in case you feel like there is something amiss.

Setup and Operation
-------------------

For the initial software setup please see below (Build Instructions)
whilst a setup of the scripts is described in `the
Changelog <https://gitlab.cern.ch/jdopke/radhard-decal-daq/blob/master/CHANGELOG.md>`__,
as this depends on your software version.

**Before following belows instructions please read them first (including
Using the scripts) and prepare required software/cables/anything.**

Setting up the Hardware
~~~~~~~~~~~~~~~~~~~~~~~

DECAL Data Acquisition is done using a `NEXYS
Video <https://reference.digilentinc.com/reference/programmable-logic/nexys-video/start>`__
board from `Digilent <http://store.digilentinc.com/>`__.

To set the board up for operation with DECAL, one needs to attach the
Decal Motherboard (`schematic <MotherboardSchematicRevB.pdf>`__) prior to
powering, as well as a low voltage supply (for NEXYS), a micro USB
programming cable (or alternatively an SD card/USB stick) and finally a
low voltage supply for the DECAL Motherboard (+6V as labelled on the
board, supplied via screw terminals, see top right of the picture
below). Remember to also **set all Jumpers on the DECAL Motherboard, as
shown in the following picture**.

.. figure:: img/decalMotherboard.jpg
   :alt: DECAL DAQ setup with NEXYS and Motherboard

   DECAL DAQ setup with NEXYS and Motherboard

Once this is set up, power up NEXYS as well as the 6V supply for the
Decal motherboard. You can now program your NEXYS Video using
`Adept <http://store.digilentinc.com/digilent-adept-2-download-only/>`__.
Find the latest Firmware, currently version 0xB1CC, on `Matt’s Firmware
Emporium <http://www.hep.ucl.ac.uk/~warren/upgrade/firmware/?C=M;O=D>`__
and download it to the board. Adept should automatically detect the
board once powered up. In case it doesn’t your jumper configuration may
well be incorrect. Please verify that jumpers (and switches) on Nexys
are set as shown in the previous picture.

To start with do this all without plugging in the DECAL daughterboard
(see below).

.. figure:: img/decalDaughterboard.jpg
   :alt: DECAL DAQ setup with NEXYS and Motherboard

   DECAL DAQ setup with NEXYS and Motherboard

Once confident that you can operate the motherboard (all operations but
reading proper data back will work without the daughterboard), power
down the 6V as well as NEXYS itself, insert your daughterboard (see
image below for the vertical connector version) and rerun the power up
sequence.

.. figure:: img/decalAssembly.jpg
   :alt: DECAL DAQ setup with NEXYS and Motherboard

   DECAL DAQ setup with NEXYS and Motherboard

Using the scripts
~~~~~~~~~~~~~~~~~

Use the ``startUP()`` function to get power up and set the chip up for
initial operation. Up to 4 boolean parameters can be provided: \* Mode
(false=PAD, true=Strip) \* PLL Enable (true=320Mbit/s outputs) \* Clear
Test Vector (true means the test values at the top of column get reset
to 0) \* Set a manual Phase adjustment, setting the phase to 0 initally

Once that is done you can read raw data from the outputs of the chip
through:

``debugData(1, 18)`` or ``debugMe(1, 18[, true])`` (the optional
parameter in the latter orders the output by return channel number).
This will print the returned data on the screen for you to examine. If
you care for a more numerical approach, have a look at how
``captureData`` is used within ``debugMe`` to retrieve a vector of
returned data.

The last two channels (when reading 18) are sampled versions of the
clock delivered by the DECAL chip. As these are sampled at 640 MBit/s
but only read with 320 MBit/s, the only reflect half of a 25ns readout
window. (I.e. don’t get confused that only one of them looks like a
well-behaved clock)

| Injecting numbers into the test vector will shift all numbers already
  in there further down to the following columns:
| - ``inject_one()`` - Injects **1** into a **single** column
| - ``inject_zero()`` - Injects **0** into a **single** column
| - ``inject_number(value, nTimes)`` - Injects **value** into a
  **nTimes** column(s), default for **nTimes** is one.

E.g.: ``reset_testVector()`` shifts in many zeros until the test vector
is empty. ``inject_one()`` will feed a single one into the chip test
vector. Repetetively ``inject_zero()`` will then push that one through
all the columns till it disappears at the end of the test vector.

At the end of operation, **DO NOT FORGET** to power down the chip using
``powerDOWN(0)``. The function takes a parameter, allowing to keep logic
signals to the chip steady even if power is absent. However calling it
with paramter *0* is recommended.

If you feel courageous, have a look at `the
tasklist <https://gitlab.cern.ch/jdopke/radhard-decal-daq/blob/master/CONTRIBUTING.md>`__
and either pick something up or add/let me know (jdopke_at_cern.ch)
whatever is missing.
