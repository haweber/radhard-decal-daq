Index of Documents available from this site
===========================================

Chip Manuals
------------

`DECAL FD Manual <DECAL_FD_Manual.pdf>`__

`DECAL Manual <DECAL_Manual.pdf>`__

Schematics
----------

`DECAL Motherboard Rev B <MotherboardSchematicRevB.pdf>`__ - For DESY

`DECAL Motherboard Rev A <MotherboardSchematicRevA.pdf>`__ - For all others, but were fixed to be equivalent to Rev B

