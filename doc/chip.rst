Chip Description
================

This page is to give a rough introduction to DECAL, outlining general
use, available test features and known bugs. Some in-depth information
is provided by the DECAL Manual, which is available
`here <DECAL_Manual.pdf>`__

General Introduction
--------------------

The DECAL ASIC provides a 64x64 matrix of pixels, each containing an
AC-coupled pre-amplifier, a shaper and finally a discriminator. The
offset between shaper output and discriminator input can be adjusted, to
allow local trimming of each pixel cell with respect to a global
threshold input. Each pixels output drives the clock input of a D-type
flip-flop (DFF) with input ‘1’ and an asynchroneous reset. With a
central clock, the output is then driven into a second DFF, which
provides the output of the pixel, as well as driving the asynchroneous
reset of the first DFF, thereby guaranteeing that the pixel does not
stay above threshold, if the discriminator output stays high for several
clock cycles.

All pixels are organised in columns and rows. The column structure is
important for readout, whilst the row structure is important during
`configuration <#chip-configuration>`__. With the chip being design for
calorimetric applications, i.e. summation of hits in mind, each pixels
output is added up to a total column value, that can at maximum reach 31
(5-bit value). This is done with local half-adders at first, staging
this up to total sums over 5 bits with full adders. On top of that, each
column has an input value that is driven from a `test
register <#test-register>`__ at the top of the column.

The chip as a whole can operate in two modes:

-  **Strip-like:** Providing up to 3 hits per column for each bunch
   crossing
-  **Pad like:** Providing up to 240 hits (plus overflow) from 16 column
   blocks for each bunch crossing

Strip Mode Operation
--------------------

Strip mode allows to use the chip in a tracker type implementation. For
each column, a two bit value is transmitted out - the content of that
two bit value is the number of hits from the column with a maximum value
if 3 being set for all values above or equal to 3.

Transferring out a number of hits, rather than just a hit-or-no-hit
value can help solve ambiguities and could therefore allow this chip to
be used with large stereo angles in high track density environments.

Pad Mode Operation
------------------

In Pad mode, the chip provides a total of eight outputs, four pairs of a
sum output and an overflow output, which provide information for four
blocks of 16 columns each. The blocks are coninuous in the chip,
spanning columns 0-15, 16-31, 32-47 and 48-63.

The sum output is generated through full summation of the lowest 4 bits
from each 5-bit column sum. The highest bit is ignored for the sum, but
instead gets added up in the overflow output, allowing to track whether
any column had more than 15 hits. This allows for reconstruction of the
total number of hits per pad area, as well as easily tagging large
column occupancies.

Chip Configuration
------------------

DECAL is configured using an SPI-type interface: A clock and data input
operate in parallel, shifting configuration data into a serial shift
register. This register however only holds a single configuration bit
for each row in the chip. Horizontal propagation through the rows (and
bits within each pixel in a row) is done using a dedicated clock. The
input shift register is propagated in from column 63 to column 0.

The horizontal clock is fed through the chip from the far side (with
respect to the input shift register, i.e. column 0), with buffering
implemented at every column transition, thus providing for correct
signal strenght and timing to propagate the configuration correctly
through the chip.

Test register
-------------

A test register is implemented at the “top” of each column, allowing one
to define an input “sum” for each column. With deactivated Pixels, this
provides a means of testing the end-of-column summation and thresholding
(in strip mode), output timings and other digital operation of the chip.

The test register is implemented as a serial shift register, feeding
test values in from column 0 through to column 63.

Known Bugs
----------

Configuration Clock Buffering
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Between adjacent Pixel Cells, the configuration clock is buffered, but
due to a layout mistake, this buffer is inverting. This leads to bit
propagating faster from one Pixel to the next. Therefore configuration
can only be loaded into either odd or even pixels correctly, but not
into both. The correct loading is achieved, but configuring with full
clock cycles per bit, but dropping a single configuration bit being
written for every other column (i.e. configuring 6 bits for the column
that is to be configured correctly and only writing 5 bits for the next
columns)

**NOTE: This bug has been fixed in DECAL_FD.**
The manual for this chip was uploaded `here <DECAL_FD_Manual.pdf>`_

Motherboard Bias Currents
~~~~~~~~~~~~~~~~~~~~~~~~~

To achieve the required pull of 100uA (nominal value) on all current
sources provided by the motherboard, the bias resistors of the current
sources (R36, R40, R42, R45, R48, R63, R68 and R78) have to be changed
from 10kOhm down to 1kOhm, as the 10kOhm load resistor doesn’t provide
enough sinking current on the PMOS current mirrors used inside DECAL.
