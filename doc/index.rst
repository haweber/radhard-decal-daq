.. DECAL DAQ Software documentation master file, created by
   sphinx-quickstart on Sat Jan 12 22:47:51 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :maxdepth: 2
   :hidden:

   Introduction <intro>
   Installing ITSDAQ <itsdaq>
   Installing DECAL DAQ <decaldaq>
   Using DECAL DAQ <usingdaq>
   Some remarks <remarks>
   Chip description <chip>
   Frequently asked questions <faq>
   File index <documentlinks>

DECAL Data Acquisition Software
===============================

This `Software <https://gitlab.cern.ch/jdopke/radhard-decal-daq/>`__ is written to allow readout and testing of the DECAL test
chip (photograph below). The software is based on
`ITSDAQ <https://gitlab.cern.ch/atlas-itk-strips-daq/itsdaq-sw>`__ and
just adds functionality to allow running with a
`NEXYS Video <https://reference.digilentinc.com/reference/programmable-logic/nexys-video/start>`__
board and DECAL adapters, configuring these to operate the chip.

I am trying to keep it up to date by pushing regularly, but please ask
jdopke_at_cern.ch, in case you feel like there is something amiss.

