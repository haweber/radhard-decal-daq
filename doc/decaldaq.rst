Building/Installing DECAL Software for ITSDAQ
=============================================

Using ``git clone https://gitlab.cern.ch/jdopke/radhard-decal-daq.git``,
clone the DECAL software repository. A third parameter can be added to
the git command to clone into a specific subdirectory,
i.e. \ ``git clone https://gitlab.cern.ch/jdopke/radhard-decal-daq.git MYFAVOURITESUBDIRECTORY``.

Go into your wonderful clone and copy files from subfolders into the
corresponding subfolders in your ITSDAQ installation (DecalConfig.\*
into stdll/, DecalMotherboard.\* and test_DECAL.cxx into macros/)

Now there is a way to understand what happens next (basically adding
DECAL related stuff to the compilation of ITSDAQ), but there is another,
simple, way:

Go into your ITSDAQ directory and run the follwing command:

.. code:: bash

   patch -p1 < $MYFAVOURITESUBDIRECTORY/itsdaq.patch

Note that `$MYFAVOURITESUBDIRECTORY` is a replacement for wherever
you checked out the DECAL software repository to.
This automatically applies a reasonably recent diff after doing the
hard stuff below - i.e. it's all been don for you! The patch also
contains removal of ITSDAQ database preliminaries, which you don't
need and output windows, which show no useful information when used
with DECAL.

The nasty details
-----------------

In ITSDAQ you’ll need to modify stdll/linkdef.h to include (just before
endif):

.. code:: cpp

   #pragma link C++ class PixConfig;
   #pragma link C++ class ColumnConfig;
   #pragma link C++ class ChipConfig;
   #pragma link C++ class RawData;
   #pragma link C++ class StripHit;
   #pragma link C++ class PadHit;
   #pragma link C++ class RawDataDecoder;
   #pragma link C++ class StreamData;
   #pragma link C++ class std::vector<StripHit>;
   #pragma link C++ class std::vector<PadHit>;
   #pragma link C++ class std::vector<StreamData>;
   #pragma link C++ class std::vector<unsigned int>;

Finally, to build DecalConfig Objects into stdll, you’ll need to add
DecalConfig to the compile parameters. This happens in wscript, located
in your ITSDAQ base folder. In line 585 (roughly), it needs adding of
``"DecalConfig.h",`` to stdll_cls. Be sure to add DecalConfig.h before
the last object in that variable, which always has to be
``"linkdef.h"``.

Your stdll_cls should now look roughly like this:

.. code:: python

       stdll_cls = ["stdll/%s" % s for s in
                               ["TLogger.h", "TModuleDCS.h", "TModule.h", "../dcsdll/TDCSDevice.h",
                               "TSequencer.h", "TSignals.h", "TST.h", "TChip.h", "HVStripV1.h",
                               "DecalConfig.h", "linkdef.h"]]

A few lines below this entry you’ll need to add DecalConfig to SrcNames,
if possible before the line adding hsio (``if bld.env.use_hsio:``):

.. code:: python

       srcNames += ['DecalConfig'] # This line needs adding!
       if bld.env.use_hsio:

You can now re-run configuration/building/installation of ITSDAQ as done
above and should be good to operate functions from DecalMotherboard.cpp
and test_Decal.cxx in macros.
