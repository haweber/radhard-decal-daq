Using DECAL DAQ
===============

To start with, go into your ITSDAQ directory and run:

.. code:: bash
   
   ./RUNITSDAQ.sh

If you used the patch procedure for installation of the
DECAL software, you should automatically end up in a ROOT
shell. The follwing code block will load the needed macros,
start up the power for the chip plugged into your test
system, adjust some settinngs and then run a threshold scan
for a single row of pixels.

.. code:: cpp

   loadmacro("DecalMotherboard")
   .L test_DECAL.cxx
   startUP(true);
   setManualPhase(true, 0);
   setupAnalog();
   counterThresholdScanRow(80, 31, 0.04, 1.11, true, 1000000, 1100, 0);

Let's take this apart: The first two lines are just loading
macros, where `DecalMotherboard` has dependencies and thus needs
to be loaded through use of another macro (that is already loaded).

`startUP(bool mode)` will power up the chip, either in strip
mode (`true` or `1`) or in pad mode (`false` or 0)

`setManualPhase(bool manual, unsigned int phase)` will force the FPGA to sample input data with a fixed phase (available are 16 steps over 40MHz)

`setupAnalog()` sets all Analog currents to standard values (100uA for most) and all voltages to standard values

`counterThresholdScanRow(unsigned int nSteps, int row, double voltagerange, double offsetvoltage, bool resetcalibration, unsigned int ncycles, unsigned int nsleep, ChipConfig * config)`
