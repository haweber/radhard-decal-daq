Remarks
=======

Motherboards
------------

All motherboards are flawed in one way or another:

Initial batch (see Rev A Schematic `here <MotherboardSchematicRevA.pdf>`__) had too large current sink resistors and trouble in turning on 5V, after a lot of playing this was determined to be a problem with the inductor in the supply to the 5V regulator and this was taken out and replaced by a short. `Revision B <MotherboardSchematicRevB.pdf>`__ of the schematic has this fixed, but is still troublesome in turning on the 5V line. Replacing C34 and C35 of the voltage monitoring circuit it was eventually understood that C30 implies a startup time of the 5V regulator of roughly 15ms - C34 therefore needs changing to at least 220nF.

Differences in running with ROOT 6
----------------------------------

Rather than loading macros with headers through ``.L <Macroname>.cpp``
root6 needs to load dependencies. This is done through a dedicated
function called ``loadmacro("<Macroname>")``, which is maintained
through ITSDAQ.
